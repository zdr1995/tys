# TYS APP

## Requirements

* php >= 7.1.3
* BCMath PHP Extension
* Ctype PHP Extension
* JSON PHP Extension
* Mbstring PHP Extension
* OpenSSL PHP Extension
* PDO PHP Extension
* Tokenizer PHP Extension
* XML PHP Extension
* [GD Library >=2.0 or Imagick PHP extension >=6.5.7](https://www.php.net/manual/en/book.image.php)

## Backend libraries and packages
* [barryvdh/laravel-cors](https://github.com/fruitcake/laravel-cors)
* [PHP DOM Pdf](https://github.com/dompdf/dompdf)
* [barryvdh/laravel-dompdf](https://github.com/barryvdh/laravel-dompdf)
* [brozot/laravel-fcm](https://github.com/brozot/Laravel-FCM)
* [guzzlehttp/guzzle](https://docs.guzzlephp.org/en/stable/)
* [intervention/image](http://image.intervention.io/)
* [mronx/laravel-ffmpeg](https://github.com/mronx/laravel-ffmpeg)
* [php-ffmpeg/php-ffmpeg](https://github.com/PHP-FFMpeg/PHP-FFMpeg)
* [tymon/jwt-auth](https://github.com/tymondesigns/jwt-auth)

## For properly run app you need:

* Go to root of app
* Run `$which(php) composer.phar install` or `composer install`
* Run `$which(php) artisan key:generate`
* Run `cp .env.example .env`
* In env you need to setup:
	* `APP_NAME` possible values `TYS or whatever`
	* `APP_URL` is domain (path) to your app
	* `DB_DATABASE` name of your database
	* `DB_USERNAME` your mysql client user
	* `DB_PASSWORD` your mysql client password
	* `FCM_USAGE` possible true|false
	* `FCM_SERVER_KEY` is key for your server app - this is be retrieved from [Firebase](firebase.google.com)
	* `FCM_SENDER_ID`
	* `FFMPEG_BINARIES` - path on filesystem where is ffmpeg binaries stored
	* `FFPROBE_BINARIES` - path on filesystem where is ffprobe binaries stored
	* `FILESYSTEM_DRIVER` - driver that app use by default, overriden under with ``FILE_DRIVER``, posible local|cloud
	* `FILE_DRIVER` - driver in use
	* `JWT_SECRET` - JSON Web Token secret key
	* `DEFAULT_PASS` - default password for new users in app
	* `APP_LINK` - Link to dashboard app
	* `API_URL` - Link to this app
	* `CHATKIT_INSTANCE_LOCATOR` - not in usage anymore
	* `CHATKIT_KEY` - Not in usage anymore
	* `CHAT_CONNECTED` possible values true|false
	* `PUSHER_APP_ID`
	* `PUSHER_APP_KEY`
	* `PUSHER_APP_SECRET`
	* `PUSHER_APP_CLUSTER`
	* `MAIL_FROM_ADDRESS` - mail address for application
	* `MAIL_PASSWORD` - mail pass for account
	* ``
* For Dev Run `$(which php) artisan migrate` to run app migrations and make tables
* If you wann'a to fill tables go to `database/seeds/DatabaseSeeder.php` file and uncomment lines for specific tables which you wan't to fill and then run `$(which php) artisan db:seed` from root of your project

* Run App:
	* Add propietary permissions for your app
	* Go to root of your project
	* Run `sudo chown $(id -u):$(id -g) -R .` in case if you run your app on internal php cli server
	* Run `sudo chown $(id -u):www-data -R .` in case if you run your app on Nginx/Fpm or Apache2 server
	* Run `chmod 755 -R .` to change permissions for your app
	* Link storage (for local filesystem)
		* Run `$which(php) artisan storage:link` or
		* Run `cd ./public && ln -s ../storage/app/public ./storage`
		* If you are in public folder run `ln -s ../resources ./src` to link resources folder in public path
	* If you run internal server go to root of your project and then run `php artisan serve` and internal cli server goes listening on `127.0.0.1:8000`

* There is folder named `.ebextensions` with file `clear.config` for automatically clear cache and setup application for AWS EC2 usage