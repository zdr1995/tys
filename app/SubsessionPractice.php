<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubsessionPractice extends Model
{
    //
    public function practices()
    {
        return $this->belongsTo(Practice::class,'practice_id');
    }
    public function subsessions()
    {
        $this->belongsTo(Subsession::class,'subsession_id');
    }
    public function users()
    {
        return $this->belongsTo(\App\User::class,'user_id');
    }
    public function deleteMany(Request $r)
    {
        $subsession_id=$r['subsession_id'];
        $practices=$r['practices'];
        foreach($practices as $practice_id)
        {
            $instance=static::where('subsession_id','=',$subsession_id)
                                ->where('practice_id','=',$practice_id)
                                ->get();
            foreach($instance as $row)
                if( !$instance->delete() )
                    return response()->json(['message'=>'failed'],404);
        }
        return response()->json(['message'=>'deleted']);
    }
}
