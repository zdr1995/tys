<?php

namespace App;

use App\Classes\Storage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Model;

class File extends BaseModel
{
    protected static $path='/storage/';
    protected static $videoPath='videos/';
    protected static $picturePath='pictures/';
    protected $type;
    protected $fullPath;
    protected $primaryKey='id';

    public $fileExtension;
    protected $table='media';
    protected $fillable=[
        'media_id',
        'media_name'
    ];
    public function store($object_type,$media_type)
    {
        parent::__construct($attributes);
        ($media_type)?$this->type=static::$picturePath:$this->type=static::$videoPath;
        $this->setFullPath();
    }
    public function setFullPath()
    {
        $this->fullPath= static::$path.$this->type;
    }
    public function download($name)
    {
        $filePath="/public/$name.jpg";
        return Storage::disk()->download($filePath,'Pera.jpg');
    }
}
