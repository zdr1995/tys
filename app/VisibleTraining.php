<?php

namespace App;


class VisibleTraining extends BaseModel
{
    protected $table= 'visible_trainings';
    public $fillable=[
        'user_id',
        'visible'
    ];
    protected $guarded=[];
    public $timestamps= false;
    public $created_at= false;
    public $updated_at= false;
    public function setUpdatedAt($value)
    {
      return NULL;
    }


    public function setCreatedAt($value)
    {
      return NULL;
    }

    public function getVisibles()
    {
        // dd($this);
        return $this->visible;
    }

    public function getParentsVisibility(User $user)
    {
        $parent= User::where('id',$user->parent_id)->first();
        if($parent!==null)
        {
            $parents_data= static::where('user_id',$parent->id)->first();
            $self_data= static::where('user_id',$user->id)->first();
            // dd($parents_data->attributes['visible']);
            if( $parents_data!==null )
            {
                $visible= json_decode(json_encode($parents_data->attributes['visible']));
                $data=new static;
                $data->user_id=$user->id;
                $data->visible=$visible;
                if($self_data==null) 
                    $success= $data->save();
                else {
                    $self_data->visible= $visible;
                    if( !$self_data->save() )
                        return false;
                }
            }
        }else {
            
        }
        
        return true;
    }
    public function updateFromParents(Training $training, $active)
    {
        $user= $this->getLoggedIn();
        $childrens= User::where('parent_id','=',$user->id)->get();
        // dd($childrens);
        foreach($childrens as $children)
        {
            $row= static::where('user_id','=',$children->id)->first();
            // dd($row);
           
            // dd($row);
            if($row!==null)
            {
                $data=[
                    'training_id'=> $training->id,
                    'active'=> $active
                ];
                if( !$row->visibleContains($data) )
                {
                    $trainings_list=$row->visible;
                    $row->visible[]=[
                        'training_id'=> $training->id,
                        'active'=> $active
                    ];
                    if(!$row->save())
                        return false;
                }else {
                    //  Return all new array of data
                    $new_value= $row->updateExisting($data);
                    $row->visible=$new_value;
                    if( !$row->save() )
                        return false;
                }
            }else {
                // If parents visibility is null use superadmins
                // $superadmin= \App\User::where('role','=','superadmin')->first();
                // $sa_visible= static::where('user_id','=',$superadmin->id)->first();
                // //  if superadmins is null
                // if($superadmin!==null) {
                //     $active_row= $sa_visible->visible;
                //     $active_row[]= [
                //         'training_id'=>$training->id,
                //         'active'=>$active
                //     ];
                //     // dd(json_encode($active_row[0]));
                //     $data=[
                //         'user_id'=> $user->id,
                //         'visible'=> json_encode($active_row[0])
                //     ];
                //     static::insert($data);
                // }
            }
            
        }
        return true;
    }

    public function visibleContains(array $data)
    {
        foreach($this->visible as $visible)
        {
            if( $visible==$data )
                return true;
        }
        return false;
    }

    public function updateExisting(array $data)
    {
        foreach($this->visible as $visible)
        {
            if( $visible['training_id']==$data['training_id'] )
            {
                $visible['active']=$data['active'];
            }
        }
        return $this->visible;
    }

    
}
