<?php

namespace App;
use Illuminate\Http\Request;

class SessionSubsession extends BaseModel
{
    protected $fillable=[
        'session_id',
        'subsession_id',
        'creator_id',
        'published',
        'publisher_id'
    ];
    public function sessions()
    {
        return $this->belongsTo(Session::class,'session_id');
    }
    public function subsessions()
    {
        $this->belongsTo(Subsession::class,'subsession_id');
    }
    public function users()
    {
        return $this->belongsTo(User::class,'creator_id');
    }
    public function preCreate(Request $r)
    {
        // ToDo
        $this->creator_id=$this->getLoggedIn()->id;
        return true;
    }
    public function postCreation(Request $r)
    {
        $subsession= new Subsession;
        if ($r->has('picture'))
            $subsession->storePicture($r);
        
        if ($r->has('drill_manual'))
            $subsession->storeDrill($r);
        
        if ( !$subsession->save() )
            return false;

        return true;
    }
    public function preUpdate(Request $r, $id)
    {
        return true;
    }
    public function postUpdate(Request $r)
    {
        return true;
    }
    public function deleteMany(Request $r)
    {
        $user=$this->getLoggedIn();
        $session_id=$r['session_id'];
        $subsessions_array=$r['subsessions'];
        foreach($subsessions_array as $subsession_id)
        {
            $self=static::where('session_id','=',$session_id)
                            ->where('subsession_id','=',$subsession_id)
                            ->get();
            foreach($self as $row) 
                if(! $row->delete() )
                    return response()->json(['message'=>'failed'],404);
        }
        return response()->json(['message'=>'deleted'],200);
    }
    
}
