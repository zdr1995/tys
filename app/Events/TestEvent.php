<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\Notification;
// use Pusher;

class TestEvent extends Event implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message='')
    {
        $this->message= $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // \broadcast('test-event');
        // dd("tu si");
        // var_dump( extension_loaded('cassandra') ) or die();
        // $pusher=new Pusher(
        //     env('PUSHER_APP_ID'),
        //     env('PUSHER_APP_SECRET'),
        //     env('PUSHER_APP_SECRET')
        // );
        // // Broadcast::channel('test-channel');
        // $data=[
        //     'message'=>'Zdravko poslo, pavle prihvatio'
        // ];
        // Pusher::trigger('test','test-event',json_encode($data));
        // return ['test'];
       
        // return ['test'];
    }
    public function broadcastAs()
    {
        return 'test-event';
    }
}
