<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingRelationModel extends BaseModel
{
    protected $table= 'training_relation_models';
    protected $fillable=[
        'training_picture',
        'session_picture',
        'subsession_picture',
        'practice_picture',
        'practice_video',
        'active',
        'user_id',
        'training_id',
        'session_id',
        'subsession_id',
        'practice_id'
    ];

    public function relations()
    {
        return TrainingRelationModel::all()->load(['trainings','practices','sessions'])->where('id','=',$this->id);

    }
    public function user($id)
    {
        return $this->where('user_id','=',$id);
    }
    public function trainings()
    {
        return $this->belongsTo(\App\Training::class,'training_id');
    }
    public function practices()
    {
        return $this->belongsTo(\App\Practice::class,'practice_id');
    }
    public function sessions()
    {
        return $this->belongsTo(\App\Session::class,'session_id');
    }
    public function subsessions()
    {
        $this->belongsTo(\App\Subsession::class,'subsession_id');
    }

    public function users()
    {
        return $this->belongsTo(\App\User::class,'user_id');
    }
}
