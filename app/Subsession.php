<?php

namespace App;

use App\Classes\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Intervention\Image\Exception\NotReadableException;
use Barryvdh\DomPDF\PDF;
use DB;
use App\Session;

class Subsession extends BaseModel
{
    protected $table= 'subsessions';
    protected $fillable=[
        'name',
        'picture',
        'creator_id',
        'description',
        'drill_manual',
        'drill_manual_name',
        'court_diagram'
    ];
    protected $rules=[
        'name'=>'required'
    ];
    public static $drill_path='drill_manual/';

    public function practices()
    {
        $query= "
            SELECT *
            FROM practices
            where practices.id in(
                SELECT practice_id
                FROM subsession_practices
                where subsession_id=$this->id
            )
        ";
        $practices= DB::select(DB::raw($query));
        return $practices;
    }
    public function checkConstraints()
    {
        if( !$this->deleteMedia())
            return false;
        foreach($this->practices() as $practice) {
            if(!$practice->delete())
                return false;
        }
        // if(!$this->delete())
        //     return false;
        return true;
    }
    public function preCreate(Request $r)
    {
        $this->creator_id=$this->getLoggedIn()->id;
        return true;
    }
    public function postCreation(Request $r)
    {
        if( $r->has('picture') )
        {
            if (! $this->storePicture($r) )
                return false;
            $this->picture='/media/subsession/'.$this->id;
        } elseif ($r->has('picture_link')) {
            $this->picture = $r['picture_link'];
        }
        if ( $r->has('drill_manual') )
        {
            if (! $this->storeDrill($r))
                return false;
        } elseif ($r->has('drill_manual_link')) {
            $this->drill_manual = $r['drill_manual_link'];
        }
        $this->save();

    }

    public function postUpdate(Request $r)
    {
        if( $r->has('picture') )
        {
            if (! $this->storePicture($r) )
                return false;
            $this->picture='/media/subsession/'.$this->id;
        } elseif ($r->has('picture_link')) {
            $this->picture = $r['picture_link'];
        }
        if ( $r->has('drill_manual') )
        {
            if ( !$this->storeDrill($r))
                return false;
        } elseif ($r->has('drill_manual_link')) {
            $this->drill_manual = $r['drill_manual_link'];
            $this->drill_manual_name= $r['drill_manual_name'];
        }
        if(!$this->save())
            return false;
        return true;
    }

    public function storePicture(Request $r)
    {
        // insert into media library
        $image=$r->file('picture');
        try{
            if($image->storeAs('/subsession/',$this->id))
                return true;
            return false;
        }catch(NotReadableException $e) {
            return false;
        }
    }
    public function storeDrill(Request $r)
    {
        $drill=$r->file('drill_manual');
        if($r->has('drill_manual'))
        {
            if($r->has('drill_manual_name')) 
            {
                $this->drill_manual_name=$r['drill_manual_name'];
            } else {
                try{
                    if($drill!==null) {
                        $name=$drill->getClientOriginalName();
                        $ext=strpos($name,'.pdf')?null:$drill->getClientOriginalExtension();
                        
                        //  check if name is null
                        if($name=='.pdf') {
                            $name=$this->id;
                            if($ext==null) {
                                $ext='.pdf';
                            }else {
                                $ext='.'.$ext;
                            }
                        }
                        Storage::disk()->putFileAs('media_gallery/pdf/',$drill,$name.$ext);
                        Storage::disk()->putFileAs('drill_manual/',$drill,$name.$ext);
                        $this->drill_manual= '/storage/drill_manual/'.$name.$ext;
                        $this->drill_manual_name=$name.$ext;
                        // dd($name);
                        if (!$this->save() )
                            return false;
                    }
                }catch(NotReadableException $e) {
                    $this->drill_manuall_name=$this->id.'.pdf';
                }
            }
        }
        return true;
    }
    public function getDefaultPicture(&$id=null)
    {
        $files=Storage::disk()->files('subsession/default');
        if($files!==null && count($files)>0)
        {
            $randomFile= $files[array_rand($files,1)];

            $id=Session::max('id');
            ($id==null)?$id=1:$id=++$id;
            try{
                if( !Storage::disk()->exists('subsession/'.$id))
                    Storage::disk()->copy($randomFile,'subsession/'.$id);
            }catch(FileExistsException $e) {
                $id++;
                if( !Storage::disk()->exists('subsession/'.$id))
                    Storage::disk()->copy($randomFile,'subsession/'.$id);
            }
            $mainPath='media/subsession/'.$id;
            return $mainPath;
        }else return null;
    }
    public function deleteMedia()
    {
        if( isset($this->drill_manual) ) 
        {
            $drill_path=str_replace('storage/','',$this->drill_manual);
            if(Storage::disk()->exists($drill_path) && !Storage::disk()->delete($drill_path))
                return false;
        }
        if( isset($this->picture) )
        {
            $picture_path= str_replace('storage/','',$this->picture);
            if(Storage::disk()->exists($picture_path) && !Storage::disk()->delete($picture_path) )
                return false;
        }
        return true;
    }
    public function deleteMany(Request $r)
    {
        if(! $r->has('ids'))
            return false;
        
        foreach($r['ids'] as $id)
        {
            $instance= static::find($id);
            if($instance!==null)
            {
                if (!$instance->deleteMedia())
                    return false;
                if (!$instance->delete)
                    return false;
            }
        }
        return true;
    }
    /**
     * @return boolean
     */
    public function generatePdf(Request $r)
    {
        if($r->has('html_to_pdf'))
        {
            $name=($r->has('drill_manual_name'))?$r['drill_manual_name']:$this->id;
            $pdf= \App::make('dompdf.wrapper');
            $pdf->loadHtml($r['html_to_pdf'],'utf8');
            $pdf->setPaper('A4','portrait');
            $path=Storage::disk()->path('drill_manual/');
            $file= $pdf->save($path.'pera.pdf');
                return true;
            return false;
        }
    }
}
