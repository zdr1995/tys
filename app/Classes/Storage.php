<?php
	namespace App\Classes;
	use Storage as FileSystem;

	class Storage extends FileSystem
	{
		/*
		| Override parent cloud static class
		| to quick switch between filesystems
		*/
		public static function cloud()
		{
			return static::getDisk();
		}

		public static function disk($disk = 'local')
		{
			return static::getDisk();
		}

		public static function getDisk($disk = 'local')
		{
			if(config('app.file-driver') == 'cloud')
				return parent::cloud();

			else return parent::disk(config('app.file-driver'));
		}
	}
?>