<?php

namespace App;


class TeamTeamNews extends BaseModel
{
    protected $table= 'team_team_news';
    public $fillable=[
        'team_news_id',
        'team_id'
    ];
    public function teams()
    {
        return $this->hasMany('App\Team', 'id', 'team_id');
    }
    public function teamnews()
    {
        return $this->hasMany('\App\TeamNews','id','team_news_id');
    }
}
