<?php

namespace App;

use DB;
use Intervention\Image\Exception\NotReadableException;
use App\Classes\Storage;
use App\Team;
use App\Organization;
use App\User;
use App\TeamTeamNews;
use Illuminate\Http\Request;

class TeamNews extends BaseModel
{
    protected $table='team_news';
    protected $fillable=[
        'title',
        'message',
        'sender',
        'created_at'
    ];
    protected $guarded=[];

    protected static $storage_path='team_news';

    protected $message;
    protected $ids=[];
    protected $title;
    protected $body;
    protected $data;
    
    protected $teams;

    protected $db;
    public $success;

    public function __construct()
    {
        // $this->db= DB::connection('cassandra');
        // dd($this->db);
    }
    public function teamteamnews()
    {
        return $this->belongsTo(TeamTeamNews::class,'team_news_id');
    }
    public function addData( $title, array $body, array $data)
    {
        $this->title= $title;
        $this->body= $body;
        $this->data= $data;
    }
    public function addUsers(array $users)
    {
        foreach($users as $user)
        {
            //  Handle if is collection
            if( $users instanceof \Illuminate\Support\Collection )
                $this->ids[]=$user->id;
            elseif(is_array($users))
                $this->ids[]= $users;
        }
    }
    public function addOrganizations(array $org_ids)
    {
        $ids= Organization::where('id','in',$org_ids)->get();
        foreach($ids as $org_id)
        {
            $users= User::where('organization_id','=',$org_id)
                                ->orWhere('role','=','superadmin')->get(['id'])->toArray();
            foreach($users as $user_id)
                $this->ids[]= $user_id;
        }
    }
    public function addTeams(array $teams)
    {
        $ids= Team::where('id','in',$teams)->get();
        $this->teams= $teams;
        foreach($ids as $team)
        {
            $user_ids= User::where('team_id','=',$team->id)
                                    ->orWhere('role','=','superadmin')->get(['id'])->toArray();
            
            foreach($user_ids as $user_id)
                $this->ids[]=$user_id;
        }
    }
    public function sendData()
    {
        // $notification= new \App\Notification(
        //     $this->title,
        //     $this->body,
        //     $this->data
        // );
        // $notification->addUsers($this->ids);
        // $notification->send();
        // if ( $notification->isSuccess() )
        //     $this->success= true;
        // else $this->success= false;
        // $this->storeSentData();
    }
    public function storeSentData(array $data,Request $r)
    {
        $url= null;

        $data_to_store=[
            'title'=> $this->title,
            'message'=> $data['message'],
            'sender'=> $this->getLoggedIn()->id,
            'file_type'=> $data['file_type'],
            'url'=> $this->url
        ];
        $id=static::insertGetId($data_to_store);
        // ** ToDo ** Napraviti insert u tabelu team_team_notify
        foreach($this->teams as $team_id)
        {
            $this->teamteamnews()->insert([
                'team_news_id'=> $id,
                'team_id'=> $team_id
            ]);
        }
        $row= static::find($id);
        $row->storeFile($r);
        return $id;
    }
    public function storeFile(Request $r)
    {
        
        if( !$r->has('file'))
            return true;
        
        $file= $r->file('file');
        // if file is video
        $name=$id.'.'.$file->getClientOriginalExtension();
        try {
            $success= Storage::disk()->putFileAs(static::$storage_path,$file,$name);
            $this->url= '/storage/'.static::$storage_path.'/'.$name;
            $this->save();
            
        }catch( NotReadableException $e) {

        }
    }
    public function loadAll()
    {
        $user= $this->getLoggedIn();
        if($user->role=='superadmin')
            $data= $this->loadSuperAdmins();
        else if($user->role=='admin')
            $data= $this->loadAdmins($user);
        else if ($user->role=='trainer')
            $data= $this->loadTrainers($user);

        if(isset($data))
        {
            foreach($data as $row)
            {
                $row->user= (new User)->loadSingle($row->sender);
            }
        }
        return $data;
    }
    public function loadSuperAdmins()
    {
        return static::orderBy('created_at','desc')->get();
    }
    public function loadSuperAdminsMobile($last_id)
    {
        return static::where('id','>',$last_id)
                        ->orderBy('created_at','desc')
                        ->get();
    }
    public function loadAdmins(User $user)
    {
        // load teams in admins organization
        $teams= Team::where('organization_id','=',$user->organization_id)
                            ->pluck('id')
                            ->toArray();
        $data= TeamTeamNews::whereIn('team_id',$teams)
                                    ->pluck('team_news_id')
                                    ->toArray();
        
        return static::whereIn('id',$data)
                        ->orderBy('created_at','desc')
                        ->get();
    }
    public function loadAdminsMobile(User $user,$last_id)
    {
        // load teams in admins organization
        $teams= Team::where('organization_id','=',$user->organization_id)->pluck('id')->toArray();
        $data= TeamTeamNews::whereIn('team_id',$teams)
                                    ->where('team_news_id','>',$last_id)
                                    ->pluck('team_news_id')->toArray();
        
        return static::whereIn('id',$data)
                        ->orderBy('created_at','desc')
                        ->get();
    }
    public function loadTrainers(User $user)
    {
        $user= $this->getLoggedIn();

        $data= TeamTeamNews::where('team_id',$user->team_id)
                                    ->pluck('team_news_id')
                                    ->toArray();

        return static::whereIn('id',$data)
                        ->orderBy('created_at','desc')
                        ->get();
    }
    public function loadTrainersMobile($last_id)
    {
        $user= $this->getLoggedIn();
        $data= TeamTeamNews::where('team_id',$user->team_id)
                                        ->where('team_team_news.team_news_id','>',$last_id)//->toSql();
                                        ->pluck('team_news_id')
                                        ->toArray();
        return static::whereIn('id',$data)
                        ->orderBy('created_at','desc')
                        ->get();
    }
    public function loadAllMobile(Request $r)
    {
        $user= $this->getLoggedIn();
        switch($user->role)
        {
            case 'superadmin':
            {
                $data= $this->loadSuperAdminsMobile($r['id']);
            };break;
            case 'admin':
            {
                $data= $this->loadAdminsMobile($user,$r['id']);
            };break;
            case 'trainer':
            {
                $data= $this->loadTrainersMobile($r['id']);
            };break;
        }
        if(isset($data))
        {
            foreach($data as $row)
            {
                $row->user= (new User)->loadSingle($row->sender);
            }
            return $data;
        }
        else return [];
    }
}