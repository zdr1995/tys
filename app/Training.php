<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Classes\Storage;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;
class Training extends BaseModel
{
    protected $table= 'trainings';
    protected $fillable= [
        'name',
        'picture',
        'icon',
        'public',
        'active',
        'creator_id',
        'publisher_id'
    ];
    // protected $guarded=[];
    protected $hidden=['training_id'];
    protected $rules=[
        'name'=>'required|string',
        'active'=>'required|in:0,1'
    ];
    // protected $hidden=[
    //     '*'
    // ];
    protected static $responseData=[
        'trainings.id as id',
        'trainings.id as training_id',
        'trainings.active',
        'trainings.public',
        'trainings.creator_id',
        'trainings.name as name',
        'trainings.picture as picture',
        'trainings.icon as icon',
        'users.first_name as first_name',
        'users.last_name as last_name',
        'users.role as creator_role'
        // 'users.last_name as creator_last'
    ];

    protected $picture_path='training_picture/';
    protected $icon_path='training_icon/';
    protected function get_default_picture_path()
    {
        return $this->picture_path;
    }
    protected function get_default_icon_path()
    {
        return $this->icon_path;
    }

    public function users()
    {
        $query="
            SELECT DISTINCT u.* from users u,training_relation_models rel 
            WHERE rel.training_id=$this->id 
                AND u.id=rel.creator_id 
            GROUP BY u.id
        ";
        return ( DB::select(DB::raw($query)));
    }
    public function sessions()
    {
        $query="
            SELECT * 
            FROM sessions 
            WHERE sessions.id IN (
                SELECT session_id 
                FROM training_sessions 
                WHERE training_id = $this->id
            )
        ";  
        // return DB::select(DB::raw( "SELECT s.* FROM sessions s,training_sessions rel where rel.training_id=$this->id and s.id=rel.session_id" ));
        $sessions= DB::select(DB::raw($query));
        return $sessions;
    }
    public function validate(Request $r)
    {
        $validation= Validator::make($r->only(['name']), $this->rules);
        // Validate is not maked

        // if($validation->fails())
        //     return false;


        return true;
    }
    public function preCreate(Request $r)
    {
        try{
            $user= JWTAuth::parseToken()->authenticate();
            $this->creator_id=$user->id;
            if (!$user || $user==null)
                return false;
            if($user->role=='user')
                return false;
            if($user->role=='admin' || $user->role=='trainer' )
                $this->public=1;
            if($user->role=='superadmin') {
                $this->public=0;
                $this->active=1;
            }
            // $this->creator_id=$user->id;
        }catch(JWTException $e){
            return false;
        }
        return true;
    }
    public function postCreation(Request $r)
    {
        // dd("tu si");
        // dd($this->id);
        (new Order)->appendNewRow($r,$this->id);

        if (!$this->saveRelatedData($r))
            return false;



        // $image=\Intervention\Image\ImageManagerStatic::make($r['picture']);
        // if(! $image->save($this->get_default_picture_path().$this->id))
        if(! $this->saveNeccessaryData($r))
            return false;

        

        return true;
    }
    public function returnCreated()
    {
        return static::all()->where('id',$this->id)->toJson();
    }
    public function preUpdate(Request $r,$id)
    {
        // $this->active=1;
        // $this->public=0;
        if(!$this->validate($r))
            return false;
        return true;
    }
    public function typeUpdate(Request $r,$id)
    {
        // If has public that mean that publisher id has change
        $relation=static::find($id);
        if($r->has('public')) {
            if($relation->publicUpdate($r,$id))
                return response()->json(['message'=>'success']);
        }
        if($r->has('active')) {
            if( $relation->activeUpdate($r))
                return response()->json(['message'=>'success']);
        }

        if($r->has('name')|| $r->has('picture') || $r->has('icon')) {
            if($this->regularUpdate($r,$id))
                return response()->json(['message'=>'success']);
            else return response()->json('Forbidden',403);

        }

        return 'success';
    }

    public function publicUpdate(Request $r,$id)
    {

        if($this->public==0 && $r['public']==2)
        {
            (new VisibleTraining())->updateFromParents($this,$this->active);
        }
        $this->public=$r['public'];
        $this->publisher_id=$this->getLoggedIn()->id;
        // dd($this);
        if( $r->has('org_ids')&&($this->getLoggedIn()->role=='superadmin') )
        {
            if( !$this->orgVisibleUpdate($r) )
                return false;
        }
        if(static::where('id',$this->id)->update(['public'=>$r['public'],'publisher_id'=>$this->getLoggedIn()->id]))
            return false;
        return true;
    }

    public function activeUpdate($r)
    {
        $this->active=$r['active'];
        $user= $this->getLoggedIn();
        if($this->creator_id==$user->id || $user->role=='admin' || $user->role=='superadmin'){
            static::where('id',$this->id)->update(['active'=>$r['active']]);
        }else{
            // $new_id=null;
            // if(!$new_id=$this->copyRow($r))
            //     return false;
            // $new_training=\App\Training::find(static::find($new_id)->training_id);
            // if(! $this->copyAllTrainings($r,$new_training) )
            //     return false;

            // Use trainings and relate UserTraining__user
            $data=[
                'user_training_id'=> $this->id,
                'user_id'=> $this->getLoggedIn()->id
            ];
        }

        return true;
    }
    /**
     * @param org_ids
     */
    public function orgVisibleUpdate(Request $r)
    {
        //  ToDo make training visible for orgs
        foreach($r['org_ids'] as $org_id)
        {
            $data=[
                'training_id' => $this->id,
                'organization_id'=> $org_id
            ];
            $ids= Visible::insertGetId($data);
            if( !$ids )
                return false;
        }
        return true;
    }

    public function copyAllTrainings(Request $r,Training $newTraining)
    {
        $sessions=TrainingSession::where('training_id',$this->training_id)->get();
        foreach($sessions as $session) {
            if(! $session->copy($r,$newTraining))
                return false;
        }
        $tss=TrainingSessionSubsession::where('training_id',$this->training_id)->get();
        foreach($tss as $row) {
            if(! $row->copy($r,$newTraining))
                return false;
        }
        $tsssp= TrainingSessionSubsessionPractice::where('training_id',$this->training_id)->get();
        foreach($tsssp as $row) {
            if(! $row->copy($r,$newTraining) )
                return false;
        }
        return true;
    }

    public function patch(Request $r,$id)
    {
        // dd($r->all());
        // $self=\App\Training::find(\App\UserTraining::find($id)->training_id);
        // dd($self);
        // dd($this);
        return $this->typeUpdate($r,$id);
    }
    public function postUpdate(Request $r)
    {
        if(!$this->saveNeccessaryData($r))
            return false;
        return true;
    }
    public function saveRelatedData(Request $r)
    {
        $user=$this->getLoggedIn();
        $data=[
            'training_id'=> $this->id,
            'active'=> 1,
            'public'=> 0
        ];
        if($user->role=='admin' || $user->role=='training') {
            $data['active']=1;
            $data['public']=1;
        }
        ($r->has('picture'))?$data['picture']=$this->picture:null;
        ($r->has('icon'))?$data['icon']=$this->icon:null;
        if ($r->has('picture_link')) {
            $data['picture'] = $r['picture_link'];
        }
        if ($r->has('icon_link')) {
            $data['icon'] = $r['icon_link'];
        }
        $user=$this->getLoggedIn()->id;
        if($user)
            $data['creator_id']= $user;
        else $data['creator_id']= null;

        // \App\UserTraining::insert($data);
        $this->fill($data);
        if(! $this->save())
            return false;
        // if (! (new \App\TrainingOrder)->appendToList($this->id,'Training') )
        //     return false;

        return true;
    }


    public function saveNeccessaryData(Request $r)
    {
        if($r->has('picture'))
        {
            if(!$this->savePicture($r))
                return false;
        }
        if($r->has('icon'))
        {
            if(!$this->saveIcon($r))
                return false;
        }

        if ($r->has('picture_link')) {
            $this->picture = $r['picture_link'];
        }
        if ($r->has('icon_link')) {
            $this->icon = $r['icon_link'];
        }


        // if( !$this->typeUpdate($r))
        //     return false;


        // dd("picture",(array)($r['picture']));
        if(!$this->save())
            return false;

        return true;
    }
    public function regularUpdate(Request $r,$user_training_id)
    {
        $loggedIn= $this->getLoggedIn();
        $training=static::find($user_training_id);



        // if creator _id= logged user id than we must update all rows
        // dd($training->creator_id==$loggedIn->id);
        // dd($this);
        // dd($loggedIn);
        if($training->creator_id==$loggedIn->id || $loggedIn->role=='admin' || $loggedIn->role=='superadmin') {
            //  Copy new training
           ($r->has('name'))?$this->name=$r['name']:1;
           if ($r->has('picture') && !$this->savePicture($r))
                return false;
           if ($r->has('icon') && !$this->saveIcon($r))
            return false;
            if ($r->has('picture_link')) {
                $this->picture = $r['picture_link'];
            }
            if ($r->has('icon_link')) {
                $this->icon = $r['icon_link'];
            }

            if(!$this->save())
                return false;
            return true;

        } else {
            // there we create new training with all same data and his creator id
        //     $training->creator_id=$loggedIn->id;
        //     $data=[
        //         'name'=>$this->name,
        //         'picture'=>$this->picture,
        //         'icon'=> $this->icon
        //     ];
        //     $new_training= DB::table('trainings')->insertGetId($data);
        //     // dd($new_training);

        //     $data=$training->attributes;
        //     $data['training_id']=$new_training;

        //     $new_id= DB::table('trainings')->insertGetId($data);
        //     (new TrainingOrder)->appendToList($new_id,'Training');
        //     // $training->save();
        //     return true;
        // }
        return false;
        }

        // if($this->creator_id)
        // if($r->has('name'))
        //     $this->name=$r['name'];
        // if($r->has('picture')) {
        //     $this->savePicture($r);
        // }
        // if($r->has('icon'))
        //     $this->saveIcon($r);

        return 'success';
    }
    public function checkConstraints()
    {
        // foreach( $this->trainings() as $user_relation) {
        //     if (! $user_relation->delete() )
        //         return false;
        // }

        // $training_session= TrainingSession::where('training_id','=',$this->id)->get();
        // foreach($training_session as $session_relation) {
        //     if(!$session_relation->delete())
        //         return false;
        // }

        // // dd($training_session);
        // // dd($this->sessions());
        // foreach($this->sessions() as $single_session) {
        //     // $order= TrainingOrder::where('user_id','=',$session->user_id);
        //     // if(!$order->delete())
        //     //     return false;
        //     $session= Session::find($single_session->id);
        //     // dd($session);
        //     if (!$session->checkConstraints())
        //         return false;
        //     // dd($session->get_subsessions());
        //     foreach($session->subsessions() as $subsession) {
        //         // dd($subsession);
        //         $subsession_instance= Subsession::find($subsession->id);
        //         if(!$subsession->checkConstraints())
        //             return false;
        //     }
        //     if(!$session->delete())
        //         return false;

        // }

        return (new TrainingOrder)->deleteOrderAfterDeleting($this->id);
        // dd("tu si");
        return true;
    }

    public function getDefaultPicture(&$id=null)
    {
        // dd($id==null);
        ($id==null)?$id=$this->id:1;
        $files=Storage::disk()->files('trainings/default_picture');


        if($files!==null && count($files)>0)
        {
            $randomFile= $files[array_rand($files,1)];

            $id=Training::max('id');
            ($id==null)?$id=1:$id=++$id;
            try{
                // dd( !Storage::disk()->exists('training_picture/'.$id) );
                if(!Storage::disk()->exists('training_picture/'.$id))
                    Storage::disk()->copy($randomFile,'training_picture/'.$id);
            }catch(FileExistsException $e) {
                $id++;
                if(!Storage::disk()->exists('training_picture/'.$id))
                    Storage::disk()->copy($randomFile,'training_picture/'.$id);
            }
            $mainPath='media/training_picture/'.$id;
            return $mainPath;
        }else return null;


    }
    public function getDefaultIcon($id=null)
    {
        // dd($id==null);
        ($id==null)?$id=$this->id:1;
        $files=Storage::disk()->files('trainings/default_icon');

        if($files!==null && count($files)>0)
        {
            $randomFile= $files[array_rand($files,1)];
            $mainPath='media/training_icon/'.$id;
            $id= Training::max('id');
            ($id==null)?$id=1:$id=++$id;
            // var_dump($id);
            try{
                if(!Storage::disk()->exists('training_icon/'.$id))
                    Storage::disk()->copy($randomFile,'training_icon/'.$id);
            }catch(FileExistsException $e) {
                // $id=\App\Training::max('id')+1;
                $id++;
                if(!Storage::disk()->exists('training_icon/'.$id))
                    Storage::disk()->copy($randomFile,'training_icon/'.$id);
            }

            $mainPath='media/training_icon/'.$id;
            return $mainPath;   
        }else return null;


    }
    public function loadAll($mobile=false)
    {
        // return $this->loadWithOrders();


        $user=$this->getLoggedIn();
        // // dd($user->role);
        switch($user->role)
        {
            case 'superadmin':
            {
                return $this->loadSuperAdmins($mobile);
            };break;
            case 'admin':
            {
                return $this->loadAdmins($mobile);
            };break;
            case 'trainer':
            {
                return $this->loadTrainers($mobile);
            };break;
            case 'user':
            {
                return $this->loadUsers();
            }
        }
        // return $this->loadWithOrders();
        // return static::all();

    }
    public function respond(Request $r,$mobile=false)
    {
        // return $this->loadAll();
        // $order=new \App\Order;
        // $order->setType('Training');
        // $order->syncIds($r);
        // return $order->getOrder($r,false,$mobile);
        $trainings= $this->getVisible('data',false, $mobile);
        // dd($trainings);
        // dd($mobile);
        if($trainings!==null)
            return response()->json($trainings);
        return response()->json([]);
    }
    public function loadSuperAdmins($mobile=false)
    {

        $trainings= DB::table('trainings')
                            ->join('users',function($join) {
                                $join->on('trainings.creator_id','users.id');
                            });
        if (!$mobile) {
            return response($trainings->get(static::$responseData)->unique()->toArray(),200);
        }

        return response($trainings->where('active','=','1')->get(static::$responseData)->unuque()->toArray(),200);

        // return $this->loadWithOrders();
    }
    public function loadAdmins($mobile= false)
    {
        // return static::all();
        // return DB::table('trainings')->select('*')->get();
        // $ids= User::where('parent_id', $this->getLoggedIn()->id)
        //                 ->orWhere('id',$this->getLoggedIn()->id)
        //                 ->orWhere('role','=','superadmin')
        //                 ->get()
        //                 ->pluck('id');
        // $trainings= DB::table('trainings')->select(static::$responseData)
        //                     ->join('users',function ($join){
        //                         $join->on('users.id','=','trainings.creator_id');
        //                     })
        //                     ->orWhereIn('trainings.creator_id',$ids)
        //                     ->get(static::$responseData);

        $trainings= DB::table('trainings')
                            ->join('users',function($join) {
                                $join->on('trainings.creator_id','users.id');
                            });
        if (!$mobile) {
            return response($trainings->get(static::$responseData),200);
        }

        // $trainings->where('active','=','1');

        // dd($trainings);
        // dd($trainings->unique()->toArray());
        return response($trainings->where('active','=','1')->get(static::$responseData),200);

    }
    public function loadTrainers($mobile=false)
    {
        // if(!$mobile)
        // {
        //     $ids=[];
        //     $ids[]= \App\User::where('id',$this->getLoggedIn()->parent_id)
        //                         ->where('role','=','admin')
        //                         ->get()
        //                         ->pluck('id');
        //     $ids[]= \App\User::where('role','=','superadmin')->get()->pluck('id');
        //     $trainings= DB::table('trainings')->select('*')->select(static::$responseData)
        //                         ->join('users',function ($join){
        //                             $join->on('users.id','=','trainings.creator_id');
        //                             // $join->select('users.first_name as creator');
        //                         })
        //                         // ->orWhereIn('trainings.creator_id',$ids)
        //                         // ->where('trainings.public','=','1')
        //                         ->get(static::$responseData);
        //     return response()->json($trainings);
        // }

            $trainings= DB::table('trainings')
                                    ->join('users',function ($join) {
                                        $join->on('trainings.creator_id','=','users.id');
                                    })
                                        ->where('creator_id',$this->getLoggedIn()->id)
                                        ->orWhere('public','=','2');
            if(!$mobile) {
                return response($trainings->get(static::$responseData)->unique()->toArray(),200);
            }
            // $trainings->where('active','1');
        return response($trainings->where('active','1')->get(static::$responseData)->unique()->toArray(),200);
    }
    public function loadUsers($mobile=false)
    {
        
        return response()->json("Not allowed",403);
    }
    public function loadWithOrders()
    {
        $user= JWTAuth::parseToken()->authenticate();
        $orders=TrainingOrder::where('user_id','=',$user->id)->where('table','=','Training')->first();
        if( !isset($orders)) {
            $ids=static::all()->sortByDesc('created_at')->pluck('id')->toArray();
            // dd($ids);
            $arr=[];
            foreach($ids as $id) {
                $arr[]=$id;
            }
            $arr_str=implode(',',$arr);
            $data=[
                'user_id'=>$user->id,
                'table'=> 'Training',
                'order'=>$arr_str
            ];
            $orders_id=TrainingOrder::insertGetId($data);
            $orders=TrainingOrder::find($orders_id);
        }
        $orders=$orders->order;
        $ordersData=explode(',',$orders);
        $allData=DB::table('trainings')
                                ->get([
                                    'trainings.id as id',
                                    'trainings.id as training_id',
                                    'trainings.name as name',
                                    'trainings.active as active',
                                    'trainings.public as public',
                                    'trainings.creator_id as creator_id',
                                    'trainings.publisher_id as publisher_id',
                                    'trainings.picture as picture',
                                    'trainings.icon as icon'
                                ]);

        foreach($allData as $training) {
            // $picture_path=$training->picture;
            // $training->id=array_pop(explode('/',$picture_path));

            $creator=User::where('id',$training->creator_id)->first();
            $publisher=User::where('id',$training->publisher_id)->first();
            $publisher_name='';
            if(isset($publisher) && $publisher!==null && $publisher!=='null')
                $publisher_name=@$publisher->first_name.' '.@$publisher->last_name;
            $training->first_name=$creator->first_name;
            $training->last_name=$creator->last_name;
            $training->publisher=$publisher_name;
        }
        $return_data=[];
        foreach($ordersData as $order) {
            $row=$allData->where('id',$order)->values();
            if($row->count())
                $return_data[]= $row[0];
            // $index++;
        }
        //  Insert not ordered data
        $existsData=$allData->whereNotIn('id',$ordersData);
        foreach($existsData as $data)
        {
            $return_data[]=$data;
        }
        return $return_data;
        // $items = $allData->sortBy(function($row,$key) use ($ordersData) {
        //     // dd($row);
        //     return array_search($row->id, $ordersData);
        //  });
        // //  dd($allData);
        // //  foreach($items as $item=>$value)
        // //  {
        // //     //  dd($item);


        // //      if(!$returnData->has($item))
        // //      $returnData->push($items->where('id',$item));
        // //  }
        // // dd($items->values());
        // // dd($returnData);
        // // $collection=$items->unique()->values()->all();
        // return $allData;
    }

    public function getVisible($get=false,$usr=false,$mobile=false)
    {
        // return static::orderBy('created_at','desc')->get();
        // if user is passed to this method then check their privileges
        // if not then check logged in user privileges
        ($usr)?$user=$usr:$user= $this->getLoggedIn();
        $query;
        $query=DB::table('trainings')
                                ->join('users',function($join){
                                    $join->on('trainings.creator_id','users.id');
                                });
        // if(count($query->get()) == 0)
        //     return [];
        switch($user->role)
        {
            case 'superadmin':
            {
            };break;
            case 'admin':
            {
                // dd($user->role);
                $data=$query->get(static::$responseData);
                $ids=[];
                foreach($data as $row)
                {
                    // dd($row);
                    $creator=\App\User::where('id','=',$row->creator_id)->first();
                    if($creator->role=='superadmin' && $row->public>0)
                    {
                        $ids[]=$row->id;
                    }
                    elseif( $creator->role=='admin' && ($row->public==2 || $user->id==$row->creator_id) )
                    {
                        $ids[]=$row->id;
                    }
                    elseif( $creator->role=='trainer')
                    { 
                        if($creator->organization_id==$user->organization_id && $row->public==1)
                        {
                            $ids[]=$row->id;
                        }
                        if($row->public==2)
                        {
                            $ids[]=$row->id;
                        }
                        // dd($row);
                    }
                }
                $query=DB::table('trainings')
                                                ->join('users',function($join) {
                                                    $join->on('trainings.creator_id','users.id');
                                                })
                                                ->whereIn('trainings.id',$ids);
            };break;
            case 'trainer':
            {
                $data=$query->get(static::$responseData);
                $ids=[];
                foreach($data as $row)
                {
                    $creator= User::where('id',$row->creator_id)->first();
                    if(($row->creator_id==$user->id)||$row->public==2)
                        $ids[]=$row->id;
                    if($row->public==1 && $user->organization_id==$creator->organization_id)
                        $ids[]=$row->id;
                }
                // dd($ids);
                $query= DB::table('trainings')
                                        ->join('users',function($join){
                                                $join->on('trainings.creator_id','=','users.id');
                                        })->whereIn('trainings.id',$ids);
            };break;
            case 'user':
            {
                return null;
            };break;
        }
        $ids=$this->insertNewOrder($query->get(['trainings.id']));
        $data=[];
        $users_active= VisibleTraining::where('user_id','=',$this->getLoggedIn()->id)->first();
        foreach($ids as $id)
        {
            $data_row= DB::table('trainings')
                            ->join('users',function($join){
                                $join->on('trainings.creator_id','=','users.id');
                            })->where('trainings.id','=',$id);
            
            if($mobile) {
                $data_active=$data_row->where('active','=',1)->get(static::$responseData);
                foreach($data_active as $row)
                {
                    if($row->public==3)
                    {
                        $publish= new Publish($row->id);
                        $user= $this->getLoggedIn();
                        if(!$publish->isVisibleByUser($user) && ($row instanceof \Illuminate\Database\Eloquent\Collection))
                            $row->forget($row);
                    }
                }
                
                if(isset($data_active) && $data_active->count()>0) 
                {
                    if(isset($data_active[0]))
                        $usable_data=$data_active[0];
                    else $usable_data=$data_active;
                
                if($users_active!==null)
                    $this->compareVisible($usable_data,$users_active);

                $data[]= $usable_data;
                }
                    
            } else {
                $usable_data=$data_row->get(static::$responseData)[0];
                if($usable_data->public==3)
                {
                    $publish= new Publish($usable_data->id);
                    $user= $this->getLoggedIn();
                    if($publish->isVisibleByUser($user))
                        $success=true;
                }
                if($users_active!==null)
                    $this->compareVisible($usable_data,$users_active);
                    $data[]=$usable_data;
            }
            // $data[]=$data_active;
        }
//        dd($data);


        ///////////////////////////////////////
        //      Insert new Order            //
        //////////////////////////////////////
        // $ids=$this->insertNewOrder($query->get(['trainings.id']));
        // $data=[];
        // foreach($ids as $id)
        // {

        //     $data[]= DB::table('trainings')
        //                     ->join('users',function($join){
        //                         $join->on('trainings.creator_id','=','users.id');
        //                     })->where('trainings.id','=',$id)->get(static::$responseData)[0];
                                
        // }
        // if($user->role=='superadmin') 
        // {
        //     return $data;
        // }else {
        //     $ids=[];
        //     foreach($data as $row )
        //     {
        //         $creator= \App\User::where('id',$row->creator_id)->first();
        //         //////////////////////////
        //         //     ToDo             //
        //         //////////////////////////
        //         if($row->public==2)
        //             $ids[]=$row->id;
                
        //         if($row->public==1 && $user->organization_id==$creator->organization_id)
        //             $ids[]=$row->id;

        //         if( $row->public==3 )
        //             $this->pickFromVisibleTable($ids);

        //     }
        // }
        // dd($data);
        // $data=
        if($get=='data')    {
            return $data;
        }
                

        switch($get)
        {
            case 'query':
            {
                return $query;
            };break;
            case 'data':
            {
                return $query->get(static::$responseData);
            };break;
            case 'ids':
            {
                return $query->get(['trainings.id']);
            };break;
        }
    }

    public function compareVisible(&$data,$users_visible)
    {
        if($users_visible!==null && $users_visible!=='null')
        {
            $visible_data= json_decode($users_visible->getAttributes()['visible']);
            // dd($visible_data);
            if($visible_data!=='null' && $visible_data!==null)
            {
                foreach($visible_data as $visible)
                {
                    if($visible->training_id==$data->training_id)
                    {
                        $data->active= $visible->active;
                    }
                }
            }
        }
    }

    public function pickFromVisibleTable(array &$ids)
    {
        //$orgs= Visible::where('organization_id','in',)->get();
    }
    public function insertNewOrder($ids)
    {
        // dd($ids);
        $idArr=[];
        foreach($ids as $id)
        {
            $idArr[]=$id->id;
        }
        $db_ids= TrainingOrder::where('user_id',$this->getLoggedIn()->id)->first();
        if($db_ids!==null)
        {
            $this->order=$db_ids->order;
            $order=$this->unpackOrders($db_ids->order);
            // dd($order);
            $exists= array_intersect($order,$idArr);
            // difference matched and all
            $diff= array_diff($idArr,$exists);
            foreach($diff as $id)
                $exists[]=$id;

            $arr=[
                'order'=> $exists
            ];
            $r=new Request($arr);
            (new Order)->insertOrUpdate($r,'Training');
            return $exists;
        } else {
            $superadmin_id= \App\User::where('role','=','superadmin')->first()->id;
            $order= \App\TrainingOrder::where('user_id','=',$superadmin_id)->first();
            if(!$order)
                return [];
//            $order=($order!==null)?$order:$idArr;
            $arr=[
                'order'=> $order->order
            ];
            $r=new Request($arr);
            (new Order)->insertOrUpdate($r,'Training');
            return explode(',',$arr['order']);
        }

        
    }

    public function savePicture(Request $r)
    {
        $image=$r->file('picture');
        if (!Storage::disk()->putFileAs('training_picture',$r->file('picture'),$this->id))
            return false;
        $this->picture='/storage/training_picture/'.$this->id;
        return true;
    }

    public function saveIcon(Request $r)
    {
        $icon=$r->file('icon');
        if(!Storage::disk()->putFileAs('training_icon',$r->file('icon'),$this->id))
            return false;
        $this->icon='/storage/training_icon/'.$this->id;
        return true;
    }

    public function findReal($r,$id)
    {
        return static::find($id);
    }
    public function deleteMany(Request $r)
    {
        foreach($r->json() as $id) {
            $instance=static::where('id','=',$id)->first();
            if($instance!==null)
                if(!$instance->delete())
                    return response()->json(['message'=>'cannot delete']);
        }
        return response()->json(['message'=>'deleted']);
    }
    public function search(Request $r) 
    {
        // dd($this->getVisible('query') );
        $data=$this->getVisible('query');
        if($data instanceof \Illuminate\Database\Query\Builder)
        {
            $data=$data->where('trainings.name','like','%'.$r['q'].'%')->get();
            return response()->json($data);
        }
        return response()->json(['message'=>'not found'],404);
    }
    public function loadSingle($id)
    {
        $data= $this->getVisible('query')->where('trainings.id',$id)->get(static::$responseData);
        if($data->count()>0)
            return $data;
        return 'null';
    }
    public function loadSingleForMobile($id)
    {
        $data= $this->getVisible('query')->where('trainings.id',$id)
                                            ->where('trainings.public','=',1)
                                            ->get(static::$responseData);
        if($data->count()>0)
            return $data;
        return 'null';
    }
    public function isVisibleId($id,User $user)
    {
        $query=DB::table('trainings')
                                ->join('users',function($join){
                                    $join->on('trainings.creator_id','users.id');
                                });
        switch( $user->role )
        {
            case 'trainer':
            {
                $data= $query::get( static::$responseData );
                $ids=[];
                foreach($data as $row)
                {
                    $creator=\App\User::where('id',$row->creator_id)->first();
                    if(($row->creator_id==$user->id)||$row->public==2)
                        $ids[]=$row->id;
                    if($row->public==1 && $user->organization_id==$creator->organization_id)
                        $ids[]=$row->id;
                }
                foreach($ids as $training_id)
                {
                    if( $training_id==$id )
                        return true;
                }
                return false;
            }
            case 'admin':
            {
                $ids=[];
                foreach($data as $row)
                {
                    $creator= User::where('id','=',$row->creator_id)->first();
                    if($creator->role=='superadmin' && $row->public>0)
                    {
                        $ids[]=$row->id;
                    }
                    elseif( $creator->role=='admin' && ($row->public==2 || $user->id==$row->creator_id) )
                    {
                        $ids[]=$row->id;
                    }
                    elseif( $creator->role=='trainer')
                    { 
                        // dd($row);
                    
                        // 
                        if($creator->organization_id==$user->organization_id || $row->public==2)
                        {
                            $ids[]=$row->id;
                        }
                    }
                }
                foreach($ids as $training_id)
                {
                    if( $training_id==$id )
                        return true;
                }
                return false;
            };break;
            
            case 'superadmin':
            {
                return $query->get(['trainings.id']);
            };break;
        }
    }
    public function isVisible($id,User $usr)
    {
        $all=$this->getVisible('ids',$usr)->toArray();
        if($all!==null)
        {
            foreach($all as $training)
            {
                if($training->id==$id)
                    return true;
            }
        }
        return false;
    }

}
