<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserUserTraining extends BaseModel
{
    protected $table='user_user_trainings';
    protected $fillable=[
        'user_id',
        'user_training_id'
    ];
}
