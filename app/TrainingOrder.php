<?php

namespace App;
use Illuminate\Http\Request;

class TrainingOrder extends BaseModel
{
    // tables are 
    // Training,
    // TrainingSession,
    // TrainingSessionSubsession,
    // TrainingSessionSubsessionPractice,
    // SessionSubsessionPractice
    protected $table='training_orders';
    protected $fillable=[
        'user_id',
        'table',
        'order'
    ];

    public function appendToList($newId,$table)
    {
        $data=static::where('table','=',$table)->get();
        // dd($data);
        // dd($table);
        foreach($data as $row)
        {
            $unpackedArray=$row->unpackData();
            // $unpackedArray[]=$newId;
            \array_unshift($unpackedArray,$newId);
            $packedData=\implode(',',$unpackedArray);
            // dd($row);
            $row->order=$packedData;
            if( !$row->save() )
                return false;
        }
        return true;
    }
    public function unpackData()
    {
        return array_unique(\explode(',',$this->order));
    }
    public function packData(array $array)
    {
        $this->order=\implode(',',$array);
    }
    public function insertNewRandomOrder()
    {
        $randomOrder= $this->getRandomOrder();
        $this->order=\implode(',',$randomOrder);
        $this->save();

        return $this->order;
    }
    public function deleteOrderAfterDeleting($training_id)
    {
        $data= static::where('table','=','Training')->get();
        foreach($data as $row) {
            $orders= $row->unpackData();
            // dd($training_id);
            $new_order=[];
            foreach($orders as $order) {
                if($order!=$training_id) {
                    $new_order[]=$order;
                }
            }
            $row->packData($new_order);
            $row->table='Training';
            if( !$row->save())
                return false;
        }
        return true;
    }
    public function insertNewRow(Request $r,$new_id)
    {
        $data=static::where('table','=','Training')->get();
        if($data->count()!=0)
        {
            foreach($data as $row)
            {
                // dd($row);
                $training= Training::where('id',$new_id)->first();
                $user= User::where('id','=',$row->user_id)->first();
                // dd($training->isVisible($new_id,$user));
                // if (!$training->isVisible($new_id,$user))
                //     continue;
                $unpackedArray=$row->unpackData();
                $unpackedArray[]=$new_id;
                $packedData=\implode(',',$unpackedArray);
                // dd($row);
                $row->order=$packedData;
                if( !$row->save() )
                    return false;
            }
            return true;
        }else {
            $data=new static;
            $data->table= 'Training';
            $data->user_id= $this->getLoggedIn()->id;
            $data->order=$new_id;
            if (! $data->save())
                return false;
            return true;
        }
       
    }
    public function updateOrStore(Request $r,$order)
    {
        $row= static::where('user_id','=',$this->getLoggedIn()->id)->first();
        if($row==null) {
            $success=static::updateOrCreate([
                'user_id'=> $this->getLoggedIn()->id,
                'table'=> 'Training',
                'order'=> $order
            ]);
            if($success)
                return true;
            return false;
        }else {
            $row->order= $order;
            if ( !$row->save() )
                return false;
            return true;
        }
    }

    public function newUserOrder(User $user)
    {
        $parents= TrainingOrder::where('user_id','=',$user->parent_id)->first();
        // dd($parents);
        if($parents!==null)
        {
            static::insert([
                'user_id'=> $user->id,
                'table'=> 'Training',
                'order'=> $parents->order
            ]);
        }else {
            $allIds= TrainingOrder::all()->pluck(['id'])->toArray();
            $ids= \implode(',',$allIds);
            static::insert([
                'order'=> $ids,
                'table'=> 'Training',
                'user_id'=> $user->id
            ]);
        }
        return true;
    }


}
