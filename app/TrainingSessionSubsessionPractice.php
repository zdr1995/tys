<?php

namespace App;

use Illuminate\Http\Request;
use App\Classes\Storage;
use Illuminate\Support\Facades\Validator;

class TrainingSessionSubsessionPractice extends BaseModel
{
    protected $table= 'training_session_subsession_practices';
    protected $fillable=[
        'training_id',
        'session_id',
        'subsession_id',
        'practice_id',
        'name',
        'default_picture',
        'practice_video',
        'practice_picture',
        'creator_id',
        'publisher_id',
        'active',
        'public'
    ];
    protected $rules=[
        'training_id'=>'required',
        'session_id'=>'required',
        'subsession_id'=>'required'
    ];
    public function validate(Request $r)
    {
        $validate=\Validator::make($r->only($this->fillable), $this->rules);
        if($validate->fails())
            return false;

        $training=Training::find($r['training_id']);
        $session=TrainingSession::find($r['session_id']);
        $subsession= TrainingSessionSubsession::find($r['subsession_id']);
        $practice= Practice::find($r['practice_id']);
        if( $training==null ||
            $session==null ||
            $subsession==null ||
            $practice==null )
            return false;
        return true;
    }
    public function preCreate(Request $r)
    {
        ($r->has('active'))?$this->active=$r['active']:$this->active=0;
        ($r->has('public'))?$this->public=$r['public']:$this->public=0;
        // $this->public=0;
        if($r->has('name'))
            $this->name=$r['name'];
        else {
            $practice= Practice::find($r['practice_id']);
            if( isset($practice) )
            {
                $this->name= $practice->name;
                $this->practice_picture=$practice->picture;
                $this->practice_video=$practice->video;
                $this->creator_id=$this->getLoggedIn()->id;
                $this->publisher_id=null;
                $this->practice_id=$practice->id;
            } else {
                return false;
            }

        }
        return true;
    }
    public function createModel(Request $r)
    {
        if(!$this->validate($r))
            return response()->json(['message'=>'not found'],404);

        $session_id=TrainingSession::where('id',$r['session_id'])->first()->session_id;
        $subsession_id=TrainingSessionSubsession::where('id',$r['subsession_id'])->first()->subsession_id;
        $training_id= $r['training_id'];

        $instance=new static;
        if(! $instance->preCreate($r) )
            return response()->json(['message'=>'failed'],404);
        $instance->session_id= $session_id;
        $instance->subsession_id=$subsession_id;
        $instance->training_id= $training_id;
        if (!$instance->save())
            return response()->json(['message'=>'failed'],404);
        return response()->json(['message'=>'success']);


    }
    public function postCreation(Request $r)
    {
        // $this->practice_id=($r->has('practice_id'))?$r['practice_id']:null;
        if( $r->has('picture') || $r->has('video') )
        {

            $practice= \App\Practice::find($this->practice_id);
            $this->practice_picture='/media/practice_picture/'.$practice_id;
            $this->practice_video= '/media/practice_video/'.$practice_id.'.mp4';
            if (!$practice->storeMedia($r))
                return false;
        }
    }
    public function preUpdate(Request $r, $id)
    {
        return true;
    }
    public function postUpdate(Request $r)
    {
        return true;
    }
    public function specificUpdate(Request $r)
    {
        return true;
    }

    public function training()
    {
        return Training::where('id',$this->training_id)->first();
    }
    public function session()
    {
        return Session::where('id',$this->session_id)->first();
    }
    public function subsession()
    {
        return Subsession::where('id',$this->subsession_id)->first();
    }
    public function respond(Request $r)
    {

//        $order = new \App\Order;
//        $order->createInstance($r);
//        $order->setType('TrainingSessionSubsessionPractice');
//        return $order->getOrder($r);
        $self = static::where('training_id', '=', $r['training_id'])
            ->where('session_id', '=', $r['session_id'])
            ->where('subsession_id', '=', $r['subsession_id'])
            ->get();
        $data = [];
//        dd($self);
        foreach($self as $instance)
        {
            $practice=Practice::where('id',$instance->practice_id)->first();
//            dd($practice);
            $practice->video_name='Video';
            if($instance!==null && $practice!==null) {
                $instance->practice=$practice;
                $instance->name = $practice->name;
                $instance->picture = $practice->picture;

//            if(!strpos($practice->video,'.mp4') && Storage::disk()->exists($practice->video.'.mp4')) {
//                $instance->video=$practice->video.'.mp4';
//            }else $practice->video=$practice->video.'.mp4';
//            if(!strpos($practice->video,'.MP4') && Storage::disk()->exists($practice->video.'.MP4'))
//                $instance->video=$practice->video.'.MP4';
//            $instance->video= $practice->default_video;
//                dd($instance);
                if (strpos($practice->video, 'media_gallery')) {

                    $video=$practice->video;
                    $video=str_replace('/storage','',$video);
                    if(!Storage::disk()->exists('/practice_video/'.$practice->id.'.mp4'))
                        Storage::disk()->copy($video,'/practice_video/'.$practice->id.'.mp4');
                    $instance->practice->videoPlayer=env('API_LINK').'storage/practice_video/'.$practice->id.'.mp4';
//                    $instance->practice->videoPlayer = 'http://thresholdmedia.com/tysapi/public/'.$instance->videoPlayer;
//                    $instance->practice->videoPlayer= str_replace(' ','%20',$instance->practice->videoPlayer);
                    $instance->video = '/storage/practice_video/'.$practice->id.'.mp4';
                    $instance->videoPlayer = $instance->video;
                    $instance->practice_video = $instance->video;
                } else {
                    $instance->video = '/storage/practice_video/' . $practice->id . '.mp4';
                    $instance->videoPlayer = '/storage/practice_video/'.$practice->id . '.mp4';
                    $instance->practice->videoPlayer = env('API_LINK').$instance->videoPlayer;
                    $instance->practice_video='/storage/practice_video'. $practice->id . '.mp4';
                }
//                dd($instance);
            }
        }
        return $self;
    }
//         $training = \App\Training::where('id', '=', $r['training_id'])->first();
//         $session = \App\Session::where('id', '=', $r['session_id'])->first();
//         $subsession = \App\Subsession::where('id', '=', $r['subsession_id'])->first();
//         $ret['training_name'] = $training->name;
//         $ret['session_name'] = $session->name;
//         $ret['subsession_name'] = $subsession->name;


//         // dd($order->getOrder($r,false));
//         foreach($self as $singleInstance)
//         {
//             $practice=\App\Practice::where('id',$singleInstance->practice_id)->first();
//             $singleInstance->picture=$practice->default_picture;
//             $singleInstance->video=$practice->default_video;
//             $singleInstance->practice_id=$practice->id;
//             $singleInstance->videoPlayer='/storage/practice_video/'.$singleInstance->practice_id.'.mp4';
// //            $singleInstance->videoPlayer=Storage::disk()->path('/practice_video/'.$singleInstance->id.'.mp4');
//             if(isset($practice))
//                 $singleInstance->practice=$practice;
//         }
//         $ret['items'] = $self;
//         return $ret;
//     }
//     public function respond(Request $r)
//     {
// //         if (!$this->validate($r))
// //             return [];
// //         $session_id= \App\TrainingSession::where('id',$r['session_id'])->first()->session_id;
// //         $subsession_id= \App\TrainingSessionSubsession::where('id',$r['subsession_id'])->first()->subsession_id;
// //         $training=\App\Training::where('id',$r['training_id'])->first();
// //         $session= \App\Session::where('id',$session_id)->first();
// //         $subsession= \App\Subsession::where('id',$subsession_id)->first();
// //         $self=static::where('training_id','=',$training->id)
// //                         ->where('session_id','=',$session->id)
// //                         ->where('subsession_id','=',$subsession->id)
// //                         ->get();
// //         $data=[];

// //         $ret['training_name'] = $training->name;
// //         $ret['session_name'] = $session->name;
// //         $ret['subsession_name'] = $subsession->name;


// //         // dd($order->getOrder($r,false));
// //         foreach($self as $singleInstance)
// //         {
// //             $practice=\App\Practice::where('id',$singleInstance->practice_id)->first();
// //             ($singleInstance->practice_picture==null)?$singleInstance->practice_picture=$practice->picture:1;
// //             ($singleInstance->picture==null)?$singleInstance->picture=$practice->picture:1;

// //             $singleInstance->video=$practice->default_video;
// //             $singleInstance->practice_id=$practice->id;
// // //            if($practice->default_video!==null)
// // //                $singleInstance->videoPlayer=$practice->default_video;
// //             if($singleInstance->videoExists())
// //                 $singleInstance->videoPlayer='/storage/practice_video/'.$singleInstance->practice_id.'.mp4';
// //             else $singleInstance->videoPlayer=null;
// // //            $singleInstance->videoPlayer=Storage::disk()->path('/practice_video/'.$singleInstance->id.'.mp4');
// //             if(isset($practice))
// //                 $singleInstance->practice=$practice;
// //         }
// //         $ret['items'] = $self;
// //         return $ret['items'];
//     }
    public function loadSingleInstance(Request $r,$p_id)
    {
        $instance=static::where('training_id','=',$r['training_id'])
                            ->where('session_id','=',$r['session_id'])
                            ->where('subsession_id','=',$r['subsession_id'])
                            ->where('practice_id','=',$p_id)
                            ->first();
        $practice=Practice::where('id',$p_id)->first();
        if($instance!==null && $practice!==null)
        {
            $instance->name=$practice->name;
            $instance->picture= $practice->picture;

//            if(!strpos($practice->video,'.mp4') && Storage::disk()->exists($practice->video.'.mp4')) {
//                $instance->video=$practice->video.'.mp4';
//            }else $practice->video=$practice->video.'.mp4';
//            if(!strpos($practice->video,'.MP4') && Storage::disk()->exists($practice->video.'.MP4'))
//                $instance->video=$practice->video.'.MP4';
//            $instance->video= $practice->default_video;
            if(strpos($practice->video,'media_gallery')){
                $instance->videoPlayer=$practice->video;
                $instance->video=$practice->video;
            }else {
                $instance->video='/storage/practice_video/'.$practice->id.'.mp4';
                $instance->videoPlayer='/storage/practice_video/'.$practice->id.'.mp4';
            }


            return $instance;
        }else return null;
    }

    public function loadById($id)
    {
        $instance= static::find($id);
        if($instance!==null)
        {
            $practice= Practice::where('id','=',$instance->practice_id)->first();
            if( isset($practice) )
            {
                $instance->name= $practice->name;
                $instance->picture= $practice->picture;
                $instance->video_name= (isset($practice->video_name))?$practice->video_name:'Video';
                if(strpos($practice->video,'media_gallery')){
                    try{
                        if(!Storage::disk()->exists('/practice_video/'.$practice->id.'.mp4'))
                        Storage::disk()->copy($practice->video,'/practice_video/'.$practice->id.'.mp4');
                        $instance->videoPlayer='/storage/practice_video/'.$practice->id.'.mp4';
                        $instance->video=$instance->videoPlayer;
                    }catch (\Exception $e){

                    }
                    //                    $instance->videoPlayer=$practice->video;
//                    $instance->video=$practice->video;
                }else {
                    $instance->video='/storage/practice_video/'.$practice->id.'.mp4';
                    $instance->videoPlayer='/storage/practice_video/'.$practice->id.'.mp4';
                }
                return $instance;
            }
            return $instance;

        }else return null;
    }

//     public function destroyInstance(Request $r)
//     {
//         if ( !$this->validate($r) )
//             return response()->json(['message'=>'invalid data'],404);
// //        dd($r->all());
//         $user= $this->getLoggedIn();
//         $self_instance= static::where('training_id','=',$r['training_id'])
//                                 ->where('session_id','=',$r['session_id'])
//                                 ->where('subsession_id','=',$r['subsession_id'])
// //                                ->where('practice_id','=',$r['practice_id'])
//                                 ->get();
// //        dd($self_instance);
//         foreach ($self_instance as $single ) {
//             if($single->id==$r['practice_id'] )
//                 if(! $single->delete() )
//                     return false;
//         }
//         dd("4nd");
//         if( !isset($self_instance) )
//             return response()->json(['message'=>'not found'],404);

// //        if ( !$self_instance->delete() )
// //            return response()->json(['message'=>'cannot delete'],404);

//         return response()->json(['message'=>'deleted']);
//     }
    public function destroyInstance(Request $r)
    {
        if ( !$this->validate($r) )
            return response()->json(['message'=>'invalid data'],404);
        $self=static::find($r['practice_id']);
        if($self==null || !$self->delete())
            return response()->json(['message'=>'not found'],404);
        return response()->json(['message'=>'deleted']);
    }

    public function copy(Request $r,Training $t)
    {
        $newRowData=$this->attributes;
        $newRowData['training_id']=$t->id;
        $newRowData['creator_id']= $this->getLoggedIn()->id;
        $newRow=new static;
        $newRow->fill($newRowData);
        if (!$newRow->save());
            return false;
        return true;
    }
    public function videoExists()
    {
        return Storage::disk()->exists('practice_video/'.$this->id.'.mp4');
    }
    public function createOrDelete(Request $r)
    {
        // dd($r->all());
        if( !$r->has('training_id') ||
            !$r->has('session_id') ||
            !$r->has('subsession_id'))
                return false;

        if ( !$this->cleanSafe($r) )
            return false;

        if( $r['order']!==null && is_array($r['order']))
        {
            $bulk=[];
            $user= $this->getLoggedIn();
            foreach($r['order'] as $practice_id)
            {
                $practice= Practice::where('id','=',$practice_id)->first();
                if( isset($practice_id) )
                {
                    $data=[
                        'creator_id'        => $user->id,
                        'training_id'       => $r['training_id'],
                        'session_id'        => $r['session_id'],
                        'subsession_id'     => $r['subsession_id'],
                        'practice_id'       => $practice->id,
                        'name'              => $practice->name,
                        'active'            => 0,
                        'public'            => 0
                    ];
                    $exists=TrainingSessionSubsessionPractice::where('training_id','=',$r['training_id'])
                                                                    ->where('session_id','=',$r['session_id'])
                                                                    ->where('subsession_id','=',$r['subsession_id'])
                                                                    ->where('creator_id','=',$user->id)
                                                                    ->first();
                    if($exists==null)
                        $bulk[]=$data;
                }
            }
            if( $bulk!=[] )
            {
                $success= static::insert($bulk);
                if( !$success )
                    return false;
                $bulk=[];
            }
        }
        return true;
    }
    public function cleanSafe(Request $r)
    {
        $matched= static::where('training_id','=',$r['training_id'])
                            ->where('session_id','=',$r['session_id'])
                            ->where('subsession_id','=',$r['subsession_id'])
                            ->get();

        foreach($matched as $row)
        {
            if( !$row->delete() )
            {
                return false;
            }
        }
        return true;
    }
    public function multiActions(Request $r)
    {
        if(!$r->has('training_id'))
            return false;
            
        $training= Training::where('id',$r['training_id'])->first();
        if($training->creator_id!==$this->getLoggedIn()->id && $this->getLoggedIn()->role!=='superadmin')
            return false;

        if( !$this->createOrDelete($r))
            return false;

        if( !(new Order)->insertOrUpdate($r,static::class))
            return false;

        return true;
    }
    public function filter(Request $r)
    {
        $query= static::select('id');
        if( $r->has('training_id') )
            $query=$query->where('training_id','=',$r['training_id']);
        if( $r->has('session_id') )
            $query=$query->where('session_id','=',$r['session_id']);
        if( $r->has('subsession_id') )
            $query=$query->where('subsession_id','=',$r['subsession_id']);
        
        $data=[];
        foreach($query->get() as $coll)
        {
            $data[]= (new static)->loadById($coll->id);
        }
        $new_arr=[];
        for( $x=0;$x<count($data);$x++)
        {
            $el=$data[$x];
            $status=0;
            foreach($new_arr as $new)
            {
                if($new->practice_id==$el->practice_id)
                    $status=1;
            }
            if(!$status)
                $new_arr[]=$el;
        }
        // $data= $query->get();

        return array_unique($new_arr);
    }



}
