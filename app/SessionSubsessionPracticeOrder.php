<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionSubsessionPracticeOrder extends Model
{
    protected $table='session_subsession_practice_order';
    protected $guarded=[];
    public function insertNewRow(Request $r,$new_id)
    {
        $subsession_id= TrainingSessionSubsession::where('id','=',$r['subsession_id'])->first()->subsession_id;
        if($subsession_id!==null)
        {
            $instances= parent::insertNewrow($r,$new_id)->where('session_id',$r['session_id'])
                                                        ->where('subsession_id',$subsession_id)
                                                        ->pluck('order');
            foreach($instances as $instance)
            {
                $arr=$instance->unpackOrders();
                $arr[]=$new_id;
                $this->order=$instance->packOrders($arr);
                $this->save();
            }
            return true;
            
        }
        
    }
}
