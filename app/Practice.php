<?php

namespace App;

use DB;
use Exception;
use App\Classes\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Exception\NotReadableException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class Practice extends BaseModel
{
    protected $table= 'practices';
    protected $fillable=[
        'name',
        'creator_id',
        'picture',
        'description',
        'video',
        'video_name'
    ];
    protected static $picture_default_path='practice_picture/default/';
    protected static $picture_path='practice_picture/';
    protected static $video_default_path='practice_video/default/';
    protected static $video_path='practice_video/';
    protected static $screenshot_path='practice_video/screenshots/';
    protected static $video_thumb_path='practice_video/thumb/';
    // Relations
    public function allTrainings()
    {
        return $this->belongsTo('\App\TrainingRelationModel');
    }

    public function getDefaultPicture()
    {
        $files=Storage::disk()->files(static::$picture_default_path);
        if($files!==null && count($files)>0)
        {
            $random_file= $files[ rand(0,count($files)-1) ];
            $path=$this->checkGetAndCopyIfNotExists($random_file,'picture');
            // $randomFile=
            return '/media/'.$path;            
        }else return null;


    }
    public function checkGetAndCopyIfNotExists($random_file,$context)
    {

        $path=($context=='picture')?static::$picture_path:static::$video_path;
        $expected_file=$path.$this->id;
        if(Storage::disk()->exists($expected_file))
            return $expected_file;
        else {
            Storage::disk()->copy($random_file,$expected_file);
        }
        return $expected_file;
    }
    public function getDefaultVideo()
    {
        $files=Storage::disk()->files(static::$video_default_path);

        if($files!==null && count($files)>0)
        {
            $random_file= $files[ rand(0,count($files)-1) ];
            $path=$this->checkGetAndCopyIfNotExists($random_file,'video');
            // $randomFile=
            return '/media/'.$path;            
        }else return null;


    }
    public function preCreate(Request $r)
    {
        $this->creator_id=$this->getLoggedIn()->id;
        return true;
    }
    public function postCreation(Request $r)
    {
        if ($r->has('picture') || $r->has('video') || $r->has('picture_link')||$r->has('video_link') )
        {
            $data=$this->storeMedia($r);

        }
        return true;
    }
    public function storeMedia(Request $r)
    {


//        if( $r->has('picture') )
//        {
//            ($r->has('picture_name'))?$picture_name=$r['picture_name']:$picture_name=$r->file('picture')->getClientOriginalName();
//            $picture=$r->file('picture');
//            $success= Storage::disk()->putFileAs(static::$picture_path,$r->file('picture'),$this->id);
//            try{
//                $path='media/media_gallery/pictures/'.$picture_name;
//                $data=[
//                    'url'=>null,
//                    'path'=>$path,
//                    'name'=>$picture_name,
//                    'type'=>1,
//                    'owner_id'=>$this->getLoggedIn()->id,
//                    'public'=>1
//                ];
//                $id=DB::table('media')->insertGetId($data);
//                $instance=\App\Media::find($id);
//                $instance->url='media/media_gallery/'.$id;
//                $instance->save();
//                $success= Storage::disk()->putFileAs(static::$picture_path,$r->file('picture'),$this->id);
//            }catch(NotReadableException $e){
//                return false;
//            }
//        } elseif ($r->has('picture_link')) {
//            $this->picture = $r['picture_link'];
//        }
//        if($r->has('video'))
//        {
//            ($r->has('video_name'))?$video_name=$r['video_name']:$video_name=$r->file('video')->getClientOriginalName();
//            $video= $r->file('video');
//             Storage::disk()->putFileAs('/media_gallery/videos/',$r->file('video'),$video_name.'.mp4');
//            try{
//                $path='media_gallery/videos/'.$video_name.'.mp4';
//                $data=[
//                    'url'=>null,
//                    'path'=>$path,
//                    'name'=>$video_name,
//                    'type'=>2,
//                    'owner_id'=>$this->getLoggedIn()->id,
//                    'public'=>1
//                ];
//                $id=DB::table('media')->insertGetId($data);
//                $instance=\App\Media::find($id);
//                $instance->url='media/media_gallery/'.$id;
////                $instance->url='/media/'.$path;
//                $instance->save();
//                $this->picture='/media/practice_picture/'.$this->id;
////                $this->video='/media/practice_video/'.$this->id;
//                $this->video='/media/practice_video/'.$this->id.'.MP4';
//
//                if(! $this->save() )
//                    return false;
//                $success= Storage::disk()->putFileAs(static::$video_path,$r->file('video'),$this->id.'.MP4');
//            }catch(NotReadableException $e){
//                return false;
//            }
//        }  elseif ($r->has('video_link')) {
//            $this->video = $r['video_link'];
//        }
//
//        if(!$this->save())
//            return false;
//        return true;
        // dd($r->all());
        if ($r->has('picture')) {
            $picture = $r->file('picture');
            Storage::disk()->putFileAs(static::$picture_path, $picture, $this->id);
            $this->picture = '/storage/'.static::$picture_path.$this->id;
            $this->save();
        }
        if ($r->has('picture_link')) {
            $this->picture = $r['picture_link'];
            $this->save();
        }
        if ($r->has('video')) {
            $video = $r->file('video');
            Storage::disk()->putFileAs(static::$video_path, $video, $this->id.'.mp4');
            $this->video = '/storage/'.static::$video_path.$this->id.'.mp4';
            //$this->makeVideoThumbnail(static::$video_path.$this->id.'.mp4');
            $this->save();
        }
        if ($r->has('video_link')) {
            $this->video = $r['video_link'];
            $video= $r['video_link'];
            $video_path=str_replace('/storage','',$video);
            //$this->makeVideoThumbnail($video_path);
            $this->save();
        }
        // Store video name
        if($r->has('video_name'))
            $this->video_name=($r->has('video_name'))?$r['video_name']:$this->id;
        elseif(!$r->has('video_name') && $r->has('video')) {
            $video=$r->file('video');
            try{
                $this->video_name=$video->getClientOriginalName();
            }catch(FileNotFoundException $e){
                $this->video_name=null;
            }
        }
        if( !$this->save() )
            return false;
        return true;
    }
    public function preUpdate(Request $r,$id)
    {
        return true;
    }
    public function postUpdate(Request $r)
    {
        if($r->has('picture')||$r->has('video')||$r->has('picture_link')||$r->has('video_link'))
        {
            $success=$this->storeMedia($r);
        }

        return true;
    }
    public function checkConstraints()
    {
        if ( !$this->delete_tsssp() )
            return false;
        if (!\strpos($this->picture,'media_gallery') ) {
            $picture_arr=\explode('/',$this->picture);

        if($this->picture==null)
            return true;

            $picture_path=$picture_arr[count($picture_arr)-2].'/'.$picture_arr[count($picture_arr)-1];
            if(Storage::disk()->exists($picture_path) && !Storage::disk()->delete($picture_path))
                return false;
        }
        if(!\strpos($this->video,'media_gallery') && $this->video!=null) {
            $video_arr=\explode('/',$this->video);
            $video_path=$video_arr[count($video_arr)-2].'/'.$video_arr[count($video_arr)-1];
            if(Storage::disk()->exists($video_path) && !Storage::disk()->delete($video_path))
                return false;

        }
        return true;
    }
    public function loadAll()
    {
        $data=static::orderBy('id','desc')->get(); // TODO: Change the autogenerated stub
        foreach ($data as $practice)
        {
            if($practice->video!==null) {
//                if(!strpos($practice->video,'.mp4') && Storage::disk()->exists($practice->video.'.mp4')) {
//                    $practice->video=$practice->video;
                try{
                    if(!Storage::disk()->exists('/practice_video/'.$practice->id.'.mp4'))
                        Storage::disk()->copy($practice->video,'/practice_video/'.$practice->id.'.mp4');
                    $practice->videoPlayer='/storage/practice_video/'.$practice->id.'.mp4';
                    $practice->video=$practice->videoPlayer;
                }catch (\Exception $e){

                }
                    $practice->video_name=(isset($practice->video_name))?$practice->video_name:'Video';
//                }else $practice->video=$practice->video.'.mp4';
//                if(!strpos($practice->video,'.MP4') && Storage::disk()->exists($practice->video.'.MP4'))
//                    $practice->video=$practice->video.'.MP$';

            }

        }
        return $data;
    }
    public function delete_tsssp()
    {
        $relations= TrainingSessionSubsessionPractice::where('practice_id','=',$this->id)->get();
        foreach($relations as $practice)
        {
            if( !$practice->delete() )
                return false;
        }
        return true;
    }
    public function deleteMany(Request $r)
    {
        if( !$r->has('ids') )
        {
            foreach($r['ids'] as $id)
            {
                $instance= static::find($id);
                if($instance!==null)
                {
                    if (!$instance->checkConstraints() )
                        return false;
                    if( !$instance->delete() )
                        return false;
                }
            }
            return true;
        }return false;
    }
//    public function makeVideoThumbnail($video)
//    {
//        $video_path= Storage::disk()->path($video);
//        $output_path= Storage::disk()->path('practice_video/thumb/');
//        $ffprobe=\FFMpeg\FFProbe::create([
//            'ffmpeg.binaries' => env('FFMPEG_BINARIES'),
//            'ffprobe.binaries' => env('FFPROBE_BINARIES'),
//        ]);
//        $duration= $ffprobe->format($video_path)->get('duration');
//        try{
//            $thumbnailStatus=VideoThumbnail::createThumbnail(
//                $video_path,
//                $output_path,
//                $this->id.'.jpeg',
//                $duration/2,
//                1200,
//                800);
//            // dd($thumbnailStatus);
//        }catch(Exception $e){
//            return false;
//        }
//        return true;
//    }
    


}
