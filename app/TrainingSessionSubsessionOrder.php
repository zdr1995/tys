<?php

namespace App;
use Illuminate\Http\Request;

class TrainingSessionSubsessionOrder extends BaseModel
{
    protected $table='training_session_subsession_order';
    protected $fillable=[
        'user_id',
        'training_id',
        'session_id',
        'order'
    ];
    /**
     *   attribute session_id is actually trainingsession_id
     */
    protected $guarded=[];
    // public function checkAndGetOrder(Request $r)
    // {
    //     $user=$this->getLoggedIn();
    //     $row= static::where('user_id','=',$user->id)
    //                     ->where('training_id','=',$r['training_id'])
    //                     ->where('session_id','=',$r['session_id'])
    //                     ->first();
    // }
    public function insertNewRow(Request $r,$new_id)
    {
        $session_id= TrainingSession::where('id','=',$r['session_id'])->first()->session_id;
        if($session_id!==null)
        {
            $instances= parent::insertNewrow($r,$new_id)->where('training_id',$r['training_id'])
                                                            ->where('session_id',$session_id)
                                                            ->pluck('order');
            foreach($instances as $instance)
            {
                $arr=$instance->unpackOrders();
                \array_unshift($arr,$new_id);
                $this->order=$instance->packOrders($arr);
                $this->save();
            }
            return true;
        }
        return false;
    }
    public function setTssIdAttribute($tss_id)
    {
        $this->attributes['session_id']= $tss_id; 
    }
    //  Insert or Update new order
    public function updateOrStore(Request $r, $order)
    {
        $training_id= $r['training_id'];
        $session_id= $r['session_id'];
       
        $rows= static::where('user_id','=',$this->getLoggedIn()->id)
                        ->where('training_id','=',$training_id)
                        ->where('session_id','=',$session_id)
                        ->get();
        if($rows !== null)
        {
            foreach($rows as $row)
            {
                $row->compareIds($r);
            }
            if($rows==null || $rows->count()==0)
            {
                $row=new static;
                if ( !$row->storeNew($r,$order) )
                    return false;
                return true;
            } else {
                $row->training_id= $r['training_id'];
                $row->session_id= $r['session_id'];
                $row->order= $order;
                $row->user_id= $this->getLoggedIn()->id;
                if( !$row->save() )
                    return false;
                return true;
            }
        }
    }
    public function compareIds(Request $r)
    {
        $ord_arr= $this->unpackOrders();
        $cleaned_order=[];
        if($ord_arr==[])
        {
            $this->delete();
        } else {
            foreach($ord_arr as $subsession_id)
            {
                $exists= TrainingSessionSubsession::where('training_id','=',$r['training_id'])
                                                            ->where('session_id','=',$r['session_id'])
                                                            ->first();
                if( $exists )
                    $cleaned_order[]= $subsession_id;
            }
            $diff= array_diff($r['order'],$cleaned_order);

            foreach($diff as $id)
            {
                $cleaned_order[]= $id;
            }
            $this->order= $this->packOrders($cleaned_order);
            if( !$this->save() )
                return false;
            return true;
        }
    }
    public function storeNew(Request $r, $order)
    {
        $row=new static;
        $row->training_id= $r['training_id'];
        $row->tss_id= $r['session_id'];
        $row->user_id= $this->getLoggedIn()->id;
        $row->order= $order;
        if (!$row->save() )
            return false;
        return true;
    }

}
