<?php

namespace App;

use Illuminate\Http\Request;

class About extends BaseModel
{
    protected $fillable=[
        'html',
        'creator_id'
    ];
    public function preCreate(Request $r)
    {
        $user=$this->getLoggedIn();
        $this->creator_id=$user->id;
        return true;
    }
    public function createOrUpdateModel(Request $r)
    {
        $user=$this->getLoggedIn();
        $exists= About::where('creator_id',$user->id)->first();
        if($exists) {
            $exists->html=($r->has('html'))?$r['html']:null;
            if(!$exists->save())
                return false;
            return true;
        }else {
            $this->html=($r->has('html'))?$r['html']:null;
            $this->creator_id=$user->id;
            if( !$this->save() )
                return false;
        }
        return true;
    }
    public function loadForUser()
    {
        $user=$this->getLoggedIn();
        $superadmin=User::where('role','=','superadmin')->first();
        if($user->role=='superadmin')
        {
            $instance=static::where('creator_id',$user->id)->first();
            if(!isset($instance))
            {
                $data=[
                    'creator_id'=>User::where('role','superadmin')->first()->id,
                    'html'=> '<p>This is about page.</p>'
                ];
                return response()->json($data);
            }
            return response()->json($instance);
        }
        else if($user->role=='admin' || $user->role=='trainer')
        {
            $instance=static::where('creator_id',$user->id)->first();
            if(!isset($instance))
            {
                $data=static::where('creator_id',$user->parent_id)->first();
                if($data)
                    return response()->json($data);
                else {
                    $data=static::where('creator_id',$superadmin->id)->first();
                    if($data!==null)
                        return response()->json($data);
                
                    $data=[
                        'creator_id'=>\App\User::where('role','superadmin')->first()->id,
                        'html'=> '<p>This is about page.</p>'
                    ]; 
                    return response()->json($data);
                }
            }else {
                if ($parents=$this->loadParents($user)) 
                    return response()->json($parents);
                
                $data=static::where('creator_id',$superadmin->id)->first();
                if($data!==null)
                    return response()->json($data);

                $data=[
                    'creator_id'=>\App\User::where('role','superadmin')->first()->id,
                    'html'=> '<p>This is about page.</p>'
                ]; 
                return response()->json($data);
            }
        }

        else if($user->role=='user')
        {
            $parent_id=$user->parent_id;
            $superadmin=User::where('role','superadmin')->first()->id;
            $instance=static::where('creator_id',$parent_id)->orWhere('creator_id',User::where('creator_id',$parent_id->parent_id));
            if($instance) {
                return response()->json($instance);
            }else {
                $instance=static::where('creator_id',$superadmin->id)->first();
            }
        }
    }
    public function loadParents($user)
    {
        $data=static::where('creator_id',$user->parent_id)->first();
        if(!isset($data))
            return false;
        return $data;
    }
    
}
