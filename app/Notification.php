<?php

namespace App;

use LaravelFCM\Facades\FCM;
use LaravelFCM\FCMManager;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;

class Notification extends BaseModel
{
    protected $table= 'notifications';
    protected $guarded=[];
    protected $fillable=[
        'training_id',
        'user_ids',
        'seen_ids'
    ];

    public $timestamps= false;
    protected $tokens=[];
    protected $ttl= 60;
    protected $sound= 'default';
    protected $builder;
    public $message_data;
    public $options;
    public $notification;
    public $success;

    public function __construct($title='',$body='',$message_data='')
    {
        $this->title= $title;
        $this->body= $body;
        $this->message_data= $message_data;

        $this->builder= new OptionsBuilder();
        $this->dataBuilder= new PayloadDataBuilder();

        $this->data= $this->builder->build();
    }
    public function setTTL($ttl)
    {
        $this->ttl=$ttl;
    }
    public function setSound(string $sound='default')
    {
        $this->sound= $sound;
    }
    public function addUsers(array $users)
    {
        foreach($users as $user_id)
        {
            if( $user_id instanceof \Illuminate\Support\Collection )
            {
                $token= User::find($user_id->id)->fcm_token;
                if( isset($token) )
                    $this->tokens[]= $token;
            }
            else {
                $tokens= User::find($user_id)->fcm_token;   
                if( isset($tokens) )
                    $this->tokens[]= $tokens;
            }
        }
        // $this->tokens= \array_unique($this->tokens);
    }
    public function setTitle($title='')
    {
        $this->title= $title;
    }
    public function setData($message_data='')
    {
        $this->data= $message_data;
    }
    public function setBody($body='')
    {
        $this->body= $body;
    }
    public function send()
    {
        if( count($this->tokens)>0 )
        {
            //////////////////////////////////////////////////////
            //      Patching FCM token communicate with FCM     //
            //////////////////////////////////////////////////////
            $this->patchTokens($this->tokens);


            //////////////////////////////////////////////////////////
            //      Custom cURL for sending FCM push notificaions   //
            //////////////////////////////////////////////////////////
            try {
                $url='https://fcm.googleapis.com/fcm/send';
                $url= 'http://tys.rs/test';
                // $fcm_tokens=[
                //     "df54VcyyNCA:APA91bH980Et58FkB7hLNsa5nDCuKn0fQ9yg3RcKVbCXhKTvtTGGmtSTGUlrwa5ltSAXOnsQTXqQSuUOcMx-5bhw64TkSz8X7n_bx82qu6Lh-rYZHBJC_7njN9xvLONfEpwe0nBEsnvK"
                // ];
                $fcm_tokens=[];
                foreach(array_unique($this->tokens) as $token) {
                    $fcm_tokens[]= $token;
                }
                $body=[
                    "notification"=> [
                      "body"=> $this->body['message'],
                      "title"=> $this->title,
                      "badge"=> "0",
                      "sound"=> "default"
                    ],
                    "registration_ids"=> $fcm_tokens
                  ];
                  $headers=[
                            'Authorization'=>'key=AAAA891XDOo:APA91bErVNKFMyzK-KkdJpdAUDQTQqd1QSnmbCRevrE6cs9hNK4WoIH2dXpgBBWhtWI2rwBOchY96AeHrlqTTvDe9iGtPOBFKaaC0NVfZ6NTGvBjrGwWVe_axtSDn8e1ljCSc0djUcF8',
                            'Content-Type'=>'application/json'
                  ];
    
                $client= new Client();
                $request= new \GuzzleHttp\Psr7\Request('POST','https://fcm.googleapis.com/fcm/send',$headers,json_encode($body));
               
    
                $res=$client->send($request,[]);
    
                $this->success=true;

            }catch(\Exception $e) {
                \Log::error("Error while sending push notification",(array)$e);
                $this->success= false;
            }



            ////////////////////////////////////////////////////
            //      Brozot FCM not working on IOS             //
            ////////////////////////////////////////////////////

            // try{
            //     $this->insertNecessaryData();



            //     $optionBuilder = new OptionsBuilder();
            //     $optionBuilder->setTimeToLive(60*20)
            //                     ->setContentAvailable(true)
            //                     ->setDryRun(true);
            //                     // ->setPriority('critical');
            //     // dd($this->body);
            //     $notificationBuilder = new PayloadNotificationBuilder($this->title);
            //     $notificationBuilder->setBody($this->body)
            //                         ->setTitle('Test')
            //                         ->setSound('default')
            //                         ->setColor('blue')
            //                         ->setClickAction('none')
            //                         ->setBadge('0');

            //     $dataBuilder = new PayloadDataBuilder();
            //     $dataBuilder->addData([$this->data]);

            //     $option = $optionBuilder->build();
            //     $notification = $notificationBuilder->build();
            //     $data = $dataBuilder->build();


            //     $downstream=FCM::sendTo(
            //         // $this->tokens,
            //         [
            //             'df54VcyyNCA:APA91bH980Et58FkB7hLNsa5nDCuKn0fQ9yg3RcKVbCXhKTvtTGGmtSTGUlrwa5ltSAXOnsQTXqQSuUOcMx-5bhw64TkSz8X7n_bx82qu6Lh-rYZHBJC_7njN9xvLONfEpwe0nBEsnvK',
            //             'df54VcyyNCA:APA91bH980Et58FkB7hLNsa5nDCuKn0fQ9yg3RcKVbCXhKTvtTGGmtSTGUlrwa5ltSAXOnsQTXqQSuUOcMx-5bhw64TkSz8X7n_bx82qu6Lh-rYZHBJC_7njN9xvLONfEpwe0nBEsnvK'
            //         ],
            //         // 'euzWlVVqhpg:APA91bFuDlE7wDJTT_1p8Fs2EunlvOwqgYPi1Nx4JEfQNI1qCz0YCyCVYc_Xd_-3LFJXyHbwwngrCUXZrgh2XjJ4cDbvZS0EPLgZHHvSp10lFQ8oLvGMrTYPAXSKusisrd80j-N9PlHp',
            //         // 'e-ciU5LQVk4:APA91bHu-6djiDYpQPjhJ66tWsrB8NrllM_yDyRiLvpLhPkbF3lSTS3-tNpBPz1uU5geFkwNcTwJbdCUa_-9wuGK8W9PRPEHy8VOrCOZ-J5EhwYdx8ljEtOBuZTiez5g_vv3npmYn7su',
            //         // 'euzWlVVqhpg:APA91bFuDlE7wDJTT_1p8Fs2EunlvOwqgYPi1Nx4JEfQNI1qCz0YCyCVYc_Xd_-3LFJXyHbwwngrCUXZrgh2XjJ4cDbvZS0EPLgZHHvSp10lFQ8oLvGMrTYPAXSKusisrd80j-N9PlHp',
            //         $this->options,
            //         $this->notification,
            //         $this->data
            //     );
            //     // dd($this->tokens,$downstream->numberSuccess());
            //     // dd($downstream->numberSuccess());
            //     if( $downstream->numberSuccess()>0 )
            //         $this->success=true;
            //     else $this->success= false;

            //     $downstream->tokensToDelete();
            //     $downstream->tokensToModify();
            //     dd(($this->tokens));
            //     // dd($downstream->numberSuccess());
            //     // \Log::error("Error while sending FCM message",(array)$e->getMessage());
            // }catch(\Exception $e) {
            //     \Log::error("Error while sending FCM message",(array)$e->getMessage());
            //     // dd("Error while sending FCM message",(array)$e->getMessage());
            //     return false;
            // }
            
        }else {
            $this->success= true;
        }
    }

    public function isSuccess()
    {
        return $this->success;
    }

    public function insertNecessaryData()
    {
        $this->builder->setTimeToLive($this->ttl*60);
        //  Setting title of Notification
        $this->notificationBuilder= new PayloadNotificationBuilder($this->title);
        
        //  Setting body of Notification
        $this->notificationBuilder->setBody($this->body)
                                    ->setSound($this->sound);
        //  And finally add data for the notification
        // dd($this->data);
        $this->dataBuilder->addData($this->data);

        $this->data= $this->dataBuilder->build();
    }

    public function addOrganizations(array $org_ids)
    {
        $user_ids=[];
        foreach($org_ids as $organization)
        {
            $user_ids[]= User::where('organization_id','=',$organization)->get(['id'])->toArray();
        }
        $this->addUsers($user_ids);
    }

    public function addTeams(array $team_ids)
    {
        $user_ids=[];
        foreach($team_ids as $team_id)
        {
            $user_ids[]= User::where('team_id','=',$team_id)->get(['id'])->toArray();
        }
        $this->addUsers($user_ids);
    }

    // Compare seen status and remove people who seen notification
    public static function compareIds($training_id,array &$user_ids,bool $all=false)
    {
        $row= static::where('training_id','=',$training_id)->first();
        if( isset($row) )
        {
            $row->user_ids= json_encode($user_ids);
            $row->save();
            if ( !$all )
            {
                $to_notify= array_intersect($user_ids,(array)json_decode($row->seen_ids));
                $user_ids= $to_notify;
                $row->seen_ids= json_encode($user_ids);
                if ($row->save() )
                    return true;
                return false;
            }else {
                $row->user_ids=$user_ids;
            }
            $row->seen_ids= $row->user_ids;
        }else {
            if($user_ids instanceof \Illuminate\Support\Collection)
            {
                $ids=[];
                foreach($user_ids as $id)
                    $ids[]= $id;
                $user_ids= $ids;
            }
            $user__ids= json_encode($user_ids);
            $seen__ids= json_encode($user_ids);
            $training__id=$training_id;
            // if (!$row->save())
            //     return false;
            $success= static::insert([
                'user_ids'=> $user__ids,
                'seen_ids'=> $user__ids,
                'training_id'=> $training_id
            ]);
            return true;
        }
    }

    public function patchTokens($tokens)
    {
        $headers=[
            'Authorization'=>'key=AAAA891XDOo:APA91bErVNKFMyzK-KkdJpdAUDQTQqd1QSnmbCRevrE6cs9hNK4WoIH2dXpgBBWhtWI2rwBOchY96AeHrlqTTvDe9iGtPOBFKaaC0NVfZ6NTGvBjrGwWVe_axtSDn8e1ljCSc0djUcF8',
            'Content-Type'=>'application/json'
        ];

        $body=[
            'application'=>'com.itcentar.reactnative.tysapps',
            'sandbox'=> false,
            'apns_tokens'=>$tokens
        ];
        $client= new Client();
        $request= new \GuzzleHttp\Psr7\Request('POST','https://iid.googleapis.com/iid/v1:batchImport',$headers,json_encode($body));
        
        $response= $client->send($request,[]);

        try {
            $data=json_decode($response->getBody()->getContents());
            if(isset($data->response))
                $data= $data->response;
            foreach($data as $tokens)
            {
                $table_row= \App\User::where('fcm_token','=',$tokens['apns_token'])->get();
                foreach($table_row as $user)
                {
                    if(\strlen($user->fcm_token)>100)
                    {
                        $user->fcm_token= $tokens['registration_token'];
                        $user->save();
                    }
                }
            }
        }catch(\Exception $e) {
            \Log::error("Patching fcm tokens",(array)$e);
        }
      
        
    }

}
