<?php

namespace App;


class UnsortedVisible extends BaseModel
{
    protected $table= 'unsorted_visible';
    protected $guarded=[];

    public function checkAndDeleteOldData($training_id)
    {
        $rows= static::where('training_id','=',$training_id)->get();
        // dd($training_id);
        // dd($rows);
        if( $rows!==null )
        {
            foreach ($rows as $visible)
            {
                if( !$visible->delete() )
                    return false;
            }  
        }
        return true;
    }

    public function insertIntoTable($user_ids,$training_id)
    {
        foreach($user_ids as $user_id)
        {
            $user= User::where('id','=',$user_id)->first();
            if( $user!==null )
            {
                $org= Organization::where('id','=',$user->organization_id)->first();
                if( $org!==null )
                {
                    $exists= OrganizationVisible::where('training_id','=',$training_id)
                                                    ->where('organization_id','=',$org->id)
                                                    ->first();
                    if( !isset($exists) )
                    {
                        OrganizationVisible::insert([
                            'training_id'=> $training_id,
                            'organization_id'=> $org->id
                        ]);
                    }
                }
            }
        }
    }
}
