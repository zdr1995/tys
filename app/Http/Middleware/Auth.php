<?php

namespace App\Http\Middleware;

use Closure;
use http\Env\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $user=JWTAuth::parseToken()->authenticate();
            if($user)
                return $next($request);
                    // ->header('Access-Control-Allow-Origin','*')
                    //                     ->header('Access-Control-Allow-Methods','*')
                    //                     ->header('Access-Control-Allow-Headers', '*');
        }catch(JWTException $e) {
            return response()->json("Not Authorized",401);
        }
    }
}
