<?php

namespace App\Http\Controllers;

use App\Media;
use App\User;
use App\Training;
use App\Practice;
use App\Classes\Storage;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class MediaController extends BaseController
{
    //

    public function userPath()
    {

    }

    // public function downloadFile(Request $r,$object,$id=null)
    // {
    //     $object='\App\\'.ucfirst($object);
    //     $instance=$object::find($id);
    //     // dd($instance);
    //     if($instance instanceof \App\User) {
    //         // dd("Dobro je");
    //         //  If he has default picture
    //         // if( strpos($instance->profile_picture,'default') )
    //         //     return Storage::disk()->download($instance->profile_picture);
    //         // If he has not have default picture we must check if picture exists
    //         $picture=Storage::disk()->exists( $instance->storage_path.'/'.$instance->id );
    //         // dd($picture);
    //         if($picture)
    //             return Storage::disk()->download( $instance->storage_path.'/'.$instance->id );
    //         return null;
    //     }


    // }

    public function insert(Request $r)
    {
        $media=new Media;
        if( !$media->store() )
            return response()->json(['not saved'],404);
        return response()->json(['message'=>'success']);
    }
    public function downloadFile(Request $r,$type,$id=null)
    {
        $object='\App\\'.ucfirst($type);

        // dd($object);
        if($object=='\App\Users') {
            //  If he has default picture
            $instance = User::find($id);
            // dd($instance->storagePath());
            // if($instance->profile_picture==null || $instance->profile_picture=='null')
                return Storage::disk()->download( '/users/'.$instance->id );
                // return Storage::disk()->download( $instance->profile_picture );
            return null;
        }
        if($object=='\App\Media_gallery')
        {
            // dd("Tu je");
            $instance=\App\Media::find($id);
            if($instance)
                return $this->responseWithMedia($instance);
            return response()->json(['message'=>'Not found']);
        }
        if($type=='training_icon')
        {
            $instance= Training::find($id);
            if($instance)
                return Storage::disk()->download('/training_icon/'.$instance->id);
        }
        if($type=='training_picture')
        {
            $instance= Training::find($id);
            if($instance)
                return Storage::disk()->download('/training_picture/'.$instance->id);
        }
        if($type=='TrainingSessionSubsession')
        {
            // here we have, for example 123
            // 1 is training_id,2 is session_id 3 is subsession_id
        }
        if($type=='practice_picture')
        {
            $instance= Practice::find($id);
            if($instance)
                return Storage::disk()->download('/practice_picture/'.$instance->id);

        }
        if($type=='practice_video')
        {
//            dd($id);
//            header('Content-Type','text/html');
            $instance= Practice::find($id);
            // if($instance)
                // return Storage::disk()->download('/practice_video/'.$instance->id.'.mp4');
//             $file= Storage::disk()->get('/practice_video/'.$inst``ance->id.'.mp4');
//            $path= "../storage/app/practice_video/".$id.'.mp4';
//            header("X-Sendfile: $path");
//            die();
//            $path=
//            return ;
//            return response('https://uc32817bc003246d0987ea184c5d.previews.dropboxusercontent.com/p/hls_playlist/AAmu6A4XqlnFsRscT13LH2BQoJrxajEj4nSNS_J-PNtiwp_6YmYO3nuyApMwMCusdllMexYDanT8kVHpNhRxR2Q-bFPHvrGPOkfYUb5YMvjB9E9QRPvrT3fZ7DqkvQJl8IeWg7bfdOIpR6vcNnqBq1TjpDIUEKnMTDQqxvIjbHcgfK8rrN1Q6Q2RgJofNy8rwrU7yGZuZYeBdb9zJvU8zctFW2e_Ybtia7CcYmJXdZo4yaOnA2_m3iKFQuCvaJBOIC04D5GCowMPRMHIVispe4FxeUAf2TAISQd2_8sUN6ETEfG8tq-79HRtL12NqWrXhwf9fCckcXzm7oHaPvsfmT-ytrNnVQkW8-8xfyeaez8BNq9bMjx1FhEKcak-gzDFZN_obAX1iTdYWu653SR3afuxxffDsPgWeBbzVV2lpE2RbJgE0rYjvzso3F0FRIZP-ELpe_tAiCKrKf-1yu2rik2uYzWjvyMZxIJl00QPnhn35lCe4znE66IrQ8IMaqv3LFIZNtcB3PusZWK5gZ46UU3tCGmSQ9VXL3I6SK4COmfXx15_4itCzZUWLp3Mgb4OVXTXAEuD-DqqkgipWbF6Wh1TvDB58jUKUpcOIC2X82DSVXP4BC67NK6WCSI0KmCmyUlYhpFH5vu4etENEJHNIfVC/p.m3u8?size=1280x720&type=video');
//            return response("https://thresholdmedia.com/tysapi/public/media_gallery/10.mp4");
//            return Storage::disk()->download('/practice_video/'.$id);
            return Storage::disk()->download('/public/practice_video/'.$id.'.mp4');
//             return response()->stream($file,200);
//            return $this->streamVideoDown($path);
        }
        if ($type=='subsession') {
            $instance= \App\Subsession::find($id);
//            dd($instance->picture);
            if($instance)
                return Storage::disk()->download('/subsession/'.$instance->id);
            else return "Not found";
        }
        if ($type=='drill_manual') {
            $instance=\App\Subsession::find($id);
            return Storage::disk()->download('/drill_manual/'.$instance->id.'.pdf');

        }


    }
    public function streamVideoDown($filePath) {
        ob_start();

        if( isset($_SERVER['HTTP_RANGE']) )

            $opts['http']['header']="Range: ".$_SERVER['HTTP_RANGE'];

        $opts['http']['method']= "HEAD";

        $conh=stream_context_create($opts);

        $opts['http']['method']= "GET";

        $cong= stream_context_create($opts);

        $out[]= file_get_contents($filePath,false,$conh);

        $out[]= 'Content-Type:application/mp4';

        ob_end_clean();

        array_map("header",['Content-Type:application/mp4']);

        readfile($filePath,false,$cong);
    }

    public function responseWithMedia($instance)
    {
        if(strpos($instance->path,'lorem'))
            return $this->responseFromNet($instance);
//        dd($instance);
        // if type is video
//        if($instance->type==2) {
//            $file= Storage::disk()->get($instance->path);
////            dd($file);
//            return response()->stream($file,200);
//        }

        return Storage::disk()->download($instance->path);
    }

    public function responseFromNet($instance)
    {
        $image=file_get_contents($instance->path);
        return $image;
    }

    public function downloadByObjectId(Request $r,$object,$default='default',$id=null)
    {
        $object='\App\\'.ucfirst($object);
        $instance=$object::find($id);

        if($instance instanceof \App\User)
        {

        }
    }

    public function loadByType(Request $r,$type,$pagination=false)
    {
        $instance= new Media;
        $query= $instance->loadFiltered($r,$type);
        $data= ($pagination)? $query->paginate(12) : $query->get();
        return $data;
    }

    public function loadDefaults(Request $r,$object,$name)
    {
        $object='users';
        if($object=='users') {
            return Storage::disk()->download('users/default/'.$name);
        }
    }

    public function test_video(Request $r)
    {
        return (new Media)->getRandomVideo();
    }
    public function handleStoragePaths()
    {
        // dd(strpos($_SERVER['REQUEST_URI'],'video'));
        if(strpos($_SERVER['REQUEST_URI'],'videos'))
            return $this->redirectVideoUriToAws();
        
        if(strpos($_SERVER['REQUEST_URI'],'pdf'))
            return $this->redirectPdfUriToAws();

        if(strpos($_SERVER['REQUEST_URI'],'pictures'))
            return $this->redirectPicturesUriToAws();

        if(strpos($_SERVER['REQUEST_URI'],'training_picture'))
            return $this->redirectToAwsTrainingPictureUri();

        if(strpos($_SERVER['REQUEST_URI'], 'training_icon'))
            return $this->redirectToAwsTrainingIconUri();

        if(strpos($_SERVER['REQUEST_URI'], 'practice_picture'))
            return $this->redirectToAwsPracticePictureUri();

        if(strpos($_SERVER['REQUEST_URI'],'practice_video'))
            return $this->redirectToPracticeVideoAwsUri();

        if(strpos($_SERVER['REQUEST_URI'],'drill_manual'))
            return $this->redirectToDrillManualAwsUri();
        
        if(strpos($_SERVER['REQUEST_URI'], 'subsession'))
            return $this->redirectToSubsessionPictureAwsUri();
        // dd(strpos($_SERVER['REQUEST_URI'],'storage')!=false);

        if(strpos($_SERVER['REQUEST_URI'], 'users'))
            return $this->redirectToAwsUserPictureUri();

        if(strpos($_SERVER['REQUEST_URI'],'storage')!=false) {
            $path= $_SERVER['REQUEST_URI'];

        // Media Gallery routes


            // $path= str_replace('storage','storage/app/public',$path);
            // $path=trim($path,'/');
            // $n_uri="http://tysapi-storage.s3-us-east-2.amazonaws.com/".urlencode($path);
            // // dd($path);
            // $location="Location: $n_uri";
            // // dd($location);
            // header("Location:$n_uri");
            // die();
        }
    }
    public function redirectVideoUriToAws()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path,'public'))
        {
            if(!strpos($path,'storage/app/public'))
                $path= str_replace('storage','storage/app/public',$path);
            if(!Storage::disk()->exists($path,'public'))
                abort(404);
        }
        $new_path= env('AWS_STORAGE_URL').$path;
        header('Location:'.$new_path);
        die();
    }

    public function redirectPdfUriToAws()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path,'public'))
        {
            if(!strpos($path,'storage/app/public'))
                $path=str_replace('storage','storage/app/public', $path);
            
            if(!Storage::disk()->exists($path, 'public'))
                abort(404);
        }
        $new_path= env('AWS_STORAGE_URL').$path;
        header('Location:'.$new_path);
        die();
    }

    public function redirectPicturesUriToAws()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path,'public'))
        {
            if(!strpos($path, 'storage/app/public'))
                $path= str_replace('storage','storage/app/public', $path);

            if(!Storage::disk()->exists($path, 'public'))
                abort(404);
        }

        $new_path= env('AWS_STORAGE_URL').$path;
        header('Location:'.$new_path);
        die(); 
    }

    public function redirectToPracticeVideoAwsUri()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path,'public'))
        {
            // if not exists .mp4 add 
            if(!strpos($path,'.mp4'))
                $path.= '.mp4';

            if(Storage::disk()->exists($path,'public')) 
            {
                header("Location:$path");
                die();
            }

            if(!strpos($path,'storage/app/public'))
                $path= str_replace('storage', 'storage/app/public', $path);

            if(!Storage::disk()->exists($path,'public'))
                abort(404);
        }
        $new_path= env('AWS_STORAGE_URL').$path;
        header("Location:$new_path");
        die();
    }

    public function redirectToAwsPracticePictureUri()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path, 'public'))
        {
            // dd($path);
            if(!strpos($path, '.jpg'))
            {
                if(Storage::disk()->exists($path.'.jpg', 'public'))
                {
                    $path= env('AWS_STORAGE_URL').$path.'.jpg';
                    header("Location:$path");
                    die();
                }else {
                    if(Storage::disk()->exists($path, 'public'))
                    {
                        $path= env('AWS_STORAGE_URL').$path;
                        header("Location:$path");
                        die();
                    }
                }
            }
            if(!strpos($path, 'storage/app/public'))
                $this->replaceWithPublicPathUri($path);
            
            if(Storage::disk()->exists($path.'.jpg', 'public'))
                $path= $path.'.jpg';

            if(Storage::disk()->exists($path, 'public'))
            {
                $path= env('AWS_STORAGE_URL').$path;
                header("Location:$path");
                die();
            }
        }
        $path= env('AWS_STORAGE_URL').$path;
        header("Location:$path");
        die();
    }

    public function redirectToAwsTrainingPictureUri()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path,'public'))
        {
            if(!strpos($path,'.jpg'))
                if(Storage::disk()->exists($path.'.jpg','public'))
                {
                    $path= env('AWS_STORAGE_URL').$path.'.jpg';
                    header("Location:$path");
                    die();
                }

            if(!strpos($path,'storage/app/public'))
                $this->replaceWithPublicPathUri($path);
            
            if(!Storage::disk()->exists($path, 'public'))
                abort(404);
        }
        $path= env('AWS_STORAGE_URL').$path;
        header("Location:$path");
        die();
    }

    public function redirectToAwsTrainingIconUri()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path, 'public'))
        {
            if(!Storage::disk()->exists($path,'public'))
            {
                if(!strpos($path,'.jpg'))
                    if(Storage::disk()->exists($path.'.jpg','public'))
                    {
                        $path= env('AWS_STORAGE_URL').$path.'.jpg';
                        header("Location:$path");
                        die();
                    }
    
                if(!strpos($path,'storage/app/public'))
                    $this->replaceWithPublicPathUri($path);
                
                if(!Storage::disk()->exists($path, 'public'))
                    abort(404);
            }
        }
        $path= env('AWS_STORAGE_URL').$path;
        header("Location:$path");
        die();
    }

    public function redirectToDrillManualAwsUri()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path, 'public'))
        {
            $ext_path= (strpos($path,'.pdf'))?$path:$path.'.pdf';
            if($path!= $xt_path && Storage::disk()->exists($path,'public'))
            {
                $path= env('AWS_STORAGE_URL').$path;
                header("Location:$path");
                die();
            }

            $this->replaceWithPublicPathUri($path);
            if(!Storage::disk()->exists($path,'public'))
                abort(404);
        }
        $path= env('AWS_STORAGE_URL').$path;
        header("Location:$path");
    }

    public function redirectToSubsessionPictureAwsUri()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path, 'public'))
        {
            // check if exists pic with extension
            if(!strpos($path, '.jpg'))
            {
                if(Storage::disk()->exists($path.'.jpg', 'public'))
                {
                    $new_path= env('AWS_STORAGE_URL').$path.'.jpg';
                    header("Location:$new_path");
                    die();
                }
            }
            if(!strpos($path,'storage/app/public'))
                $this->replaceWithPublicPathUri($path);
            if(!Storage::disk()->exists($path, 'public'))
                abort(404);
        }
        $path= env('AWS_STORAGE_URL').$path;
        header("Location:$path");
        die();
    }

    public function redirectToAwsUserPictureUri()
    {
        $path= $_SERVER['REQUEST_URI'];
        $this->removeSlashesFromUri($path);
        if(!Storage::disk()->exists($path, 'public'))
        {
            if(!strpos($path,'storage/app/public'))
                $this->replaceWithPublicPathUri($path);

            if(!Storage::disk()->exists($path,'public'))
                abort(404);
        }
        $path= env('AWS_STORAGE_URL').$path;
        header("Location:$path");
        die();
    }

    public function removeSlashesFromUri(&$path='')
    {
        if($path=='')
            $path= $_SERVER['REQUEST_URI'];

        if(strpos($path,'///')) {
            $path=str_replace('///','/',$path);
        }else if(strpos($path,'//')) {
            $path=str_replace('//','/',$path);
        }

        $path= trim($path, '/');
        $path=urldecode($path);
    }

    public function replaceWithPublicPathUri(&$path)
    {
        $path= str_replace('storage','storage/app/public', $path);
    }
}
