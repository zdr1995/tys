<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Publish;
class PublishController extends BaseController
{
    public function changePublishedStatus(Request $r)
    {
        if( !$r->has('training_id'))
        {
            return response()->json(['message'=>'Not found'],404);
            die();
        }
        $training_id= $r['training_id'];
        $publish= new Publish($training_id);
        if( !$publish->changeState($r) )
            return response()->json(['message'=>'failed'],404);

        return response()->json(['message'=>'success'],200);
    }
    public function checkVisibilityForTraining(Request $r)
    {
        if( !$r->has('training_id'))
        {
            return response()->json(['message'=>'Not found'],404);
        }
        $publish=new Publish($r['training_id']);
        $orgs= $publish->loadPublishByTrainingId();
        if(isset($orgs))
            return $orgs;
        return [];
    }
}
