<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:api',['except'=> ['login']]);
    }
    public function login(Request $r)
    {
        if(!$r->has('hash'))
        {
            $user= User::where('username','=',$r['email'])->orWhere('email','=',$r['email'])->first();
            if($user)
            {
                $creditials= [
                    'email'=>$user->email,
                    'password'=>$r['password']
                ];
                if ( !$token= JWTAuth::attempt($creditials) ) {
                    return response()->json(['message'=>'Unauthorized'],401);
                }else{
                    $user->token=$token;
                    return response()->json($user);
                }
            }else {
                return response()->json(['error'=>'not found'],404);
            }
        }else {
            if($r->has('hash'))
            {
                $user=UserController::getHashedUser($r);
                if( isset($user) && ($user->identifier!=='null' && $user->identifier!==null) )
                {
                    $user->password=\Hash::make($r['hash']);
                    if($user->save())
                    {
                        // dd($user);
                        $creditials= [
                            'email'=>$user->email,
                            'password'=>$r['hash']
                        ];
                        if ( !$token=JWTAuth::attempt($creditials) ) {
                            return response()->json(['message'=>'Unauthorized'],401);
                        }else{
                            $user->token=$token;
                            return response()->json($user);
                        }
                    } else {
                        return response()->json('not complete');
                    }
                } else {
                    return response()->json('Already registered');
                }
                
            }else {
                return response()->json('Unauthorized',401);
            }
       
    }
        
    }
    public function handle($request,\Closure $next)
    {
    }
}
