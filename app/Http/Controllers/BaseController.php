<?php

namespace App\Http\Controllers;

use App\File;
use App\Chat;
use App\Help;
use App\About;
use App\Training;
use App\TeamNews;
use App\BaseModel;
use App\Organization;
use App\Classes\Storage;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\Console\Output\BufferedOutput;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use DB;

class BaseController extends Controller
{
    public static function makeInstance($resource)
    {
        return '\App\\'.ucfirst($resource);
    }
    public function downloadFile(Request $r,$name,$id=null)
    {
        $file=new File();
        return $file->download($name);
    }
    public function create(Request $r,$resource)
    {
        \Log::info("Zahtjev za create",(array)$r->all());
        $model=$this->makeInstance($resource);
        //  Instance of model that we creating
        $instance=new $model;
        if( !$instance->validate($r))
            return response()->json(['message'=>'You must input all fields']);

        if($resource=='about' || $resource=='help') {
            if(! $instance->createOrUpdateModel($r) )
                return response()->json(['message'=>'failed'],404);
            return response()->json(['message'=>'successifully']);

        }
        if($resource=='TrainingSessionSubsession' ||
            $resource=='TrainingSessionSubsessionPractice') {
            if(! $instance->createModel($r))
                return response()->json(['message'=>'failed'],404);
            return response()->json($instance->getLastInserted());
        }


        if (! $instance->preCreate($r) )
            return response()->json(['message'=>'Not allowed'],403);


        $instance->fill($r->only($instance->fillable));

        if(! $instance->save() )
            return response()->json(['message'=>'Invalid data']);

        if(! $instance->postCreation($r))
            return response()->json('saved but not complete');

        if($instance instanceof \App\Media)
            return $instance->returnCreated();

        return response()->json($instance->getLastInserted());
            $resp=[];
            $resp[]= $instance->returnCreated();

            return $resp;
            // return response()->json($instance::find($instance->id)->attributes);
        // return $instance->returnCreated();


    }
    public function patch(Request $r,$resource,$id)
    {
        $model=$this->makeInstance($resource);
        $instance= $model::find($id);

        if($resource=='training') {
            if(($r->all())!==null)
            {
                try{
                    $request=  \json_decode(\file_get_contents("php://input"));
                    if ( $request->public!==null)
                    {
                        $req= new Request([
                            'public'=> $request->public
                            ]);
                        $instance=$instance->findReal($req,$id);
                        if($instance)
                            return $instance->patch($req,$id);
                    }else {

                    }
                }catch(\Exception $e) {
                    // return response()->json(['error'=>'not found'],404);
                    $instance= $instance->findReal($r,$id);
                    if($instance)
                        return $instance->patch($r,$id);
                    else return response()->json($r,$id);
                }

            }
            $instance=$instance->findReal($r,$id);
            if($instance)
                return $instance->patch($r,$id);
            else return response()->json(['error'=>'not found'],404);
        }
        if( !$instance->preUpdate($r,$id) )
            return response()->json(['message'=>'Invalid data'],404);

        if( $instance->specificUpdate($r)=='success')
            return response()->json(['message'=>'success']);

        if( $instance->specificUpdate($r)=='failed' )
            return response()->json(['message','failed'],404);

        if($instance)
        {
            $instance->fill($r->all());
            if($instance->save())
            {
                if(!$instance->postUpdate($r))
                    return response()->json(['message'=>'saved but failed']);
                if($resource=='User' || $resource=='user')
                    return response()->json($instance->loadSingle($instance->id));
                return response()->json($instance);
            }
            return response()->json(['message'=>'Error']);
        } else {
            return response()->json(['message'=>'Not found'],404);
        }

    }
    public function specificPatch(Request $r)
    {
        $fullPath=$r->path();
        $path= \parse_url($_SERVER['REQUEST_URI']);
        $class= explode('/',$path['path'])[2];
        $instance= $this->makeInstance($class);

        return (new $instance)->patchInstance($r);
    }
    public function destroy(Request $r,$resource,$id)
    {
        \Log::info("Zahtjev za delete: ".$resource,(array)$r->all());
        $model=$this->makeInstance($resource);
        $instance=$model::find($id);
        
        if($resource=='training') 
        {
            $relation=Training::find($id);
            $user=(new BaseModel)->getLoggedIn();
            if($user->role=='admin' || $user->role=='superadmin' || $user->id==$relation->creator_id)
            {
                if( !$instance->checkConstraints() )
                    return response()->json(['message'=>'cannot delete,constraint fails']);
                if(!$relation->delete())
                    return response()->json(['message'=>'Not found'],404);
                return response()->json(['message'=>'deleted']);
            }else {
                return response()->json(['message'=>'not allowed'],403);
            }

        }
        else if($resource=='Team' || $resource=='Organization') 
        {
            if($instance instanceof \App\Team || $instance instanceof \App\Organization)
            {
                $chatkit= Chat;
                ($resource=='Team')?$chatkit->deleteTeamRoom($instance):$chatkit->deleteOrganizationRoom($instance);
            }
        }
        else if($resource=='TrainingSessionSubsessionPractice') 
        {
        }
        if($instance)
        {
            if( !$instance->checkConstraints() )
                return response()->json(['message'=>'cannot delete,constraint fails']);

            if($resource=='User' || $resource=='user')
            {
                $instance->deleteEmptyOrganizations();
                $instance->deleteEmptyTeams();
                if($instance->isChatEnabled())
                {
                    $chat= new Chat();
                    $chat->deleteUser($instance);    
                }
            }
            if( !$instance->delete() )
                return response()->json(['message'=>'Not fully deleted']);
            return response()->json(['message'=>'deleted']);
        }else return response()->json(['message'=>'not found'],404);
    }
    public function destroyData(Request $r,$resource)
    {
        $model=$this->makeInstance($resource);
        $instance= new $model;
        return $instance->destroyInstance($r);

    }

    public function freshMigrations()
    {
//        $message=new BufferedOutput();
//        $message= new \Symfony\Component\Console\Output\BufferedOutput();
        Artisan::call('migrate:fresh',['--force'=>true]);
        Artisan::call('db:seed');
        return response()->json(Artisan::output());
    }

    public function all(Request $r,$resource,$paginate=false,$mobile=false)
    {
        // \Log::info('Request: ',(array)($r->all()));
        $model=$this->makeInstance($resource);
        $instance=new $model;
        if($r->has('training_id')|| $r->has('session_id') || $r->has('subsession_id')|| $resource=='trainingSession' || $resource=='TrainingSession')
            return $this->respondWithRelations($r,$model);
        if($resource=='training' || $resource=='Training') {
            $response=$instance->respond($r,$mobile);
            return $response;
        }
        if(isset($response))
            return response()->json($response);

        if($resource=='practice') {
            return response()->json($instance->loadAll());
        }
        if($resource=='TeamNews' || $resource=='teamNews')
        {
            $data= $instance->loadAll($r,$mobile);
            return response()->json($data);
        }
           
        $data=$instance->loadAll();
        // dd($data);
        if($resource=='user')
            return response()->json($data);
        
        // for media resources paginate is 12 images
        if($paginate && $resource=='media')
            return $data->paginate(12);
        if($paginate)
            return $data->paginate(10);
        if($data instanceof Collection)
            return response()->json($data);
        return response()->json($data->get());
    }


    public function single(Request $r,$resource,$id)
    {
        $model= $this->makeInstance($resource);
        $instance= new $model;
        return response()->json($instance->loadSingle($id));

    }

    public function loadTeams(Request $r,$id)
    {
        $user= User::find($id);
        return response()->json($user->teams);
    }

    //  --Custom paginate method--  //
    public function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null)
    {

        // if($pageName==undefined)
        //     $pageName=0;
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems->toArray(),
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }
    public function test(Request $r,$resource,$paginate=false)
    {
        $model=$this->makeInstance($resource);
        $instance=new $model;
        $allData=$instance::all();

        return $this->paginateCollection($allData,10,'page',null);

    }
    public function hashMail(Request $r)
    {
        // $hash=\password_hash($r['password'],5);
        // return response()->json($hash);
        $user=User::where('email',$r['email'])->first();
        // dd($user);
        if(isset($user))
        {
            $succ= \Hash::check($r['password'],$user->password);
            return response()->json(['succ'=>$succ]);
        }
    }
    public function respondWithRelations(Request $r,$model)
    {
        $instance=new $model;
        return $instance->respond($r);
    }
    public function parents(Request $r,$id)
    {
        $user=User::find($id);
        $parents=$user->user_parents();
    }
    public function childrens(Request $r,$id)
    {
        $user=User::find($id);
        $childrens=$user->user_childrens();
    }

    public function loadAbout()
    {
        $about= new About;
        return $about->loadForUser();
    }
    public function loadHelp()
    {
        $help= new Help;
        return $help->loadForUser();
    }
    public function runMigrations()
    {
        $output= new \Symfony\Component\Console\Output\BufferedOutput;
        Artisan::call('migrate:fresh',(array)$output);
        return Response::json(['migrate:fresh'=>$output]);
    }
    public function runSeeder()
    {
        $output2=new BufferedOutput;
        Artisan::call('db:seed',(array)$output2);
        return Response::json(['db:seed'=>$output2]);
    }
//    public function storage()
//    {
//        return response()->json("Success");
//    }
    public function destroyMany(Request $r,$resource)
    {
        $model= $this->makeInstance($resource);
        $instance= new $model;
        if($instance->deleteMany($r))
            return response()->json(['message'=>'success']);
        return response()->json(['message'=>'failed'],500);
    }
    public function searchQuery(Request $r,$resource)
    {
        $model= $this->makeInstance($resource);
        $instance= new $model;
        return $instance->search($r);
    }
    public function multiActions(Request $r,$resource)
    {
        $model= $this->makeInstance($resource);
        $instance= new $model;
        $success=0;
        $failed=[];
        if($r->has('order'))
        {
            // To Do
            if ( !$instance->multiActions($r) )
                return response()->json(['message'=>'not completed'],404);
        }elseif( $resource=='user'|| $resource=='User' ) {
            $users=$instance->multiCreate($r);
            if( $users===null)
                return response()->json(['message','failed'],404);
            return response()->json($users);
        }
        return response()->json(['message'=>'success']);
    }
    public function nestedOrganizations(Request $r)
    {
        $orgs= new Organization();
        $organizations= $orgs->loadOrgsNestedByUser();
        if( !isset($organizations) )
            return response()->json(['message'=>'not found'],404);
        return response()->json($organizations);
    }
    public function loadNewsMobile(Request $r)
    {
        if( !$r->has('id') )
            return response()->json(['message'=>'Not found'],404);

        if($r['id']=='0')
        {
            $news= (new TeamNews())->loadAll();
            return response()->json($news);
        }
        
        $news= (new TeamNews())->loadAllMobile($r);
            return response()->json($news);
    }
    public function storageLink(Request $r)
    {
        Artisan::call('storage:link',[]);
    }
    public function addColumn()
    {
        $pdo= DB::getPdo();
////        $path=Storage::disk()->path('media_gallery/videos');
////        $data=shell_exec("mkdir $path/thumb");
////        return $data;
    //    $pass= \Hash::make('user1234');
    //    $user=\App\User::where('id',343)->first();
    //    $user->password= $pass;
    //    $user->save();
    // $query="CREATE TABLE unsorted_visible (
    //         id INT AUTO_INCREMENT NOT NULL,
    //         user_id INT UNSIGNED,
    //         training_id INT UNSIGNED,
    //             PRIMARY KEY(id),
    //             FOREIGN KEY (user_id)
    //                 REFERENCES users(id) 
    //                 ON UPDATE CASCADE
    //                 ON DELETE CASCADE,
    //             FOREIGN KEY (training_id)
    //                 REFERENCES trainings(id)
    //                 ON UPDATE CASCADE
    //                 ON DELETE CASCADE
    //             )";
    //     $success= $pdo->query($query);
    //     return 'success';
    
    // $query="CREATE TABLE visible_trainings (id INT AUTO_INCREMENT NOT NULL,user_id INT NOT NULL, visible TEXT,PRIMARY KEY(id)) ENGINE=INNODB";
    // $pdo->query($query);
//        $query="SELECT * FROM users WHERE id=243;";
//        $data= DB::select(DB::raw($query));
//        return response()->json($data);
        // $query="DELETE FROM training_orders WHERE user_id=343";
        // $pdo->query($query);
    //    $query="SELECT * FROM training_orders WHERE user_id=2";
//        $data= DB::select(DB::raw($query));
        $data= "CREATE TABLE team_publish(id INT AUTO_INCREMENT NOT NULL,team_id INT UNSIGNED,training_id INT UNSIGNED,PRIMARY KEY(id))";
        $success= $pdo->query($data);

        // $query="ALTER TABLE team_publish ADD FOREIGN KEY(team_id) REFERENCES teams(id) ON UPDATE CASCADE ON DELETE CASCADE";
        // $pdo->query($query);

        // $query="ALTER TABLE team_publish ADD FOREIGN KEY(training_id) REFERENCES trainings(id) ON UPDATE CASCADE ON DELETE CASCADE";
        // $pdo->query($query);



        return '$data';

//        dd($path);
        //        return 'dsgfsd';
//        $users=\App\User::where('organization_id','=',19)->get();
//        $ffmpeg=\FFMpeg\FFMpeg::create([
//            'ffmpeg.binaries'=> env('FFMPEG_BINARIES'),
//            'ffprobe.binaries'=> env('FFPROBE_BINARIES')
//        ]);
//        dd($ffmpeg);
        $ord=[];
//        foreach ($users as $user)
//        {
//            $data=[
//                'user_id'=>$user->id,
//                'table'=>'Training'
//            ];
//            $data=\App\TrainingOrder::where($data)->get();
//            if($data->count()>1)
//            {
//                $ord=$data[0];
//                $row=\App\TrainingOrder::where('id',$ord->id)->first();
//                $row->delete();
//            }
//        }
//        $query="UPDATE users set organization_id=19 WHERE id=44";
//        $pdo->query($query);
//        return 'success';


//        $query="SELECT *
//                from training_orders
//                join users on training_orders.user_id=users.id
//                where users.organization_id=19
//                AND training_orders.order not like '94,84,85';";
////        dd($query);
/// 
//        $order=DB::select(DB::raw($query));
//        return $order;
//        return $users;
        // $this->copy_ts(97,114);
        // $this->copy_tss(97,114);
        // $succ=$this->copy_tsssp(97,114);

        // $ct_query="CREATE TABLE IF NOT EXISTS `visible` (
        //                                                     `id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
        //                                                     `organization_id` TEXT(255),
        //                                                     `training_id` INT(11),
        //                                                     PRIMARY KEY (`id`)
        //                                                 ) ENGINE=InnoDB;";
        //     $query="ALTER TABLE `media` ADD COLUMN `video_thumbnail` VARCHAR(255) DEFAULT NULL";
        // $succ=$pdo->query($query);

//        $query="ALTER TABLE teams ADD COLUMN organization_id INT DEFAULT NULL;";
//        $query="ALTER TABLE trainings MODIFY icon VARCHAR(255) DEFAULT NULL";
//        $query= "DELETE FROM training_orders WHERE 1";

        // $query="ALTER TABLE users MODIFY first_name VARCHAR(255) DEFAULT NULL";
        // $query="ALTER TABLE users MODIFY last_name VARCHAR(255) DEFAULT NULL";

    //    $query="ALTER TABLE practices ADD COLUMN video_name VARCHAR(255) DEFAULT NULL";

        // $query="ALTER TABLE media DROP FOREIGN KEY media_owner_id_foreign";
        //  $query="ALTER TABLE abouts DROP FOREIGN KEY abouts_creator_id_foreign";
        

//        $succ= $pdo->query($query);
        return '$succ';
    }
    public function copy_ts($training_id,$new_id)
    {
        $pdo= DB::getPdo();
        $query="SELECT * FROM training_sessions WHERE training_id=97";
        $succ= DB::select(DB::raw($query));
        foreach($succ as $data)
        {
            // dd($data->session_id);
            $query2="INSERT INTO training_sessions (user_id,training_id,session_id,active,public) VALUES (2,$new_id,$data->session_id,0,0)";
            $pdo->query($query2);
        }
    }
    public function copy_tss($t_id,$new_t_id)
    {
        $pdo= DB::getPdo();
        $query="SELECT * FROM training_session_subsessions WHERE training_id=$t_id";
        $data= DB::select(DB::raw($query));
        foreach($data as $row)
        {
            $session_id=$row->session_id;
            $subsession_id=$row->subsession_id;
            $subsession= Subsession::where('id','=',$subsession_id)->first();
            $query2= "INSERT INTO training_session_subsessions (training_id,session_id,subsession_id,creator_id,active,name,picture,public,publisher_id) VALUES($new_t_id,$session_id,$subsession_id,2,0,\"$subsession->name\",\"".$subsession->picture."\",0,null)";
            // dd($query2);
            $pdo->query($query2);
        }
        return 'true';
    }
    public function copy_tsssp($t_id,$new_t_id)
    {
        $pdo= DB::getPdo();
        $query="SELECT * FROM training_session_subsession_practices WHERE training_id=$t_id";
        $data= DB::select(DB::raw($query));
        foreach($data as $row)
        {
            $practice=Practice::where('id','=',$row->practice_id)->first();
            $session_id=$row->session_id;
            $subsession_id=$row->subsession_id;
            $query2= "INSERT INTO training_session_subsession_practices (training_id,session_id,subsession_id,practice_id,creator_id,active,name,practice_video,practice_picture,public,publisher_id) VALUES ($new_t_id,$session_id,$subsession_id,$practice->id,2,0,\"$practice->name\",\"$practice->video\",\"$practice->picture\",0,null)";
            // dd($query2);
            $pdo->query($query2);
        }
        return 'true';
    }
    public function returnEmpty(Request $r)
    {
        return response()->json("Not found");
    }
    public function testPdfMaker(Request $r)
    {
        $subsession= new Subsession;
        return response()->json(['success'=>$subsession->generatePdf($r)]);
    }
    public function thumbnailMake(Request $r)
    {
        $video=$r['video'];
        $practice= new Practice();
        return $practice->makeVideoThumbnail($video);
    }
}


/*

        Paginacija Kolekcije

        function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null)
        {
            $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
            $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
            parse_str(request()->getQueryString(), $query);
            unset($query[$pageName]);
            $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
                $currentPageItems,
                $collection->count(),
                $perPage,
                $currentPage,
                [
                    'pageName' => $pageName,
                    'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                    'query' => $query,
                    'fragment' => $fragment
                ]
            );

            return $paginator;
        }

        /*
        /**
            * Gera a paginação dos itens de um array ou collection.
            *
            * @param array|Collection      $items
            * @param int   $perPage
            * @param int  $page
            * @param array $options
            *
            * @return LengthAwarePaginator
            */
            // public function paginate($items, $perPage = 15, $page = null, $options = [])
            // {
            //     $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
            //     $items = $items instanceof Collection ? $items : Collection::make($items);
            //     return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
            // }
        // **/
        // */
?>
