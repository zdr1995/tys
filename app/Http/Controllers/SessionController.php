<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Session;
class SessionController extends BaseController
{
    public function subsessions(Request $r,$id)
    {
        $session=Session::findOrFail($id);
        $subsessions=$session->subsessions();
        return response()->json($subsessions);
    }
}
