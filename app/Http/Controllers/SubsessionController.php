<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Subsession;
class SubsessionController extends BaseController
{
    public function practices(Request $r, $id)
    {
        $subsession= Subsession::findOrFail($id);
        return response()->json($subsession->practices(),200);
    }

}
