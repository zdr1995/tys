<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Chat;
use Validator;
use App\Classes\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Http\Request;


class UserController extends BaseController
{
    protected $defaultUserPicture = 'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fkooledge.com%2Fassets%2Fdefault_medium_avatar-57d58da4fc778fbd688dcbc4cbc47e14ac79839a9801187e42a796cbd6569847.png&f=1&nofb=1';
    public function getTrainer(Request $r,$id)
    {
        $user= User::findOrFail($id);
        return response()->json($user->trainer());
    }
    public function trainings(Request $r,$id)
    {
        $user=JWTAuth::parseToken()->authenticate();
        if($user)
        {
            $user=User::findOrFail($id);
            $data=collect();
            foreach ($user->relations as $relation)
            {
                $data->push($relation->relations());
            }
            return response()->json($data);
        } else {
            return response()->json("Not authorized",401);
        }

    }
    public function userTrainings(Request $r, $id)
    {
        $user= User::findOrFail($id);
        return response()->json($user->getTrainings());
    }
    public function allSessions(Request $r, $id)
    {
        $user= User::findOrFail($id);
        return response()->json($user->sessions());
    }
    public function visibleTrainings(Request $r)
    {
        $user=JWTAuth::parseToken()->user();
        
    }
    //  ToDo
    // public function initialRegister(Request $r)
    // {
    //     $user=new User;
    //     $user->preRegister($r);
    // }
    public static function getHashedUser(Request $r)
    {
        $users=User::all();
        if($r->has('hash')) {
            foreach($users as $user)
            {
                if(Hash::check($user->email,$r['hash']))
                    return $user;
            }
        }
        return false;
    }
    public function changePassword(Request $r)
    {
        if(!$r->has('id'))
            return response('Hello world', 404);
        
        $loggedInUser=JWTAuth::parseToken()->authenticate();
        $user= User::findOrFail($r['id']);
        if($loggedInUser->id==$user->id || $loggedInUser->role=='superadmin') {
            $user->password= Hash::make($r['password']); 
            if(!$user->save())
                return response()->json(['message'=>'not found']);
            return response()->json(['message'=>'success']);
        }
        return response()->json(['message'=>'not allowed'],401);
    }
    public function loadWithOrganizations(Request $r)
    {
        $model= new User;
        return response()->json($model->loadAllWithOrganizations($r));
    }
    public function filter(Request $r)
    {
        $users= new User;
        $allData=$users->loadAll(true);

        if($r->has('organization_id') && $r['organization_id']=='superadmins')
        {
            $dat= $users->loadSuperAdmins();
            foreach($dat as $user)
            {
                $picture=Storage::disk()->exists($user->profile_picture);
                if($picture==null)
                    $user->picture = $this->defaultUserPicture;
            }
            return $dat;
        }
        if($r->has('organization_id') && $r->has('team_id'))
        {
            if( intval($r['organization_id']) && $r['team_id']=='all' )
            {
                // Sve iz organizacije 
                $data=$users->loadByOrgId($r['organization_id']);
                foreach($data as $user)
                {
                    $picture= Storage::disk()->exists($user->profile_picture);
                    if($picture==null)
                        $user->picture = $this->defaultUserPicture;
                }
                return $data;
            }
            if($r['organization_id']=='unsorted')
            {
                // Unsorted globally
                $data= $users->loadUnsorted(false,'global');
                return $data;
            }
            
            if( intval($r['organization_id']) && $r['team_id']=='unsorted' )
            {
                //  from org_id unsorted
                $org= DB::select(DB::raw("SELECT * FROM users WHERE organization_id=".$r['organization_id']." LIMIT 1"));
                if(isset($org))
                {
                    $unsorted= DB::select(DB::raw("SELECT * FROM users WHERE organization_id= ".$r['organization_id']." AND team_id $IS NULL"));
                    foreach($unsorted as $user)
                    {
                        $picture= Storage::disk()->exists($user->profile_picture);
                        if($picture==null)
                            $user->picture= User::$default_picture;
                    }
                    return $unsorted;
                }
            }
            if( intval($r['organization_id']) && intval($r['team_id']) )
            { 
                return $users->getByTeamId($r['team_id']);
            }

            // return $allData->where('organization')
        }
    }

    public function resetPasswdRequest(Request $r)
    {
        if(!$r->filled('email'))
            return response()->json([
                'message' => 'Missing data!'
            ], 400);

        $validator = Validator::make($r->all(), [
            'email' => 'required|string'
        ]);
        if($validator->fails()) {
            return response()->json([
                'message' => 'Missing data!'
            ], 400);  
        }

        $user = User::where('email', $r->email)->first();
        if($user == null) 
            return response()->json([
                'message' => 'User not found.'
            ], 400);

        return $user->resetPasswordRequest();
    }

    /*
    |-----------------------------------------------
    | After sending reset password code on email
    | this method would process code, and returns
    | user
    |-----------------------------------------------
     */
    public function validateResetPasswdCode(Request $r)
    {
        if(!$r->filled('email') || !$r->filled('code'))
            return response()->json([
                'message' => 'Invalid data'
            ], 400);


        $validator = Validator::make($r->all(), [
                'email' => 'required|string',
                'code' => 'required|string'
            ]
        );
        if($validator->fails()) {
            return response()->json([
                'message' => 'Missing data!'
            ], 400);  
        }

        $user = User::where([
            'email' => $r->email,
        ])->first();

        if($user == null)
            return response()->json([
                'message' => 'User not found!'
            ], 404); 

        if(!$user->resetPasswordCode($r))
        {
            return response()->json([
                'message' => 'Code is invalid'
            ], 401);
        }

        return response()->json([
            'message' => 'success',
            'data' => $user
        ]);
    }

    /*
    |-----------------------------------------------
    |   This method is used for check password for
    |   user after resetting password trough mail
    |-----------------------------------------------
     */
    public function changeResetPassword(Request $r)
    {
        // dd($r->all());
        $validator = Validator::make($r->all(),[
            'email' => 'required|string',
            'password' => 'required|string',
            'password_repeat' => 'required|string',
        ]);
        if($validator->fails())
            return response()->json([
                'message' => 'Invalid data!'
            ], 400);

        $user = User::where('email', $r->email)
            ->first();
        if($user == null) {
            return response()->json([
                'message' => 'User not found!'
            ], 404);
        }

        if($user->identifier != 'NULL') {
            return response()->json([
                'message' => 'Invalid code'
            ], 400);
        }

        if($r->pasword != $r->repeat_password)
            return response()->json([
                'message' => 'Passwords doesn\'t match'
            ], 400);

        if(!$user->changePassword($r))
            return response()->json([
                'message' => 'Server error'
            ], 500);

        // For next round
        $user->identifier = str_random(8);
        $user->save();

        return response()->json([
            'message' => 'Successifully changed password.',
            'data' => $user
        ], 200);

    }

    public function getChatkitData()
    {
        return (new Chat)->loadChatkitUser();
    }

}
