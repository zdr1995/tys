<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Practice;
use App\TrainingSessionSubsessionPractice as TSSSP;

class PracticeController extends BaseController
{
    
    public function testThumbnail(Request $r)
    {
        $practice= new Practice();
        $practice->makeVideoThumbnail($r,$r['video']);
    }

    public function filterPractices(Request $r)
    {
        $practice= new TSSSP();
        // dd($practice);
        $filteredPractice= $practice->filter($r);
        if($filteredPractice!==null)
        {
            return response()->json($filteredPractice);
        }else {
            return response()->json(['message'=>'failed']);
        }
    }

}
