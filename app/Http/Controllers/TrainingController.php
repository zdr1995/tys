<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BaseModel;
use App\Training;
use App\VisibleTraining;
use App\TrainingRelationModel;

class TrainingController extends BaseController
{

    public function find(Request $r,$id)
    {
        $training= TrainingRelationModel::all()->load(['trainings','practices','sessions','users'])->where('id','=',$id);
        return response()->json($training);
    }
    public function users(Request $r,$id)
    {
        $training= Training::findOrFail($id);
        return response()->json($training->users());
    }
    
    public function sessions(Request $r,$id)
    {
        $training=Training::find($id);
        if($training)
            return response()->json($training->sessions());
        return response()->json([]);
    }

    public function setActive(Request $r)
    {
        $user= (new BaseModel())->getLoggedIn();
        // if($user->role!=='superadmin')
        // {
        if(!$r->has('active'))
            return response()->json(['message'=>'Not found'],404);
    
        $data=[
            'user_id'=> $user->id,
            'visible'=> $r['active']
        ];
        $exists= VisibleTraining::where('user_id','=',$user->id)->first();
        if($exists==null) 
            VisibleTraining::create($data);
        else {
            $exists->visible= $data['visible'];
            if( !$exists->save() )
                return response()->json(['message'=>'failed'],404);
        }
        return response()->json('success');
        
    }
}
