<?php

namespace App\Http\Controllers;

use App\BaseModel;
use Illuminate\Http\Request;

use App\User;
use App\Team;
use App\Training;
use App\TeamNews;
use App\Publish;
use App\BaseModel;
use App\Notification;

class NotifyController extends BaseController
{
    
    public function notifyUsers(Request $r)
    {
        // if( !is_array($r['user_ids']))
        //     return response()->json(['message'=>'failed'],403);
        
        $ids=[];
        $title= $r['title'];
        $body= [
            'type'=>$r['type'],
            'message'=>$r['body']
        ];
        $data=['message'=>$body];

        if( $r['type']!==null )
        {
            if( $r['type']=='training')
            {
                if($r['all'])
                    $ids= User::all()->pluck('id')->toArray();
                else $this->usersFromTraining($r['training_id'],$ids);
                if( !Notification::compareIds($r['training_id'],$ids) )
                    return response()->json(['message'=>'failed'],404);
                // $notification-> 
            }
            if( $r['type']=='general')
            {
                $this->usersForGeneral($r,$ids);
            }   
            if( $r['type']=='team' )
            {
                $team_notify= new TeamNews;
                $team_notify->addData(
                    $title,
                    $body,
                    $data
                );
                $teams= \explode(',',$r['teams']);
                $team_notify->addTeams($teams);
                $team_notify->sendData();
                $id=$team_notify->storeSentData([
                    'message'=>$r['message'],
                    'file_type'=> $r['file_type'] 
                ],$r);
                $ids=[];
                foreach($teams as $team)
                {
                    $users= User::where('team_id','=',$team)->get();
                    foreach($users as $user)
                        $ids[]= $user->id;
                }
                $s_admins= User::where('role','=','superadmin')->pluck('id')->toArray();
                foreach( $s_admins as $s_admin_id)
                    $ids[]=$s_admin_id;
                

                $orgs= Team::whereIn('id',$teams)->pluck('organization_id')->toArray();
                // dd($orgs);
                $admins= User::where('role','=','admin')->whereIn('organization_id',$orgs)->pluck('id')->toArray();
                foreach($admins as $admin_id)
                    $ids[]= $admin_id;
            }
            $notification= new Notification;
            $notification->setTitle($title);
            $notification->setBody($body);
            $notification->setData($data['message']);
            $notification->addUsers($ids);
            
            $notification->send();
            if( !$notification->isSuccess() )
                return response()->json(['message'=>'Send failed'],500);
            
            return response()->json(['message'=>'success'],200);
        }else {
            return response()->json(['message'=>'failed'],404);
        }
        
    }
    public function usersFromTraining($training_id,array &$user_ids)
    {
        $training= Training::where('id','=',$training_id)->first();
        if( $training==null )
                return [];
        if($training->public==3)
        {
            foreach(User::all() as $user)
            {
                $publish= new Publish($training->id);
                // If this user is not creator of training and 
                // training is visible for user then send notifications
                if(
                    // $training->creator_id!==(new \App\BaseModel)->getLoggedIn()->id &&
                    $publish->isVisibleByUser($user)
                )
                    $user_ids[]= $user->id;       
            }           
        }
        elseif ($training->public==2) 
        {
            $ids= User::all()->pluck('id')->toArray();
            foreach($ids as $id)
                $user_ids[]= $id;
        }
        elseif( $training->public==1 )
        {
            $creator= User::where('id','=',$training->creator_id)->first();
            $ids= User::where('organization_id','=',$creator->organization_id)->pluck('id')->toArray();
            foreach($ids as $id)
                $user_ids[]= $id;
        }

    }
    public function usersForGeneral(Request $r,array &$user_ids)
    {
        if( $r->has('organizations'))
        {
            foreach( $r['organizations'] as $organization_id )
            {
                $users= User::where('organization_id','=',$organization_id)->get(['id'])->toArray();
                foreach($users as $user_id)
                {
                    $equals_id= (new BaseModel())->getLoggedIn()->id==$user_id['id'];
                    if( !$equals_id )
                        $user_ids[] = $user_id['id'];
                }
                $sadmins= User::where('role','=','superadmin')->get(['id'])->toArray();
                // dd($sadmins);
                foreach($sadmins as $admin_id)
                {
                    // $equals= (new BaseModel())->getLoggedIn()->id==$admin_id['id'];
                    // if ( !$equals )
                        $user_ids[]= $admin_id['id'];
                }
            }
        }
    }
}
