<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chat;
use App\BaseModel;
use App\Notification;
use DB;

class TestController extends BaseController
{
    public function sendPush(Request $r)
    {
        $title= 'Firebase messaging test';
        $data=[
            'message'=>'Zdravko poslo',
            'json'=>json_encode(['screen'=>'team'])
        ];
        $body= json_encode('PAja poslo');
        $notification= new Notification();
        $notification->setTitle($title);
        $notification->setBody($body);
        $notification->setData($data);

        $notification->addUsers([5]);
        $notification->send();
        // dd($notification->success);
        if( $notification->isSuccess() )
            return response()->json(['message'=>'success']);
        return response()->json(['message'=>'failed']);
    }
    // public function alterModifications(Request $r)
    // {
    //     $pdo= DB::getPdo();
    //     $query= "ALTER TABLE users ADD COLUMN fcm_token VARCHAR(255) DEFAULT NULL";

    //     if( $pdo->query($query) )
    //         return response()->json('Success',200);
    //     return response()->json(['message'=>'failed'],404);
    // }
    // public function alterModifications(Request $r)
    // {
    //     $pdo= DB::getPdo();
    //     $query= "CREATE TABLE IF NOT EXISTS notifications (id INT AUTO_INCREMENT NOT NULL,training_id INT, user_ids VARCHAR(255), seen_ids VARCHAR(255), PRIMARY KEY (id)) ENGINE='INNODB'";
    //     $pdo->query($query);
    //     return 'str';
    // }
    
    //  For add team_news table
    //  And team_team_news table

    // public function alterModifications(Request $r)
    // {
    //     $query= "CREATE TABLE IF NOT EXISTS team_news (id INT NOT NULL AUTO_INCREMENT,title VARCHAR(255),message TEXT, sender INT,url VARCHAR(255) NULL,file_type VARCHAR(30) NULL,created_at DATETIME DEFAULT CURRENT_TIMESTAMP,updated_at DATETIME DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY(id)) ENGINE=INNODB;";
    //     $succ= DB::getPdo()->query($query);

    //     $query= "CREATE TABLE IF NOT EXISTS team_team_news(id INT NOT NULL AUTO_INCREMENT,team_id INT(10) UNSIGNED,team_news_id INT,PRIMARY KEY (id)) ENGINE=INNODB;";
    //     $succ2= DB::getPdo()->query($query);

    //     $relation= "ALTER TABLE team_team_news ADD FOREIGN KEY(team_id) REFERENCES teams(id) ON UPDATE CASCADE ON DELETE CASCADE";
    //     $relation= "ALTER TABLE team_team_news ADD FOREIGN KEY(team_news_id) REFERENCES team_news(id) ON UPDATE CASCADE ON DELETE CASCADE";
    //     $rel_query= DB::getPdo()->query($relation);
    //     if( $succ && $succ2 && $rel_query )
    //         return \Response::json(['message'=>'success']);
    //     else return \Response::json(['message'=>'failed'],404);
    // }

    // public function alterModifications(Request $r)
    // {
    //     $pdo= DB::getPdo();
    //     $query="SHOW TABLES";
//     $res= DB::select(DB::raw($query));
    //     return \Response::json($res);
    // }
    // public function alterModifications(Request $r)
    // {

    // }
    public function insertTeamNews(Request $r)
    {
        $news= new \App\TeamNews();
        $body=[
            'type'=>'team',
            'message'=>$r['body']
        ];
        $news->addData($r['title'],$body,['message'=>'suces']);
        $news->addTeams($r['teams']);
        $news->storeSentData($body,$r);

        return \Response::json(['message'=>'success']);
    }
    public function sendPusher(Request $r)
    {
        $message=$r['message'];
        // dd($message);
        event(new \App\Events\TestEvent('test'));
    }

    public function testChatMongo(Request $r)
    {
        $chat= new Chat;
        $chat->store($r->all());
        return response()->json(['message'=>'success'],200);
    }
    public function getChat(Request $r)
    {
        $data= new Chat();
        return response()->json($data->loadAll());
    }
    public function loadChatByUserId(Request $r,$id)
    {
        $chat= new Chat();
        // dd($chat);
        $messages= $chat->loadMessages($id);
        return response()->json($messages);
    }
    public function storeRoom(Request $r)
    {
        // $chat=new \App\Chat();
        // $res= $chat->insertRoom((new.));
        // return response()->json(['message'=>$res],200);
    }
    public function guzzletest(Request $r)
    {
        dd($r->all());
        return response()->json($r->all());
    }
    public function loadAllRooms(Request $r)
    {
        $chatkit= new Chat;
        $user=(new BaseModel)->getLoggedIn();
        return response()->json($chatkit->getAllRooms($user));
    }
    public function mailTest(Request $r)
    {
        $email= $r->email;
        
        $creds= new \StdClass();
        $creds->data= env('PAJIN_LINK');
        $creds->message= 'Welcome to tys app';

        \Mail::send('registerMail', ['creditials' => $creds], function ($m) use ($email) {
            $m->to($email, 'Pera Petrovic')->subject('Welcome to the website');
        });
        if(\Mail::failures())
            return response()->json(['message'=>'nije moguce poslati mail']);
        else return response()->json(['message'=>'mail uspjesno poslat']);
    }

    public function loadHtmlView(Request $r)
    {
        $to= "zdravkosokcevic100@gmail.com";
        $hashedCreditials= new \StdClass();
        $hashedCreditials->data=env('PAJIN_LINK').\Hash::make($to);
        $hashedCreditials->message='reset password';
        return View('registerSingleMail',['creditials'=>$hashedCreditials]);
    }

    public function childUsers(Request $r)
    {
        //return response()->json(\App\User::all()->pluck('first_name')->toArray());
    }
}
