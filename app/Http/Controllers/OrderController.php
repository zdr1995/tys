<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    public function getMakeInstance($model)
    {
        return '\App\\'.ucfirst($model);
    }
    public function insertNew(Request $r,$resource)
    {
        $resource=ucfirst($resource);
        $model=$this->getMakeInstance($resource);
        $order= new Order;
        if($r['order']==null || 
            $r['order']=='null'
            )
            return response()->json(['message'=>'failed'],404);
        $order->setType($resource);
        $order->setOrder($r['order']);

        $success=$order->insertOrUpdate($r,$resource);
        if($success)
            return response()->json(['message'=>'success']);
        return response()->json(['message'=>'failed'],404);
    }
}
