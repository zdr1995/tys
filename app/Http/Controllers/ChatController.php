<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BaseModel;
use App\Chat;

class ChatController extends BaseController
{
    public function loadRooms(Request $r)
    {
        $user= (new BaseModel)->getLoggedIn();
        $chat= new Chat;
        return response()->json($chat->loadRoomsByUser($user));
    }
}
