<?php

namespace App;

use Illuminate\Http\Request;

class Order extends BaseModel
{
    protected $guarded=[];
    protected $req;
    protected $user;
    protected $orderInstance;
    public $collection;
    public $order;
    public static $types=[
        'Training',
        'TrainingSession',
        'TrainingSessionSubsession',
        'TrainingSessionSubsessionPractice',
        'SessionSubsessionPractice',
        'SessionSubsession',
        'SubsessionPractice'
    ];

    public function __construct()
    {
        $this->user=$this->getLoggedIn();
    }

    public static function create($data=null)
    {
            $instance=new self();
            $instance->returnData= $data;
            $instance->user=(new static)->getLoggedIn();
            $instance->collection=$attr;
            return $instance;
    }

    public function createInstance(Request $r,$data=null)
    {
        $this->user=$this->getLoggedIn();
        $this->returnData=null;
        $this->req=$r;

    }

    public function setOrder($order)
    {
        // dd($order);
        if(is_array($order))
        {
            if(count($order)>1)
                $this->order=\implode(',',$order);
            else if(count($order)==1)
                $this->order=$order;
            else $order='';
            // dd($this->order);
        }
    }

    public function insertOrUpdate(Request $r,$resource)
    {
        if($r->has('order'))
        {
            if($r['order']=='')
                return;
            else if(is_array($r['order']))
                $this->order=\implode(',',$r['order']);
            else if(strpos($r['order'],','))
                $this->order=$r['order'];

            // if(!is_array($r['order'])  )
            //     $this->order=$r['order'];
            // else $this->order=\implode(',',$r['order']);
            switch($resource)
            {
                case 'Training':
                {
                    if (! (new TrainingOrder)->updateOrStore($r,$this->order))
                        return false;
                };break;
                case 'TrainingSession':
                case 'App\TrainingSession':
                {
                    if (! (new TrainingSessionOrder)->updateOrStore($r,$this->order) )
                        return false;
                };break;
                case 'TrainingSessionSubsession':
                case 'App\TrainingSessionSubsession':
                {
                    if (! (new TrainingSessionSubsessionOrder)->updateOrStore($r,$this->order))
                        return false;
                };break;
                case 'TrainingSessionSubsessionPractice':
                case 'App\TrainingSessionSubsessionPractice':
                {
                    if (! (new TrainingSessionSubsessionPracticeOrder)->updateOrStore($r,$this->order))
                        return false;
                };break;
                case 'SessionSubsession':
                {
                    SessionSubsessionOrder::firstOrNew([
                        'user_id'=> $this->user->id,
                        'session_id'=> $r['session_id'],
                        'order'=>$this->order
                    ]);
                };break;
                case 'SubsessionPractice':
                {
                    SubsessionPracticeOrder::firstOrNew([
                        'user_id'=> $this->user->id,
                        'subsession_id'=> $r['subsession_id'],
                        'order'=> $this->order
                    ]);
                };break;
                case 'Media':
                {
                    MediaOrder::firstOrNew([
                        'user_id'=> $this->user->id,
                        'order'=> $this->order
                    ]);
                };break;
                default:
                {
                    return 'In progress';
                }
            }
        }
        return true;
    }

    public function setType($type)
    {
        if(!$this->validateType($type) )
            return false;
        $this->type= $type;
    }

    public function getOrder(Request $r,$getOrder=false,$mobile=false)
    {
        if(isset($this->type))
        {
            switch($this->type)
            {
                case 'Training':
                {
                    $this->loadTrainingOrder($r);
                };break;
                case 'TrainingSession':
                {
                    $this->loadTrainingSessionOrder($r);
                };break;
                case 'TrainingSessionSubsession':
                {
                    $this->loadTrainingSessionSubsessionOrder($r);
                };break;
                case 'TrainingSessionSubsessionPractice':
                {
                    $this->loadTrainingSessionSubsessionPracticeOrder($r);
                };break;
                case 'Media':
                {
                    $this->loadMedia($r);
                }
            }
            //  If order is stull null
            if($this->order==null)
            {
                switch($this->type)
                {
                    case 'Training':
                    {
                        $this->loadTrainingOrder($r);
                    };break;
                    case 'TrainingSession':
                    {
                        $this->loadTrainingSessionOrder($r);
                    };break;
                    case 'TrainingSessionSubsession':
                    {
                        $this->loadTrainingSessionSubsessionOrder($r);
                    };break;
                    case 'TrainingSessionSubsessionPractice':
                    {
                        $this->loadTrainingSessionSubsessionPracticeOrder($r);
                    };break;
                    case 'Media':
                    {
                        $this->loadMedia($r);
                    }
                }
            }
            if($this->order!=[] && $this->order!=='')
            {
                return ($getOrder)?$this->order:$this->parseOrder($mobile);
            }else return [];
        }

    }

    public function validateType($type)
    {
        foreach(static::$types as $approvedType)
        {
            if($type==$approvedType)
                return true;
        }
        return false;
    }

    public function loadTrainingOrder(Request $r)
    {
        $this->orderInstance= TrainingOrder::where('user_id','=',$this->user->id)->first();
        if ($this->orderInstance==null)
            $this->insertRandomOrder();
        $this->order= $this->unpackOrder($r);
        // dd($this->order);

    }

    public function loadTrainingSessionOrder(Request $r)
    {
        $this->orderInstance= TrainingSessionOrder::where('user_id',$this->user->id)->where('training_id','=',$r['training_id'])->first();
        if ($this->orderInstance==null)
            $this->insertRandomOrder();
        $this->order= $this->unpackOrder();
    }

    public function loadTrainingSessionSubsessionOrder(Request $r)
    {
        $this->orderInstance= TrainingSessionSubsessionOrder::where('user_id',$this->user->id)->where('training_id','=',$r['training_id'])->where('session_id','=',$r['session_id'])->first();
        if ($this->orderInstance==null)
            $this->insertRandomOrder();
        $this->order= $this->unpackOrder();
    }

    public function loadTrainingSessionSubsessionPracticeOrder(Request $r)
    {
        $this->orderInstance= TrainingSessionSubsessionPracticeOrder::where('user_id','=',$this->user->id)
                                                                                ->where('training_id','=',$r['training_id'])
                                                                                ->where('session_id','=',$r['session_id'])
                                                                                ->where('subsession_id','=',$r['subsession_id'])
                                                                                ->first();
        if ($this->orderInstance==null)
            $this->insertRandomOrder();
        $this->order= $this->unpackOrder();
    }

    public function parseOrder($mobile=false)
    {
        $data=[];
        // $model=$this->makeInstance($this->type);
        // $instance= new $model;
        // dd($instance->loadSingle($this->order));
        switch($this->type)
        {
            case 'Training':
            {
                if(!$mobile) {
                    if(\is_string($this->order)) {
                        return (new Training)->loadSingle($this->order);
                    } else {
                        foreach($this->order as $id)
                        {
                            if($id!==null) {
                                $model=(new Training)->loadSingle(intval($id));
                                if($model!='null')
                                    $data[]=$model[0];
                                // TO DO if model is null remove empty data
                            }
                        }
                    }


                } else {
                    if(\is_string($this->order))
                    return (new Training)->loadSingleForMobile($this->order);
                    foreach($this->order as $id)
                    {
                        if($id!==null) {
                            $model=(new Training)->loadSingleForMobile(intval($id));
                            if($model!='null')
                                $data[]=$model[0];
                            // TO DO if model is null remove empty data
                        }
                    }
                }

            return $data;
            };break;
            case 'TrainingSession':
            case 'App\TrainingSession':
            {
                if(\is_string($this->order))
                    return [(new TrainingSession)->loadSingleInstance($this->req,$this->order)];
                foreach(array_unique($this->order) as $id)
                {
                    if($id!==null) {
                        $return=(new TrainingSession)->loadSingleInstance($this->req,$id);
                        if($return!==null)
                            $data[]=$return;
                    }

                }
            return $data;
            };break;
            case 'TrainingSessionSubsession':
            case 'App\TrainingSessionSubsession':
            {
                if(\is_string($this->order)) {
                    $tss=TrainingSessionSubsession::where('subsession_id','=',$this->order)
                                                            ->where('training_id','=',$this->req['training_id'])
                                                            ->where('session_id','=',$this->req['session_id'])
                                                            ->first();
                    return [$tss];

                    // return (new \App\TrainingSessionSubsession)->loadSingle($tss->id);
                }
                $data=[];
                foreach(array_unique($this->order) as $id)
                {
                    if($id!==null)
                    {
                        $model= (new TrainingSessionSubsession)->loadSingleInstance($this->req,$id);
                        if($model!==null)
                            $data[]=$model;
                    }
                }
                return $data;
            };break;
            case 'TrainingSessionSubsessionPractice':
            case 'App\TrainingSessionSubsessionPractice':
            {
                if(\is_string($this->order)) {
                    $data= (new TrainingSessionSubsessionPractice)->loadSingleInstance($this->req,$this->order);
                    return [$data];
                }

                foreach(array_unique($this->order) as $id)
                {
                    if($id!==null)
                    {
                        $practice=(new TrainingSessionSubsessionPractice)->loadSingleInstance($this->req,$id);
                        if($practice!==null && $practice!=='null')
                            $data[]= $practice;
                    }
                }
            return $data;
            };break;
        }
    }

    public function unpackOrder()
    {

        if(!isset($this->orderInstance) || $this->orderInstance->order=='') {
            return;
        }
        if($this->orderInstance->order!=='' && !strpos($this->orderInstance->order,','))
            return $this->orderInstance->order;
        return \explode(',',$this->orderInstance->order);
    }

    public function insertRandomOrder()
    {
        // dd($this->type);

        switch($this->type)
        {
            case 'Training':
            {
                $ids=(new Training)->getVisible('ids');
                if($ids!==null)
                {
                    $this->order= \implode(',',$this->idClassToArray($ids));
                    TrainingOrder::updateOrCreate([
                        'user_id'=> $this->getLoggedIn()->id,
                        'order'=> $this->order,
                        'table'=> 'Training'
                    ]);
                }

            };break;
            case 'TrainingSession':
            {
                $ids= TrainingSession::where('training_id','=',$this->req['training_id'])->get(['session_id']);
                $id_arr=[];
                if($ids->count()>0)
                {

                    foreach($ids as $id)
                    {
                        $id_arr[]=$id->session_id;
                    }
                }else {
                    $ids= null;
                }
                if($id_arr!==[])
                {
                    $this->order= $id_arr;
                }

                if($this->order==[] || $this->order=='null') return;
                TrainingSessionOrder::updateOrCreate([
                    'user_id'=>$this->user->id,
                    'training_id'=>$this->req['training_id'],
                    'order'=> $this->packOrder()
                ]);
            };break;
            case 'TrainingSessionSubsession':
            {
                $ids= TrainingSessionSubsession::where('training_id','=',$this->req['training_id'])
                                                                ->where('session_id','=',$this->req['session_id'])
                                                                ->get(['id']);
                $this->order=$this->serializeIds($ids);
                if($this->order==[] || $this->order=='null') return;
                TrainingSessionSubsessionOrder::updateOrCreate([
                    'user_id' => $this->user->id,
                    'training_id' => $this->req['training_id'],
                    'session_id' => $this->req['session_id'],
                    'order'=> $this->packOrder()
                ]);
            };break;
            case 'TrainingSessionSubsessionPractice':
            {
                $ids= TrainingSessionSubsessionPractice::where('training_id','=',$this->req['training_id'])
                                                                ->where('session_id','=',$this->req['session_id'])
                                                                ->where('subsession_id','=',$this->req['subsession_id'])
                                                                ->get(['id']);
                if($this->order==[] || $this->order=='null') return;
                $this->order= $this->serializeIds($ids);
                TrainingSessionSubsessionPracticeOrder::updateOrCreate([
                    'user_id'=>$this->user->id,
                    'training_id'=>$this->req['training_id'],
                    'session_id'=> $this->req['session_id'],
                    'subsession_id'=> $this->req['subsession_id'],
                    'order'=> $this->packOrder()
                ]);
            };break;
            case 'SessionSubsessionPractice':
            {
                $ids= SessionSubsessionPractice::where('session_id','=',$this->req['session_id'])
                                                                ->where('subsession_id','=',$this->req['subsession_id'])
                                                                ->get(['id']);
                $this->order=$this->serializeIds($ids);
                if($this->order==[] || $this->order=='null') return;
                SessionSubsessionPracticeOrder::updateOrCreate([
                    'user_id'=> $this->user->id,
                    'session_id'=> $this->req['session_id'],
                    'subsession_id'=> $this->req['subsession'],
                    'order'=> $this->packOrder()
                ]);
            };break;
            case 'SessionSubsession':
            {
                $ids= SessionSubsession::where('session_id','=',$this->req['session_id'])
                                                        ->get(['id']);
                $this->order=$this->serializeIds($ids);
                if($this->order==[] || $this->order=='null') return;
                SessionSubsessionOrder::updateOrCreate([
                    'user_id'=>$this->user->id,
                    'session_id'=> $this->req['session_id'],
                    'order'=> $this->packOrder()
                ]);
            };break;
            case 'SubsessionPractice':
            {
                $ids= SubsessionPractice::where('subsession_id','=',$this->req['subsession_id'])
                                                        ->get(['id']);
                $this->order=$this->serializeIds($ids);
                if($this->order==[] || $this->order=='null') return;
                SubsessionPracticeOrder::updateOrCreate([
                    'user_id'=>$this->user->id,
                    'subsession_id'=>$this->req['subsession_id'],
                    'order'=> $this->packOrder()
                ]);
            };break;
        }
    }

    public function loadMedia(Request $r)
    {
        // $ids= ;

    }

    public function serializeIds($ids)
    {
        $idArr=[];
        foreach($ids as $collection)
        {
            $idArr[]=$collection['id'];
        }
        return $idArr;
    }

    public function idClassToArray($arr_classes)
    {
        $data= [];
        foreach($arr_classes as $elem)
        {
            $data[]=$elem->id;
        }
        return $data;
    }

    public function packOrder()
    {
        if(count($this->order)==1)
            return $this->order[0];
        return \implode(',',$this->order);
    }

    public function appendNewRow(Request $r,$row_id)
    {
        $req_path=$r->server('REQUEST_URI');
        $path=ucfirst(explode('/',$req_path)[2]);
        switch($path)
        {
            case 'Training':
            {
                (new TrainingOrder)->insertNewRow($r,$row_id);
            };break;
            case 'TrainingSession':
            {
                (new TrainingSessionOrder)->insertNewRow($r,$row_id);
            };break;
            case 'TrainingSessionSubsession':
            {
                (new TrainingSessionSubsessionOrder)->insertNewRow($r,$row_id);
            };break;
            case 'TrainingSessionSubsessionPractice':
            {
                (new TrainingSessionSubsessionPracticeOrder)->insertNewRow($r,$row_id);
            };break;
            case 'SessionSubsessionPractice':
            {
                (new SessionSubsessionPracticeOrder)->insertNewRow($r,$row_id);
            };break;
            case 'SessionSubsession':
            {
                (new SessionSubsessionOrder)->insertNewRow($r,$row_id);
            };break;
            // case 'SubsessionPractice':
            // {
            //     (new \App\SubsessionPracticeOrder)->insertNewRow($r,$id);
            // };break;
            default:{}
        }
        return true;
    }
    
    public function syncIds(Request $r,$object=false)
    {
        switch($this->type)
        {
            case 'Training':
            {
                $db_ids=(new Training)->getVisible('id');
                $order_ids=$this->order;
                if($db_ids!==null)
                {
                    $difference= \array_diff($db_ids,$order_ids);
                    if($difference!==null)
                    {
                        foreach($difference as $id)
                        {
                            (new TrainingOrder)->insertNewRow($r,$id);
                        }
                    }
                }

            };break;
            case 'TrainingSession':
            {
                if($object->training_id!==null)
                {
                    $ts= TrainingSession::where('training_id',$object->training_id)->get(['id']);
                    $db_arr=explode(',',$ts);
                    $orders_id= $this->order;
                    if($ts!==null && $order_ids!==null)
                    {
                        $difference= \array_diff($db_arr,$order_ids);
                        if($difference!==null)
                        {
                            foreach($difference as $id)
                            {
                                (new TrainingSessionOrder)->insertNewRow($r,$id);
                            }
                        }
                    }
                }
            };break;
            case 'TrainingSessionSubsession':
            {
                if($object->training_id!=null && $object->session_id!=null)
                {
                    $tsss= TrainingSessionSubsession::where('training_id','=',$r['training_id'])
                                                            ->where('session_id','='.$object->session_id)
                                                            ->get(['id']);
                    $db_arr=$this->idClassToArray($tsss);
                    $order_ids=$this->order;
                    if($db_arr!=null && $order_ids!=null)
                    {
                        $diff=\array_diff($db_arr,$order_ids);
                        foreach($diff as $id)
                        {
                            (new TrainingSessionSubsessionOrder)->insertNewRow($r,$id);
                        }
                    }
                }
            };break;
            case 'TrainingSessionSubsessionPractice':
            {
                if (
                    $object->training_id!=null &&
                    $object->session_id!=null &&
                    $object->subsession_id!=null
                )
                {
                    $tsssp= TrainingSessionSubsessionPractice::where('training_id','=',$object->training_id)
                                                                    ->where('session_id','=',$object->session_id)
                                                                    ->where('subsession_id','=',$object->subsession_id)
                                                                    ->get(['id']);
                    $db_arr= $this->idClassToArray($tsssp);
                    $order_ids=$this->order;
                    if($db_arr!=null && $order_ids!=null)
                    {
                        $diff= \array_diff($db_arr,$order_ids);
                        foreach($diff as $id)
                        {
                            (new TrainingSessionSubsessionPracticeOrder)->insertNewRow($r,$id);
                        }
                    }
                }
            };break;
        }
    }
}
