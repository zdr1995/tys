<?php

namespace App;


class TeamVisible extends BaseModel
{
    protected $table= 'team_publish';
    protected $guarded=[];

    public function checkAndDeleteOldData($training_id)
    {
        $rows= static::where('training_id','=',$training_id)->get();
        if( $rows!==null && $rows->count()>0)
        {
            foreach($rows as $row)
            {
                if( !$row->delete() )
                    return false;
            }
        }
        return true;
    }
}
