<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class LaravelLoggerProxy
{
    public function log($message)
    {
        \Log::info($message);
    }
}
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if('config.chatkit-enabled') 
        {
            $pusher=$this->app->make('pusher');
            $pusher->set_logger( new LaravelLoggerProxy() );
        }
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
