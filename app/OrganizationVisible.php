<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationVisible extends BaseModel
{
    protected $table= 'org_publish';
    protected $guarded=[];

    public function checkAndDeleteOldData($training_id)
    {
        $rows= static::where('training_id','=',$training_id)->get();
        if( isset($rows) && $rows->count()>0 )
        {
            foreach($rows as $row)
            {
                if( !$row->delete() )
                    return false;
            }
        }
        return true;
    }
}
