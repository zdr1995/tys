<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Symfony\Component\HttpFoundation\Request;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class BaseModel extends Model
{
    public function makeInstance($model)
    {
        return '\App\\'.ucfirst($model);
    }
    public function loadAll()
    {
        $allData=static::orderBy('created_at','desc');
        return $allData;
    }
    public function preCreate(Request $r)
    {

        return true;
    }
    public function postCreation(Request $r)
    {
        return true;
    }
    public function preUpdate(Request $r,$id)
    {
        return true;
    }
    public function postUpdate(Request $r)
    {
        return true;
    }
    public function checkConstraints()
    {
        return true;
    }

    public function validate(Request $r)
    {
        return true;
    }
    
    public function specificUpdate(Request $r)
    {
        return false;
    }
    public function getLoggedIn()
    {
        try{
            return \JWTAuth::parseToken()->authenticate();
        }catch(\JWTException $e) {
            return false;
        }
    }
    public function deleteMany(Request $r)
    {
        dd("Base Model");
    }
    public function returnCreated()
    {
        return $this;
    }
    public function insertNewRow(Request $r,$new_id)
    {
        $this->user_id=$this->getLoggedIn()->id;
        return static::all()->where('user_id',$this->getLoggedIn()->id);
    }
    public function unpackOrders()
    {
        if($this->order=='' || $this->order==' ')
            return [];
        if(!strpos($this->order,','))
            return [$this->order[0]];
        return explode(',',$this->order);
    }
    public function packOrders($arr)
    {
        if(count($arr)==1)
            return $arr[0];
        return implode(',',$arr);
    }
    public function loadSingle($id)
    {
        return static::find($id);
    }
    public function search(Request $r)
    {
        foreach($this->fillable as $attribute) 
        {
            if($attribute=='name') 
            {
                $data=static::where('name','like','%'.$r['q'].'%')->get(['id']);
                $return=[];
                foreach($data as $single) {
                    $return[]=$single->loadSingle($single->id);
                }
                return response()->json($return,200);
            }
        }
        return response()->json(['message','not found key'],404);
    }
    public function getLastInserted()
    {
        $id= static::max('id');
        return $this->loadSingle($id);
    }

    public function isChatEnabled()
    {
        return config('app.chatkit-enabled') == true;
    }

}
