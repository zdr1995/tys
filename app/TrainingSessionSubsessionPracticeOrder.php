<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TrainingSessionSubsessionPracticeOrder extends BaseModel
{
    protected $table='training_session_subsession_practice_order';
    protected $guarded=[];
    public function insertNewRow(Request $r,$new_id)
    {
        $training_id= Training::find($r['training_id']);
        $session_id= TrainingSession::where('id','=',$r['session_id'])->first()->session_id;
        $subsession_id= TrainingSessionSubsession::where('id','=',$r['subsession_id'])->first()->subsession_id;
        $instances= parent::insertNewrow($r,$new_id)
                                            ->where('training_id',$r['training_id'])
                                            ->where('session_id',$r['session_id'])
                                            ->where('subsession_id',$r['subsession_id'])
                                            ->get();

        foreach($instances as $instance)
        {
            $arr=$instance->unpackOrder();
            // $arr[]=$new_id;
            // Append new row into front of order
            \array_unshift($arr,$new_id);
            $this->order=$instance->packOrder($arr);
            $this->save();
        }
    }
    public function updateOrStore(Request $r,$order)
    {
        $training_id= $r['training_id'];
        $session_id= $r['session_id'];
        $subsession_id= $r['subsession_id'];

        $rows= static::where('user_id','=',$this->getLoggedIn()->id)
                        ->where('training_id','=',$training_id)
                        ->where('session_id','=',$session_id)
                        ->where('subsession_id','=',$subsession_id)
                        ->get();
        if($rows !== null)
        {
            foreach($rows as $row)
            {
                $row->compareIds($r);
            }
            if($rows==null || $rows->count()==0)
            {
                $row= new static;
                if( !$row->storeNew($r,$order) )
                    return false;
                return true;
            } else {
                $row->training_id= $training_id;
                $row->session_id= $session_id;
                $row->subsession_id= $subsession_id;
                $row->order= $order;
                $row->user_id= $this->getLoggedIn()->id;
                if( !$row->save() )
                    return false;
            }
        }
        return true;
    }
    public function compareIds(Request $r)
    {
        $ord_arr= $this->unpackOrders();
        $cleaned_order= [];
        if( $ord_arr==[] )
        {
            $this->delete();
        } else {
            foreach($ord_arr as $practice_id)
            {
                $exists= TrainingSessionSubsessionPractice::where('training_id','=',$r['training_id'])
                                                                    ->where('session_id','=',$r['session_id'])
                                                                    ->where('subsession_id','=',$r['subsession_id'])
                                                                    ->first();
                if( $exists )
                    $cleaned_order[]= $practice_id;
            }
            $diff= array_diff($r['order'],$cleaned_order);
            foreach($diff as $id)
            {
                $cleaned_order[]= $id;
            }
            $this->order= $this->packOrders($cleaned_order);
            if( !$this->save() )
                return false;
        }
        return true;
    }
    public function storeNew(Request $r,$order)
    {
        $self= new static;
        $self->training_id=$r['training_id'];
        $self->session_id=$r['session_id'];
        $self->subsession_id=$r['subsession_id'];
        $self->user_id= $this->getLoggedIn()->id;
        $self->order= $order;
        if ( !$self->save() )
            return false;
        return true;
    }
}
