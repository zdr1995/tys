<?php

namespace App;

use Illuminate\Http\Request;
use Invertation\Image;

class TrainingSessionSubsession extends BaseModel
{
    protected $table='training_session_subsessions';
    protected $fillable=[
        'training_id',
        'session_id',
        'subsession_id',
        'name',
        'picture',
        'creator_id',
        'publisher_id',
        'active',
        'public'
    ];
    protected $rules=[

    ];
    public function validate(Request $r)
    {
        if(!$r->has('training_id')|| !$r->has('session_id')|| !$r->has('subsession_id'))
            return false;
        $training= Training::find($r['training_id']);
        $session= TrainingSession::find($r['session_id']);
        $subsession= Subsession::find($r['subsession_id']);
        if( $training==null || $session==null || $subsession==null)
            return false;
        return true;
    }

    public function preCreate(Request $r)
    {
        if($r->has('subsession_id')) {
            $subsession=Subsession::find($r['subsession_id']);
            $this->name=$subsession->name;
            $this->picture=($r->has('picture'))?$this->savePicture($r):$subsession->picture;
        }
        if($r->has('session_id')) {
            $this->session_id= TrainingSession::find($r['session_id'])->first()->session_id;
        }
        $this->creator_id= $this->getLoggedIn()->id;
        $this->active=0;
        $this->public=0;
        return true;
    }

    public function createModel(Request $r)
    {
        $model= new static;
        if($r->has('session_id') && $r->has('subsession_id'))
        {
            $trainingSession=TrainingSession::where('id',$r['session_id'])->first();
            $subsession=Subsession::where('id',$r['subsession_id'])->first();
            $model->training_id=$r['training_id'];
            $model->session_id=Session::where('id',$trainingSession->session_id)->first()->id;
            $model->subsession_id=$subsession->id;
            $model->name=($r->has('name'))?$r['name']:$subsession->id;
            $model->active=0;
            $model->public=0;
            if(!$model->save())
                return false;

            $order=new Order();
            if (!$order->appendNewRow($r,$model->id))
                return false;

            return true;
        }else return false;

    }

    public function postCreation(Request $r)
    {
        // ToDo insert pictures and all data relationed to subsession
        if($r->has('picture') && !strpos($r['picture'],'media')) {
            // $this->default_picture=$r['picture'];
            if(!$this->savePicture($r))
                return false;
        }else if($r->has('picture')) {
                $this->default_picture=$r['picture'];
        }else {
            //copy picture from lower table
            $this->picture= Subsession::where('id',$this->subsession_id)->first()->picture;
        }

        $order=new Order;
        if(!$order->appendNewRow($r,$this->id))
            return false;

        // Insert into SessionSubsession table
        $this->checkAndInsertRelation($r);
        if(! $this->save() )
                return false;
        return true;
    }
    public function savePicture(Request $r)
    {
        $image=new Image;
        if(!$image->save('/TrainingSessionSubsession/'.$this->id))
            return false;
        $this->picture= '/media/TrainingSessionSubsession/'.$this->id;
        if (!$this->save())
            return false;
    }
    public function checkAndInsertRelation(Request $r)
    {
        $exists=SessionSubsession::find($r['subsession_id']);
        // there is not exists
        if( !$exists )
        {
            $data=[
                'session_id'=>$r['session_id'],
                'subsession_id'=>$r['subsession_id'],
                'creator_id'=>$this->getLoggedIn()->id
            ];
            if(SessionSubsession::insert($data))
                return true;
            return false;
        } else {
            $exists->session_id=$r['session_id'];
            $exists->subsession_id=$r['subsession_id'];
            if( $exists->save() )
                return true;
            return false;
        }
    }
    public function preUpdate(Request $r,$id)
    {
        return true;
    }
    public function postUpdate(Request $r)
    {

        return true;
    }
    public function specificUpdate(Request $r)
    {
        return false;
    }

    public function training()
    {
        return Training::find($this->training_id);
    }
    public function session()
    {
        return Session::find($this->session_id);
    }
    public function subsession()
    {
        return Subsession::find($this->subsession_id);
    }
    // public function destroyInstance(Request $r)
    // {
    //     $self=\App\TrainingSessionSubsession::where('training_id',$r['training_id'])
    //                     ->where('session_id',$r['session_id'])
    //                     ->where('id',$r['subsession_id'])
    //                     ->first();
    //     if(isset($self)) {
    //         if($self &&$self->delete())
    //             return response()->json(['message'=>'deleted']);
    //         return response()->json(['message'=>'not found'],404);
    //     }else {
    //         return response()->json(['message'=>'not found'],404);
    //     }
    // }
    public function destroyInstance(Request $r)
    {

        $self=static::find($r['subsession_id']);
        if($self!==null && isset($self)){
            if (! $self->delete_from_tsssp())
                return response()->json('Failed',404);
            if (!$self->delete())
                return response()->json('Failed',404);
            return response()->json('Success');
        }

    }
    public function deleteMany(Request $r)
    {
        $training_id=$r['training_id'];
        $session_id=$r['session_id'];
        $subsessions_array=$r['subsessions'];
        foreach($subsessions_array as $subsession_id) {
            $request= new Request([
                'training_id'=>$training_id,
                'session_id'=>$session_id,
                'subsession_id'=>$subsession_id
            ]);
            if( !$this->destroyInstance($request) )
                return response()->json(['message'=>'not deleted'],404);
        }
        return response()->json(['message'=>'deleted']);
    }
    //  Delete garbage data from tssssp
    public function delete_from_tsssp()
    {
        $practices= TrainingSessionSubsessionPractice::where('user_id',$this->user_id)
                                                            ->where('training_id','=',$this->training_id)
                                                            ->where('session_id','=',$this->session_id)
                                                            ->where('subsession_id','=',$this->id)
                                                            ->get();
        foreach($practices as $practice) {
            if($practice!==null) {
                if( !$practice->delete() )
                    return false;
            }
        }
        return true;
    }
    public function respond(Request $r)
    {
//        $order= new Order;
//        $order->setType('TrainingSessionSubsession');
//        $order->createInstance($r);
//        return $order->getOrder($r);


        //     --Old code, no order included--      //

        $row= static::where('training_id',$r['training_id'])
                        ->where('session_id',$r['session_id'])
                        ->get();
        foreach ($row as $static)
        {
            $subsession=Subsession::where('id',$static->subsession_id)->first();
            $static->subsession_id=$subsession->id;
            $static->name=$subsession->name;
            $static->picture=$subsession->picture;
            $static->drill_manual=$subsession->drill_manual;
            $static->drill_manual_name=$subsession->drill_manual_name;

        }
        return $row;
//         $trainingSession=\App\TrainingSession::where('session_id',$r['session_id'])->first();
//         if($trainingSession===null)
//             return [];
//         $session_id=$trainingSession->session_id;
//         $training = \App\Training::where('id', '=', $training_id)->first();
//         $session = \App\Session::where('id', '=', $session_id)->first();
//         if($training!==null && $session!==null) {
//
//             $ret['training_name'] = $training->name;
//             $ret['session_name'] = $session->name;
//
//             $req_data=static::where('training_id',$training_id)->where('session_id',$session_id)->get();
//             $data=[];
//             foreach($req_data as $single)
//             {
//                 $subsession=\App\Subsession::where('id',$single->subsession_id)->first();
//                 if(isset($subsession)) {
//                     $single->name=($single->name==null)?$subsession->name:$single->name;
//                     $single->subsession= $subsession;
//     //                $single->subsession_id= $subsession->id;
//                     $single->name=$subsession->name;
//                 }
//                 // $subsession->id=$this->id;
//                 // if(isset($subsession))
//                 //     $data[]=$subsession->original;
//             }
//             $ret['items'] = $req_data;
//             return $ret['items'];
//         }else return [];

    }
    public function loadSingleInstance(Request $r,$ss_id)
    {
        $instance= static::where('training_id','=',$r['training_id'])
                            ->where('session_id','=',$r['session_id'])
                            ->where('subsession_id','=',$ss_id)
                            ->first();
        // dd($ss_id);
        if($instance) {
            $subsession=Subsession::where('id',$ss_id)->first();
            $instance->drill_manual=$subsession->drill_manual;
            if($instance->drill_manual=='null')
                $instance->drill_manual==null;
            ($instance->name==null)?$instance->name=$subsession->name:$instance->name;

            $instance->subsession=$subsession;
            return $instance;
        }else return null;

    }
    public function patchInstance(Request $r)
    {
        return true;
    }

    public function copy(Request $r,Training $t)
    {
        $newRowData= $this->attributes;
        $newRowData['training_id']=$t->id;
        $newRowData['creator_id']=$this->getLoggedIn()->id;
        $newRow=new static;
        $newRow->fill($newRowData);
        if (!$newRow->save())
            return false;
        return true;
    }
    public function multiActions(Request $r)
    {
        if(!$r->has('training_id'))
            return false;
            
        $training=Training::where('id',$r['training_id'])->first();
        if($training->creator_id!==$this->getLoggedIn()->id && $this->getLoggedIn()->role!=='superadmin')
            return false;

        if( !$this->createOrDelete($r))
            return false;

        if(!(new Order)->insertOrUpdate($r,static::class))
            return false;
        return true;
    }
    public function createOrDelete(Request $r)
    {
        if( !$r->has('training_id') || !$r->has('session_id') )
            return false;

        if( !$this->cleanSafe($r) )
            return false;

        if( $r['order']!==null && is_array($r['order']) )
        {
            $bulk=[];
            $user= $this->getLoggedIn();
            foreach($r['order'] as $subsession_id)
            {
                $subsession= Subsession::where('id','=',$subsession_id)->first();
                if(isset($subsession))
                {
                    $bulk[]=[
                        'creator_id'    => $user->id,
                        'training_id'   => $r['training_id'],
                        'session_id'    => $r['session_id'],
                        'subsession_id' => $subsession->id,
                        'name'          => $subsession->name,
                        'picture'       => $subsession->picture,
                        'active'        => 1,
                        'public'        => 1
                    ];
                }
            }
            if( $bulk!=[] )
            {
                $success= static::insert($bulk);
                if( !$success )
                    return false;
            }
        }
        return true;
    }
    public function cleanSafe(Request $r)
    {
        $matched= static::where('training_id','=',$r['training_id'])
                            ->where('session_id','=',$r['session_id'])
                            ->get();
        foreach( $matched as $row )
        {
            if( !$row->delete() )
                return false;
        }
        return true;
    }

}
