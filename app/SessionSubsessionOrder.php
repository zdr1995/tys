<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionSubsessionOrder extends BaseModel
{
    protected $table='session_subsession_order';
    protected $fillable=[
        'user_id',
        'session_id',
        'order'
    ];
    protected $guarded=[];
    public $timestamps=false;
    public $updated_at=false;
    public $created_at=false;
    public function insertNewRow(Request $r,$new_id)
    {
        $instances= parent::insertNewrow($r,$new_id)
                                            ->where('session_id',$r['session_id'])
                                            ->get();
        foreach($instances as $instance)
        {
            $arr=$instance->unpackOrder();
            $arr[]=$new_id;
            $this->order=$instance->packOrder($arr);
            $this->save();
        }
    }
}
