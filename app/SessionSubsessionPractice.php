<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SessionSubsessionPractice extends BaseModel
{
    protected $table='session_subsession_practices';
    protected $fillable=[
        'session_id',
        'subsession_id',
        'practice_id',
        'creator_id',
        'publisher_id',
        'active',
        'public',
        'default_picture',
        'name',
        'practice_video',
        'practice_picture'
    ];
    protected $rules=[
        'session_id'=> 'required',
        'subsession_id'=>'required',
        'practice_id'=> 'required'
    ];
    public function validate(Request $r)
    {
        $validator=Validator::make($r->only($this->fillable), $this->rules);
        if($validator->fails())
            return false;
        return true;

    }
    public function session()
    {
        return Session::find($this->session_id);
    }
    public function subsession()
    {
        return Subsession::find($this->subsession_id);
    }
    public function practice()
    {
        return Practice::find($this->practice_id);
    }

    public function preCreate(Request $r)
    {
        return true;
    }
    public function postCreation(Request $r)
    {
        if($r->has('practice_video')) {
            $video=$r->file('practice_video');
            
        }
    }
    public function preUpdate(Request $r, $id)
    {
        
    }
    public function postUpdate(Request $r)
    {
        $user=$this->getLoggedIn();

    }
    public function loadAll()
    {
        
    }
    public function deleteMany(Request $r)
    {
        $session_id=$r['session_id'];
        $subsession_id=$r['subsession_id'];
        $practices=$r['practices'];
        foreach($practices as $practice_id)
        {
            $instance= static::where('session_id','=',$session_id)
                                ->where('subsession_id','=',$subsession_id)
                                ->where('practice_id','=',$practice_id)
                                ->get();
            if( !$instance->delete() )
                return response()->json(['message'=>'cannot delete'],500);
        }
        return response()->json(['message'=>'deleted'],200);
    }

    
}
