<?php

namespace App;

use DB;
use App\Classes\Storage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Pawlox\VideoThumbnail\Facade\VideoThumbnail;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

define('PICTURE_FLAG','1');
define('VIDEO_FLAG','2');
define('PDF_FLAG','3');

class Media extends BaseModel
{
    protected $table= 'media';

    protected $fillable=[
        'url',
        'type',
        'name',
        'owner_id',
        'public',
        'published_by',
        'path'
    ];
    protected $rules=[
        'type'=>'in:1,2,3',
        'name'=>'required|string'
    ];
    protected $hidden=['path'];
    public static $global_path='media_gallery/';

    public static function get_pictures_path() {
        return static::$global_path.'pictures/';
    }
    public static function get_videos_path() {
        return static::$global_path.'videos/';
    }
    public static function get_pdf_path() {

    return static::$global_path.'pdf/';
    }

    public function validate(Request $r)
    {
        return true;
    }

    public function preCreate(Request $r)
    {
        $validate=Validator::make($r->only('type','name'), $this->rules);
        if($validate->fails())
            return false;
        try {
            $user=\JWTAuth::parseToken()->authenticate();
            if( !$user->role=='user' )
                return false;
            $this->owner_id=$user->id;
            return true;
        }catch(JWTException $e) {
            return false;
        }
        if( !$r->has('file') )
            return false;


        return true;
    }

    public function postCreation(Request $r)
    {
        if(!$this->saveFile($r) )
            return false;
        return true;
    }
    public function preUpdate(Request $r,$id)
    {
        return true;
    }
    public function postUpdate(Request $r)
    {
        return true;
    }

    public function saveFile(Request $r)
    {
        $file=$r->file('file');
        \Log::info('file',(array)$file);
        switch($r['type'])
        {
            case PICTURE_FLAG:
            {
                $this->path='/storage/'.$this->get_pictures_path().$r['name'];
                $this->url='/storage/'.$this->get_pictures_path().$r['name'];
                Storage::disk()->putFileAs($this->get_pictures_path(),$file,$r['name']);
            };break;
            case VIDEO_FLAG:
            {
                $this->path='/storage/'.$this->get_videos_path().$r['name'].'.mp4';
                $this->url='/storage/'.$this->get_videos_path().$r['name'].'.mp4';
                Storage::disk()->putFileAs($this->get_videos_path(),$file,$r['name'].'.mp4');
                $video_path=$this->get_videos_path().$r['name'];
                $this->makeVideoThumbnail($video_path);

            };break;
            case PDF_FLAG:
            {
                $pdf_name= $r['name'];
                if(strpos($r['name'],'.pdf'))
                    $pdf_name=$r['name'];
                $this->path='/storage/'.$this->get_pdf_path().$pdf_name;
                $this->url='/storage/'.$this->get_pdf_path().$pdf_name;
                Storage::disk()->putFileAs($this->get_pdf_path(),$file,$pdf_name);
            }
        }

        $this->public=($r['public'])?$r['public']:0;
        $this->type=$r['type'];

        if( !$this->save())
            return false;

        return true;
    }

    public function store(Request $r)
    {
        $data= $r->only(['type','name','public']);
        try{
            $data['owner_id']= JWTAuth::parseToken()->authenticate()->id;
        }catch(JWTException $e) {
            $data['owner_id']=null;
        }

    }

    public function checkConstraints()
    {
        if( !$this->deleteFromDisk())
            return false;
        return true;
    }

    public function getRandomVideo()
    {
        $files= Storage::disk()->files(static::get_videos_path());
        if(!$files)
            return null;
        return $files[array_rand($files,1)];
    }
    // public function loadAll()
    // {
    //     return static::orderBy('created_at','DESC');
    // }

    public function loadSingle($id)
    {
        return static::find($id);
    }
    public function loadFiltered(Request $r,$type)
    {
        return static::where('type',$type)
                        ->join('users','media.owner_id','users.id')
                        ->select('media.*','first_name','last_name','profile_picture')
                        ->orderBy('created_at','DESC');
    }
    public function loadAll()
    {
        return DB::table('media')->join('users','media.owner_id','users.id')
                                    ->select('media.*','first_name','last_name','profile_picture')
                                    ->orderBy('created_at','DESC');
    }
    public function deleteMany(Request $r)
    {
        if($r->has('ids'))
        {
            $ids=$r['ids'];
            foreach($ids as $id) 
            {
                $instance=static::find($id);
                if($instance!==null)
                {
                    if(! $instance->deleteFromDisk() )
                        return false;
                    if (! $instance->delete() )
                        return false;
                }
            }
            return true;
        }else {
            return false;   
        }
    }
    public function deleteFromDisk()
    {
        switch($this->type)
        {
            case PICTURE_FLAG:
            {
                if(Storage::disk()->exists($this->path) && !Storage::disk()->delete($this->path))
                    return false;
                
            };break;
            case VIDEO_FLAG:
            {
                if(Storage::disk()->exists($this->path) && !Storage::disk()->delete($this->path))
                    return false;
            };break;
            case PDF_FLAG:
            {
                if(Storage::disk()->exists($this->path) && !Storage::disk()->delete($this->path))
                    return false;       
            }
        }
        return true;
    }
    public function returnCreated()
    {
        return $this;
//         return static::loadAll()->get()->where('id','=',$this->id);
    }
    public function makeVideoThumbnail($video)
    {
        $video_path= Storage::disk()->path($video);
        $thumbnail_path= Storage::disk()->path('media_gallery/videos/thumb/');
        /*
         * --------------------------------------------------------
         *  Video thumbnail for app videos
         * --------------------------------------------------------
         */
//        $ffprobe=\FFMpeg\FFProbe::create([
//            'ffmpeg.binaries' => env('FFMPEG_BINARIES'),
//            'ffmpeg.threads'  => 12,
//            'ffprobe.binaries' => env('FFPROBE_BINARIES'),
//            'timeout' => 3600,
//        ]);
//        $duration= $ffprobe->format($video_path)->get('duration');
        $duration=2;
        try{
            $thumbnailStatus=VideoThumbnail::createThumbnail(
                $video_path,
                $thumbnail_path,
                $this->id.'.jpeg',
                2,
                1200,
                800);
//             dd($thumbnailStatus);
        }catch(Exception $e){
            return false;
        }
        if( Storage::disk()->exists('media_gallery/thumb/'.$this->id.'.jpeg'))
        {
            $path='/storage/media_gallery/videos/thumb/'.$this->id.'.jpeg';
            $this->video_thumbnail= $path;
        }

        return true;
    }
    
}
