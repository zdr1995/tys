<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MailSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $from= "zdravkozdravcee@gmail.com";
        $to= "zdravkosokcevic100@gmail.com";
        $hashedCreditials= new \StdClass();
        $hashedCreditials->data=env('PAJIN_LINK').\Hash::make($to);
        $hashedCreditials->message='reset password';
        $sent=\Mail::send('registerMail', ['creditials'=> $hashedCreditials], function($message)use ($to) {
            $message->sender("zdravkosokcevic100@gmail.com");
            $message->to($to, 'full_name');
            $message->from("zdravkosokcevic100@gmail.com");
            $message->subject('Register');
            // $message->priority(3);
        });

        dd(\Mail::failures());
        dd($sent);
    }
    /**
     * -----------------------------------------------------------------------------
     *  This code also works
     * -----------------------------------------------------------------------------
     *  $sent= \Mail::raw('<p>to je to</p>', function($message)use ($from,$to){
            $message->from($from);
            $message->to($to, 'John Smith')->subject('Welcome!');
        });
     * 
     * -----------------------------------------------------------------------------
     */
}
