<?php

namespace App;
use DB;
use Illuminate\Http\Request;
use Chatkit\Exceptions\ChatkitException;
use Chatkit\Laravel\Facades\Chatkit;

class Chat extends BaseModel
{
    protected $collection= 'chatting';
    protected $fillable=[
        'sender_id',
        'message'
    ];
    public function __construct()
    {
        $chatkit_connection=[
            'instance_locator'=> env('CHATKIT_INSTANCE_LOCATOR'),
            'key' => env('CHATKIT_KEY')
        ];
        $this->chatkit= new \Chatkit\Chatkit($chatkit_connection);
    }
    public function store(array $data)
    {
        static::insert((array)$data);
        return true;
    }
    public function loadAll()
    {
        return static::all();
    }
    public function loadMessages($id)
    {
       
        try {
            $user_get= $this->chatkit->getUser(['id'=>"$id"]);
        }catch(ChatkitException $e) {
            dd($e);
        }
        
        dd($user_get);

        return static::all()->where('sender_id','=',$id);
        // return DB::connection('mongodb')->collection('chatting')->where('sender_id',$id)->get();
    }
    // public function insertRoom(Request $r)
    // {
    //     $this->chatkit->createRoom([
    //         'id'=> $r['team_id'],
    //         'creator_id'=> $r['creator_id'],
    //         'name'=> $r['organization_name'],
    //         'private'=> false
    //     ]);
    //     return 'success';
    // }
    public function insertRoom(User $creator)
    {
        if(! isset($creator->organization_id))
            return;
        $organization= \App\Organization::where('id','=',$creator->organization_id)->first();
        $this->chatkit->createRoom([
            'id'=> 'Team-',
            'creator_id'=> $creator->id,
            'name'=> $organization->name,
            'private'=> false
        ]);
    }
    public function joinCreatorToRoom(Team $team)
    {
        $trainer= $team->trainer();
        $this->chatkit->createRoom([
            'id'=> 'Team-'.$team->name,
            'creator_id' => "$trainer",
            'name'=> 'Organization: '.$team->name,
            'private'=> false
        ]);
    }

    public function createChatkitUser(User $user)
    {
        $data=[
            'id'=>"$user->id",
            'name'=> $user->first_name.' '.$user->last_name,
            'custom_data'=>[
                'desc'=> 'some desc'
            ]
        ];
        if(isset($user->profile_picture))
            $data['avatar_url']= ($this->profile_picture==null || $user->profile_picture=='null')? User::$default_picture:env('APP_LINK').'/'.$user->profile_picture;
        try {
            // dd($user->chatkit);
            $this->chatkit->createUser($data);
            $this->createNeccessaryRooms($user);
            // $this->addUserToRoom($user);
        }catch(\Chatkit\Exceptions\ChatkitException $e) {
            \Log::info('Cannot create Chatkit user');
        //    throw new \Exception('Failed to create user');
        }
    }
    // LOAD USER DATA FROM CHATKIT
    public function loadChatkitUser($id=false)
    {
        if(!$id)
            $user= (new \App\BaseModel)->getLoggedIn();
        else $user=\App\User::find($id);
        if(isset($user))
        {
            try {
                $user_get= $this->chatkit->getUser(['id'=>"$user->id"])['body'];
                // dd($user_get);
            }catch(ChatkitException $e) {
                $this->createChatkitUser($user);
                $user_get=$this->loadChatkitUser($user->id);
            }
            return $user_get;
        }
        return [];
    }

    public function patchPusherAccount(User $user)
    {
        try{
            $chatkit_user= $this->chatkit->getUser(['id'=>"$user->id"]);
        }catch(\Chatkit\Exceptions\ChatkitException $e) {
            $this->createChatkitUser($user);
        }
        $data=[
            'id'=> "$user->id",
            'creator_id'=>$user->id,
            'name'=> $user->first_name.' '.$user->last_name,
            'private'=>false
        ];
        if(isset($user->profile_picture))
        $data['avatar_url']= ($user->profile_picture==null || $user->profile_picture=='null')?\App\User::$default_picture:env('APP_LINK').'/'.$user->profile_picture;
        $this->chatkit->updateUser($data);
    }

    public function addUsersToRoom(array $users, string $room_id) 
    {
        $room= $this->chatkit->getRooms(['id'=>$room_id]);
        foreach($users as $user)
        {
            $this->addUserToRoom($user,$room);
        }
    }

    public function addUserToRoom(User $user,$room)
    {
        $user_data= $this->loadChatkitUser($user->id);
        $this->chatkit->addUsersToRoom([
            'user_ids'=>[$user_data->id],
            'room_id'=> $room->id
        ]);
    }

    public function createOrganizationRoom(User $user)
    {
        if(!isset($user->organization_id))
            return;
        $admin= User::where('organization_id','=',$user->organization_id)
                            ->where('role','=','admin')
                            ->first()->Chatkit();

        $users= User::where('organization_id','=',$user->organization_id)
                            ->orWhere('role','=','superadmin')
                            ->get();
        $organization= \App\Organization::where('id','=',$user->organization_id)->first();
        // dd($organization->name);

        try{
            $room=$this->chatkit->getRoom(['id'=>'Organization-'.$organization->name])['body'];
        }catch(ChatkitException $e) {
            $room=$this->chatkit->createRoom([
                'id'=> 'Organization-'.$organization->name,
                'creator_id' => \strval($admin['id']),
                'name'=> 'Organization: '.$organization->name,
                'private'=> false
            ])['body'];
        }
        $this->joinUsersToRoom($users, $room);
    }

    public function joinUsersToRoom(\Illuminate\Database\Eloquent\Collection $users, $room)
    {
        $user_ids=[];
        foreach($users as $user) {
            $userChatkit= $this->loadChatkitUser($user->id);
            $user_ids[]= $userChatkit['id'];
        }
        $this->chatkit->addUsersToRoom([
            'user_ids' => $user_ids,
            'room_id' => $room['id']
          ]);
    }

    public function createTeamRoom(User $user)
    {
        $trainer= User::where('team_id','=',$user->team_id)
                                ->where('role','=','trainer')
                                ->first();
        if(!isset($trainer))
            $trainer= User::where('role','=','superadmin')->first();

        $users= User::where('team_id','=',$user->team_id)
                            ->orWhere('role','=','superadmin')
                            ->get();

        $team= Team::where('id','=',$user->team_id)
                            ->first();

        
        $chatkitTrainer= $this->loadChatkitUser($trainer->id);
        // dd($chatkitTrainer);
        

        try{
            $room=$this->chatkit->getRoom(['id'=>'Team-'.$team->name])['body'];
        }catch(ChatkitException $e) {
            $room=$this->chatkit->createRoom([
                'id'=> 'Team-'.$team->name,
                'creator_id'=> \strval($chatkitTrainer['id']),
                'name'=> 'Team: '.$team->name,
                'private'=> false
            ])['body'];
        }
        $this->chatkit->joinUsersToRoom($users, $room);
    }

    public function createNeccessaryRooms(User $user)
    {
        if(isset($user->organization_id))
        {
            $this->createOrganizationRoom($user);
        }
        if(isset($user->team_id))
        {
            $this->createTeamRoom($user);
        }
    }
    public function getAllRooms(User $user)
    {
        $rooms=$this->chatkit->getRooms([])['body'];
        return $rooms;
    }
    public function loadRoomsByUser(User $user)
    {
        return $this->chatkit->getUserRooms(['id'=>$user->Chatkit()['id']])['body'];
    }
    public function deleteUser(\App\User $user)
    {
        $rooms= $this->getAllRooms($user);
        // dd($rooms);
        foreach($rooms as $room)
        {
            $this->removeUserFromRoom($user,$room);
        }
        $this->chatkit->asyncDeleteUser([ 'id' => strval($user->id) ]);
    }
    public function removeUserFromRoom(User $user,array $room)
    {
        $this->chatkit->removeUsersFromRoom([
            'user_ids'=>[strval($user->id)],
            'room_id'=>$room['id']
        ]);
    }
    public function deleteTeamRoom(Team $team)
    {
        $this->chatkit->asyncDeleteRoom(['id'=>'Team- '.$team->id]);
    }
    public function deleteOrganizationRoom(Organization $organization)
    {
        $this->chatkit->asyncDeleteRoom(['id'=>'Organization- '.$organization->id]);
    }
}
