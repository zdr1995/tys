<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TrainingSessionOrder extends BaseModel
{
    protected $table='training_session_orders';
    protected $fillable=[
        'user_id',
        'training_id',
        'session_id',
        'order'
    ];
    public $timestamps= false;
    protected $created_at=false;
    protected $updated_at=false;

    // protected CREATED_AT= false;
    // protected UPDATED_AT= false;
    public function insertNewRow(Request $r,$new_id)
    {
        // dd(parent::insertNewRow($r,$new_id)->where('training_id',$r['training_id']));
        $instances= static::where('training_id',$r['training_id'])
                            ->where('user_id','=',$this->getLoggedIn()->id)
                            ->get();
        // if(!$instances instanceof \Illuminate\Support\Collection)
            // $instances=$instances->get();
        //  Append new row to all TrainingSessions where training_id=$new_training id
        if($instances->count()>0)
        {
            foreach($instances as $instance)
            {
                $arr=$instance->unpackOrders();
                \array_unshift($arr,$new_id);

                $instance->order=$instance->packOrders($arr);
                $instance->user_id= $this->getLoggedIn()->id;
                $instance->training_id=$r['training_id'];
                $instance->save();
            }
        }else {
            $data=new static;
            $data->user_id= $this->getLoggedIn()->id;
            $data->order= $new_id;
            $data->training_id=$r['training_id'];
            if (!$data->save())
                return false;
        }
        return true;
    }
    //  Update new order if row exists,
    //  if not create new row
    public function updateOrStore(Request $r,$order)
    {
        // Loop over all arrays and compare new array 
        //  with old array
        $rows= static::where('user_id',$this->getLoggedIn()->id)
                        ->where('training_id','=',$r['training_id'])
                        ->get();
        if($rows!==null)
        {
            foreach($rows as $row)
            {
                $row->compareIds($r);
            }
            // dd($rows);
            if($rows== null || $rows->count()==0) {
                $row=new static;
                if( !$row->storeNew($r,$order))
                    return false;
                return true;
            }else {
                $row->training_id=$r['training_id'];
                $row->order=$order;
                $row->user_id= $this->getLoggedIn()->id;
                if( !$row->save() )
                    return false;
                return true;
            }
        }else {

        }
        
    }
    //  Compare old and new id's sent by request
    public function compareIds(Request $r)
    {
        $ord_arr=$this->unpackOrders();
        $cleaned_order=[];
        //  Loop all order from db and delete that not exists
        if($ord_arr==[])
        {
           $this->delete();
        }else {
            foreach($ord_arr as $session_id)
            {
                // dd($session_id);
                $exists= TrainingSession::where('session_id','=',$session_id)->first();
                if($exists)
                    $cleaned_order[]=$session_id;
            }
            //  Here we have cleanded order from db
            $diff=array_diff($r['order'],$cleaned_order);
            // $this->toDelete=array_diff($cleaned_order,$r['order']);
            // dd($cleaned_order);
            foreach($diff as $id)
            {
                $cleaned_order[]=$id;
            }
            // dd($this);
            // dd($this->packOrders($cleaned_order));
            // dd($this->packOrders($cleaned_order));
            $this->order= $this->packOrders($cleaned_order);
            if(!$this->save())
                return false;
            return true;
        }
        //    dd($ord_arr);
        
    }
    public function storeNew(Request $r,$order)
    {
        $row= new static;
        $row->training_id= $r['training_id'];
        $row->user_id= $this->getLoggedIn()->id;
        $row->order= $order;
        if ( !$row->save() )
            return false;
        return true;
    }
    public function deleteOrderAfterDeleting($training_id,$ts_id)
    {
        $data= static::where('training_id','=',$training_id)
                        ->get();
        if($data->count()>0)
        {
            foreach($data as $row)
            {
                $orders=$row->unpackData();
                $new_order=[];
                foreach($orders as $order)
                {
                    if($order!=$ts_id) {
                        $new_order[]=$order;
                    }
                }
                $row->packData($new_order);
                if(!$row->save())
                    return false;
            }
        }
        
    }
    public function unpackData()
    {
        return array_unique(\explode(',',$this->order));
    }
    public function packData(array $array)
    {
        $this->order=\implode(',',$array);
    }

}

// let nesto= ()=> {
//     return new Promise(res=> {
//         ds[pjgkffdogfdpgjdfpjgpfdg
//         fdx;n;kfd;g
//         snfklfdgp
//         nfdlkgfd
//         nsdfjkg;
//         res(true)
//     })
// }

// nesto().then(res)