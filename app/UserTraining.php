<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTraining extends BaseModel
{
    protected $table='user_trainings';
    protected $fillable=[
        'creator_id',
        'training_id',
        'active',
        'public',
        'publisher_id',
        'picture',
        'icon'
    ];

    public function create($data)
    {

    }
    public function patch($data)
    {

    }
    public function publicUpdate(Request $r,$id) 
    {
        // $relation=$this->logged_user_trainings();
        $this->public=$r['public'];
        $this->publisher_id=$this->getLoggedIn()->id;
        if(UserTraining::where('id',$this->id)->update(['public'=>$r['public'],'publisher_id'=>$this->getLoggedIn()->id]))
            return false;
        return true;
    }
    public function patchAll(Training $t) {
        $this->picture=$t->picture;
        $this->training_id=$t->id;
        $this->icon=$t->icon;
        $this->creator_id=$this->getLoggedIn()->id;
        $this->save();
    }
    public function activeUpdate($r)
    {
        // $this->active=$r['active'];
        if($this->creator_id==$this->getLoggedIn()->id){
            static::where('id',$this->id)->update(['active'=>$r['active']]);
        }else{
            // $new_id=null;
            // if(!$new_id=$this->copyRow($r))
            //     return false;
            // $new_training=\App\Training::find(static::find($new_id)->training_id);
            // if(! $this->copyAllTrainings($r,$new_training) )
            //     return false;

            // Use trainings and relate UserTraining__user
            $data=[
                'user_training_id'=> $this->id,
                'user_id'=> $this->getLoggedIn()->id
            ];
            UserUserTraining::create($data);
        }
        
        return true;
    }
    
    public function copyRow(Request $r)
    {
        $arr=$this->getAttributes();
        unset($arr['id']);
        $arr['active']=$r['active'];
        $arr['creator_id']=$this->getLoggedIn()->id;
        if($new_id=UserTraining::insertGetId($arr))
            return $new_id;
        return false;
    }
    // ToDo copy all trainings,sessions,subsessions,practices
    public function copyAllTrainings(Request $r,Training $newTraining)
    {
        $sessions=TrainingSession::where('training_id',$this->training_id)->get();
        foreach($sessions as $session) {
            if(! $session->copy($r,$newTraining))
                return false;
        }
        $tss=TrainingSessionSubsession::where('training_id',$this->training_id)->get();
        foreach($tss as $row) {
            if(! $row->copy($r,$newTraining))
                return false;
        }
        $tsssp= TrainingSessionSubsessionPractice::where('training_id',$this->training_id)->get();
        foreach($tsssp as $row) {
            if(! $row->copy($r,$newTraining) )
                return false;
        }
        return true;
    }
    // public function destroy($data)
    // {

    // }
    
}
