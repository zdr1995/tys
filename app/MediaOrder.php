<?php

namespace App;

use Illuminate\Http\Request;

class MediaOrder extends BaseModel
{
    protected $table='media_orders';
    protected $guarded=[];

    public $timestamp= false;
    public $updated_at= false;
    public $created_at= false;
    public function insertNewRow(Request $r,$new_id)
    {
        $instances= parent::insertNewrow($r,$new_id)->where('training_id',$r['training_id'])->get();
        foreach($instances as $instance)
        {
            $arr=$instance->unpackOrder();
            $arr[]=$new_id;
            $instance->setOrder($arr);
            // dd($instance->order);
            $this->save();
        }
    }
}
