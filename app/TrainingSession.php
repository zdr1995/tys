<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TrainingSession extends BaseModel
{
    protected $table='training_sessions';
    protected $fillable= [
        'user_id',
        'training_id',
        'session_id',
        'active',
        'public'
    ];
    public function trainings()
    {
        return $this->belongsTo(Training::class,'training_id');
    }
    public function sessions()
    {
        return $this->belongsTo(Session::class,'session_id');
    }
    public function users()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function validate(Request $r)
    {
        if( !$r->has('training_id') || !$r->has('session_id'))
            return false;

        $training= Training::find($r['training_id']);
        $session= Session::find($r['session_id']);

        if($training==null || $session==null)
            return false;

        return true;
    }
    public function preCreate(Request $r)
    {
        $this->user_id=$this->getLoggedIn()->id;
        $this->active=0;
        $this->public=0;
        return true;
    }
    public function postCreation(Request $r)
    {
        (new Order)->appendNewRow($r,$this->id);
        return true;
    }
    public function preUpdate(Request $r, $id)
    {
        // $loggedIn= $this->getLoggedIn()->id;

        return true;
    }
    public function postUpdate(Request $r)
    {
        return true;
    }
    public function get_subsessions()
    {
        $query="
            SELECT * FROM subsessions 
            WHERE id IN (
                SELECT session_subsessions.subsession_id 
                FROM session_subsessions 
                WHERE session_id= $this->id
            )
        ";
        // $subsessions=DB::select(DB::raw("SELECT * from subsessions where id in (SELECT * FROM session_subsessions where session_subsession.session_id= $this->id"));
        $subsessions= DB::select(DB::raw($query));

        return $subsessions;

    }
    public function get_practices()
    {
        $query="
            SELECT * 
            FROM practices 
            WHERE practices.id IN(
                SELECT * 
                FROM subsession_practices 
                WHERE subsession_practices.subsession_id=$this->id
            )
        ";
        $practices= DB::select(DB::raw($query));
    }
    public function loadByIds(Request $r)
    {
        dd("tu si");
    }
    public function setTrainingAttribute($value)
    {
        $this->attributes['training']=$value;
    }
    public function setSessionAttribute($value)
    {
        $this->attributes['session']=$value;
    }
    public function respond(Request $r)
    {
        // dd("tu si");
    //    $order=new Order;
    //    $order->createInstance($r);
    //    $order->setType('TrainingSession');
    //    return $order->getOrder($r);
        $response=static::where('training_id',$r['training_id'])->get();
        foreach ($response as $single)
        {
            $session= Session::where('id',$single->session_id)->first();
            $single->name= $session->name;
            if($session->icon)
                $single->icon=$session->icon;
        }
//        dd($response);
        return $response;
    }
    public function loadSingleInstance(Request $r,$id)
    {
        // \Log::info(['single training_session_id'=>$id]);
        $instance=static::where('session_id','=',$id)
                            ->where('training_id','=',$r['training_id'])->first();
        // dd($instance->session_id);
        if($instance!==null && $instance!=='null') {
            $session= Session::where('id','=',$instance->session_id)->first();
            if($session) {
                $instance->name=$session->name;
                return $instance;
            }else return null;

        }

        // dd($session);
        // dd($session);
        // $instance->name=$session->name;
        // ;
    }
    // public function loadSingle(\Illuminate\Http\Request $r,$id)
    // {
    //     // \Log::info(['single training_session_id'=>$id]);
    //     $instance=static::where('session_id','=',$id)
    //                         ->where('training_id','=',$r['training_id'])->first();
    //     // dd($instance->session_id);
    //     if($instance!==null && $instance!=='null') {
    //         // dd($instance);
    //         $session=\App\Session::where('id','=',$instance->session_id)->first();
    //         if($session->count()>0) {
    //             $instance->name=$session->name;
    //             return $instance;
    //         }else return null;

    //     }

    //     // dd($session);
    //     // dd($session);
    //     // $instance->name=$session->name;
    //     // ;
    // }
    ////////////////////////////////////////////////////
    //          Used for multi actions                //
    ////////////////////////////////////////////////////

    public function createOrDelete(Request $r)
    {
        if(!$r->has('training_id'))
            return false;

        $training= Training::where('id',$r['training_id'])->first();
        if($training->creator_id!==$this->getLoggedIn()->id && $this->getLoggedIn()->role!=='superadmin')
            return false;

            // dd($this->cleanSafe($r));
        if(!$this->cleanSafe($r))
            return false;
        if( $r['order']!==null && is_array($r['order']) )
        {
            $bulk=[];
            $user= $this->getLoggedIn();
            foreach($r['order'] as $session_id)
            {
                $bulk[]=[
                    'training_id' => $r['training_id'],
                    'user_id'     => $user->id,
                    'session_id'  => $session_id,
                    'active'      => 0,
                    'public'      => 0
                ];
            }
            if( $bulk!=[] )
            {
                $success=static::insert($bulk);
                if(!$success)
                    return false;
            }
        }
        return true;
    }
    public function cleanSafe(Request $r)
    {
        // Delete all training at moment
        //  which is matched requested training_id
        $matched= static::where('training_id','=',$r['training_id'])->get();
        // dd($matched);
        foreach($matched as $row)
        {
            if( !$row->delete() )
                return false;
        }
        return true;
    }
    public function destroyInstance(Request $r)
    {
        // dd($r->all());
        $instances= TrainingSession::where('training_id','=',$r['training_id'])->where('session_id','=',$r['session_id'])->get();
        // dd($self);
        foreach($instances as $self)
        {
            if(isset($self))
            {
                if( !$self->delete_from_tsss($r))
                    return response()->json(['message'=>'constraint failed'],404);
                (new TrainingSessionOrder)->deleteOrderAfterDeleting($r['training_id'],$self->id);
                if(!$self->delete())
                    return response()->json(['message'=>'not found'],404);

            }
        }
        return response()->json(['message'=>'deleted']);

    }
    public function deleteMany(Request $r)
    {
        $training_id=$r['training_id'];
        $session_array=$r['sessions'];
        foreach($session_array as $session_id) {
            $request= new Request(['training_id'=>$training_id,'session_id'=>$session_id]);
            if( !$this->destroyInstance($r))
                return response()->json(['message'=>'not deleted'],404);
        }
        return response()->json(['message'=>'deleted'],200);
    }
    public function delete_from_tsss(Request $r) {
        $relation= TrainingSessionSubsession::where('training_id',$this->id)
                                                    ->where('session_id',$this->id)
                                                    ->get();
        foreach($relation as $rel) {
            if(!$rel->delete_from_tsssp() || !$rel->delete() )
                return false;
        }
        return true;
    }
    public function patchInstance(Request $r)
    {
        dd("Stigo si");
    }
    public function copy(Request $r,Training $t)
    {
        $newRowData=$this->attributes;
        $newRowData['training_id']=$t->id;
        $newRowData['creator_id']=$this->getLoggedIn()->id;
        $newRow=new static;
        $newRow->fill($newRowData);
        if (!$newRow->save());
            return false;
        return true;
    }
    public function multiActions(Request $r)
    {
        if (!$this->createOrDelete($r))
            return false;

        // if(!$this->delete($r))
        //     return false;

        //  request for insertOrUpdateMethod
        if(!(new Order)->insertOrUpdate($r,static::class))
            return false;
        return true;
    }
    public function updateOrder(Request $r,$class)
    {
        return true;
    }

}
