<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Team extends BaseModel
{
    protected $table='teams';
    protected $fillable=['name','organization_id'];
    public function TeamTeamNews()
    {
        return $this->belongsTo(TeamTeamNews::class,'team_id');
    }

    public function trainer()
    {
        $admin= User::where('team_id','=',$this->id)
            ->where('role','=','trainer')->first();
        return $admin;
    }
}
