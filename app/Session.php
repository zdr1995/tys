<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Session extends BaseModel
{
    protected $table='sessions';
    protected $fillable=[
        'name',
        'public',
        'creator_id'
    ];

    protected $rules=[
        'name'=>'string|required'
    ];
    // public function subsessions()
    // {
    //     $subsessions=(DB::select(DB::raw( "select distinct sub.* from subsessions sub,training_relation_models s where s.session_id=$this->id and sub.id=s.subsession_id" )));
    //     return $subsessions;
    // }
    public function validate(Request $r)
    {
        $validate=\Validator::make($r->all(), $this->rules);
        if($validate->fails())
            return false;

        return true;
    }
    public function preCreate(Request $r)
    {
        $this->creator_id=$this->getLoggedIn()->id;
        return true;
    }
    

    public function postCreation(Request $r)
    {
        return true;
    }
    public function specificUpdate(Request $r)
    {
        return false;
    }
    public function preUpdate(Request $r, $id)
    {
        return true;
    }
    public function postUpdate(Request $r)
    {
        return true;
    }
    public function deleteMany(Request $r)
    {
        if( !$r->has('ids') )
            return false;
        foreach( $r['ids'] as $id)
        {
            if( !$instance->delete() )
                return false;
        }
        return true;
    }

   
    // public function checkConstraints()
    // {
    //     foreach($this->subsessions() as $subsession) {
    //         if(! $subsession->checkConstraints() )
    //             return false;
    //     }
    //     if(!$this->delete())
    //         return false;
    //     return true;
    // }
    // public function subsessions()
    // {
    //     return DB::select(DB::raw("SELECT * FROM subsessions 
    //                                  where id in(
    //                                      select subsession_id 
    //                                      from session_subsessions 
    //                                      where session_id=$this->id
    //                                      )"
    //                                 ));
    // }

}
