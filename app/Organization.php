<?php

namespace App;


class Organization extends BaseModel
{
    protected $table='organizations';
    protected $fillable=['name','public'];
    protected $casts=[
        'public'=>'int'
    ];

    public function teams()
    {
        return $this->hasMany('App\Team', 'id', 'team_id');
    }
    public function appendTeams(array $data)
    {
        $pub_teams=[];
        $pub_count=0;
        foreach(Team::where('organization_id',$this->id)->get() as $team)
        {
            /// append all teams into organization
            $all_teams=[];
            if($data['team_visible']->contains('team_id',$team->id) || $data['org_visible']->contains('id',$team->organization_id))
            {
                $pub_count++;
                $team->public=2;
            }    
            else {
                $team->public=0;
            }

            if( !$data['org_visible']->contains('id',$team->organization_id) &&
                $data['team_visible']->contains('id',$team->id)
            )
               $pub_count++;
            $pub_teams[]=$team;
        // if( $data['unsorted']->contains('id',$team->id) )
        //     $pub_teams[]=$team;
        }
        // dd($pub_teams);
        $this->public=($pub_count)?1:0;
        // dd($this->public);
        $this->teams=$pub_teams;
    }
    public function appendUnsorted(array $data)
    {
        $unsorted=[];
        if( isset($data) && (count($data) || $data->count()>0 ) )
        {
            // foreach($data['unsorted'] as $unsorted_user)
            // {
            //     // dd($unsorted_user);
            //     $user= User::where('id',$unsorted_user->user_id)->first();
            //     if( isset($user) && $user->organization_id==$this->id)
            //         $unsorted[]= $user;
            //     if (!$data['org_visible']->contains('id',$user->organization_id))
            //         $this->public=1;
            // }
            $users= User::where('organization_id',$this->id)
                            ->whereNull('team_id')
                            ->where('role','<>','superadmin')
                            ->get([
                                'id',
                                'first_name',
                                'last_name',
                                'email'
                            ]);
            foreach($users as $user)
            {

                if($data['unsorted']->contains('user_id',$user->id))
                {
                    $this->public=1;
                    $user->public=2;
                }else {
                    $user->public=0;
                }
                $unsorted[]= $user;
            }
        }
        $this->unsorted=$unsorted;
    }

    public function loadOrgsNestedByUser()
    {
        $user= $this->getLoggedIn();
        switch($user->role)
        {
            case 'superadmin':
            {
                $orgs= Organization::all();
                foreach($orgs as $org)
                {
                    $teams= Team::where('organization_id','=',$org->id)->get();
                    $org->setAttribute('teams',$teams);
                    // dd($org);
                }
            };break;
            case 'admin':
            {
                $orgs= Organization::where('id','=',$user->organization_id);
                foreach($orgs as $org)
                {
                    $teams= Team::where('organization_id','=',$org->id)->get();
                    $org->setAttribute('teams',$teams);
                }
            }
        }
        return $orgs;
    }
}
