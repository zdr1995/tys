<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

define('VISIBLE',2);
define('PARTIAL',1);
define('NOT_VISIBLE',0);

class Publish extends BaseModel
{
    protected $data;
    protected $organization_ids;
    protected $team_ids;
    protected $unsorted_ids;
    public function __construct($training_id)
    {
        $this->training_id= $training_id;
        $this->training= Training::where('id','=',$training_id)->first();
    }
    public function changeState(Request $r)
    {
        $this->new_data= $r;
        $this->organization_ids= $r['organizations'];
        // dd($r['organizations']);
        $this->team_ids= $r['teams'];
        $this->unsorted_ids= $r['unsorted'];
        return $this->detectChanges();
    }
    public function detectChanges()
    {
        if( !$this->changeVisibleForUnsorted() )
            return false;
        
        if( !$this->changeVisibleForTeams() )
            return false;

        if( !$this->changeVisibleForOrganizations() )
            return false;

        if( !$this->trainingStateUpdate() )
            return false;
        return true;
    }

    public function changeVisibleForOrganizations()
    {
        $organizations= new OrganizationVisible();
        if( !$organizations->checkAndDeleteOldData($this->training_id) )
            return false;
        if( is_array($this->organization_ids) )
        {
            foreach($this->organization_ids as $organization)
            {
                $success= OrganizationVisible::insert([
                    'organization_id'=> $organization,
                    'training_id'=> $this->training_id
                ]);
                if( !$success )
                    return false;
            }
        }
        return true;
    }
    public function changeVisibleForTeams()
    {
        $teams= new TeamVisible();
        if( !$teams->checkAndDeleteOldData($this->training_id) )
            return false;
        if( is_array($this->team_ids) && count($this->team_ids)>0 )
        {
            foreach($this->team_ids as $team)
            {
                $success= TeamVisible::insert([
                    'team_id'=> $team,
                    'training_id'=> $this->training_id
                ]);
                if( !$success )
                    return false;
            }
        }
        return true;
    }
    public function changeVisibleForUnsorted()
    {
        $unsorted= new UnsortedVisible();
        if( !$unsorted->checkAndDeleteOldData($this->training_id) )
            return false;

        if( is_array($this->unsorted_ids) )
        {
            // $unsorted-> insertIntoTable( $this->unsorted_ids,$this->training_id );
            foreach( $this->unsorted_ids as $unsorted )
            {
                $success= UnsortedVisible::insert([
                    'user_id'=> $unsorted,
                    'training_id'=> $this->training_id
                ]);
                if( !$success )
                    return false;
            }
        }
        return true;
    }

    public function loadPublishByTrainingId()
    {
        $org_visible= OrganizationVisible::where('training_id','=',$this->training_id)->get();
        $team_visible= TeamVisible::where('training_id','=',$this->training_id)->get();
        $unsorted_visible= UnsortedVisible::where('training_id','=',$this->training_id)->get();
        $data=[
            'org_visible'=> $org_visible,
            'team_visible'=> $team_visible,
            'unsorted'=> $unsorted_visible
        ];
        $public_teams=[];
        $returnData=[];
        foreach($orgs=Organization::all() as $organization)
        {
            $organization->public=0;
            $organization->appendTeams($data);
            $organization->appendUnsorted($data);
            // dd($organization->public);
            \Log::info("Public org",['name'=>$organization->name,
                                        'public'=>$organization->public
                                    ]);
            if($org_visible->contains('organization_id',$organization->id))
            {
                //  we know here that organization contains 
                $organization->public=2;
                //  current organization
                // $organization->appendTeams($data);
            } else {
                // $organization->public= NOT_VISIBLE;
                // if($organization->teams->count()>0=[];
                // if(isset($unsorted_visible))
                //     $organization->public= PARTIAL;
                // elseif(isset($unsorted_visible))
                //     $organization->public= PARTIAL;
                // else $organization->public= NOT_VISIBLE;
            }
            
        }
        $returnData['organizations']= $orgs;
        $unsorted_data=[];
        $users= User::whereNull('organization_id')
                        ->where('role','<>','superadmin')
                        ->whereNull('team_id')
                        ->get([
                            'id',
                            'first_name',
                            'last_name',
                            'email'
                        ]);
        foreach($users as $unsorted)
        {
            if( $data['unsorted']->contains('user_id',$unsorted->id) )
            {
                $unsorted->public=2;
            }else {
                $unsorted->public=0;
            }
                // $unsorted_data[]=$un;
        }
        $returnData['unsorted']=$users;
        return $returnData;
    }
    public function trainingStateUpdate()
    {
        $training= Training::where('id',$this->training_id)->first();
        // dd($training,$this->training_id);
        if(isset($training))
        {
            $training->public=3;
            if( !$training->save() )
                return false;
        }
        return true;
    }

    public function isVisibleByUser(User $user)
    {
        switch($user->role)
        {
            case 'superadmin': return true;break;
            case 'admin':
            {
                $exists= UnsortedVisible::where('training_id','=',$this->training_id)
                                            ->where('user_id','=',$user->id)
                                            ->first();
                if( $exists!==null )
                    return true;
                
                $org= Organization::where('id',$user->organization_id)->first();
                $exists= OrganizationVisible::where('training_id','=',$this->training_id)
                                                ->where('organization_id','=',$user->organization_id)
                                                ->first();
                if( $exists!==null ) 
                    return true;
                
                return false;
            };break;
            case 'trainer':
            {
                $exists= UnsortedVisible::where('training_id','=',$this->training_id)
                                            ->where('user_id','=',$user->id)
                                            ->first();
                if( $exists!==null )
                    return true;
                
                $exists= OrganizationVisible::where('training_id','=',$this->training_id)
                                                ->where('organization_id','=',$user->organization_id)
                                                ->first();
                
                // dd($exists);
                if( $exists!==null )
                    return true;
            
                return false;
            }
            default: return false;break;
        }
    }
}
