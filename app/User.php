<?php

namespace App;

use DB;
use Exception;
use App\Classes\Storage;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Facades\JWTAuth;
use Intervention\Image\ImageManagerStatic as Image;
use League\Flysystem\FileExistsException;
use stdClass;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;
// use Chatkit\Chatkit;
// use Chatkit\Exceptions\ChatkitException;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'first_name', 
        'last_name', 
        'email', 
        'role', 
        'gender', 
        'password', 
        'parent_id',
        'username',
        'team_id',
        'organization_id',
        'identifier',
        'fcm_token'
    ];

    protected $rules=[
        // 'first_name'    => 'required|string',
        // 'last_name'     => 'required|string',
        'email'         =>  'required|email',
        'role'          => 'required'
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        'identifier',
        'parent_id',
        'fcm_token',
        'created_at',
        'updated_at'
    ];

    protected static $responseData=[
        'users.id as id',
        'users.first_name',
        'users.last_name',
        'users.role',
        'users.gender',
        'users.parent_id',
        'users.username',
        'users.team_id',
        'teams.name as team_name',
        'organizations.name as organization_name',
        'users.team_id',
        'users.organization_id',
        'users.email',
        'users.profile_picture'
    ];
    protected $table= 'users';
    protected $chatkit;
    public static $storage_path='users';
    protected static $default_storage_path='users/default';
    public static $default_picture='https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fkooledge.com%2Fassets%2Fdefault_medium_avatar-57d58da4fc778fbd688dcbc4cbc47e14ac79839a9801187e42a796cbd6569847.png&f=1&nofb=1';
    public function getJWTIdentifier()
    {
        // TODO: Implement getJWTIdentifier() method.
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function __construct()
    {
        /*
        | We can disable this in .env
        */
        if($this->isChatEnabled())
        {
            // $chatkit_connection=[
            //     'instance_locator'=> env('CHATKIT_INSTANCE_LOCATOR'),
            //     'key' => env('CHATKIT_KEY')
            // ];
            // $this->chatkit= new Chatkit($chatkit_connection);
        }
    }
    public function getLoggedIn()
    {
        try{
            return JWTAuth::parseToken()->authenticate();
        }catch(\JWTException $e) {
            return false;
        }
    }
    public function parents() {
        return $this->hasOne(self::class,'parent_id');
    }
    public function childrens() {
        // return static::where('parent_id',$this->id)->join('users as u',function($join){
        //     $join->on('u.parent_id','=',$this->id);
        // });
        $query="
            SELECT * 
            FROM users u 
            WHERE u.parent_id is NULL OR u.parent_id IN(
                SELECT p.id 
                FROM users p 
                WHERE p.parent_id=$this->id 
                OR p.parent_id is NULL
            )
        ";
        $res= DB::raw($query);
        return $res;
    }
    public function teams()
    {
        return $this->hasMany('\App\Team');
    }
    public function relations()
    {
        foreach ($this->hasMany('\App\TrainingRelationModel') as $relations)
        {
            $relation=[];
            // dd($relations);
        }
        return $this->hasMany(TrainingRelationModel::class);
    }
    public function sessions()
    {
        $sessions= DB::select(DB::raw("select distinct s.* from training_relation_models t,sessions s where t.user_id=$this->id and t.session_id=s.id "));
        return $sessions;
    }
    public function getTrainings()
    {
        $trainings= DB::select(DB::raw("select * from training_relation_models rel, trainings t where rel.training_id=t.id and rel.user_id=$this->id"));
        return $trainings;
    }
    public function trainer()
    {
        $trainer= DB::select(DB::raw( "select distinct u2.* from users u1,users u2 where u1.id!=u2.id and u1.parent_id=u2.id and u1.id=$this->id" ));
        return $trainer;
    }

    // public static function get_default_user_folder()
    // {
    //     return Storage::disk()->path('');
    // }
    public function SetProfilePicturesAttribute()
    {
        $picture='';
        $picture=($this->profile_picture!=='null')?$this->profile_picture:'null';
        $this->attributes['profile_pictures']=$picture;
    }
    public function GetProfilePicturesAttribute()
    {
        $profile_picture= ($this->profile_picture!==null)?$this->profile_picture:'/media/user/'.$this->id;
    }
    
    protected static function get_default_image_full_path()
    {
        return Storage::disk()->path(static::$default_storage_path);
    }
    public function storePicture(Request $r)
    {
        if($r->has('profile_picture')) {
            if(!strpos($r['profile_picture'],'media'))
            {
                    $image=$r->file('profile_picture');
                    if(Storage::disk()->putFileAs(static::$storage_path,$image,$this->id))
                    {
                        $this->profile_picture='/storage/users/'.$this->id;
                        // $instance=static::where('email',$r['email'])->first();
                        // $instance->profile_picture=null;
                        // // dd($instance);
                        // $instance->save();
                    }
                        return true;
            }else {
                return true;
            }
        }else{
            // $picture='/media/'.$this->getRandomDefaultPicture();
            // if($picture==null)
                $this->profile_picture='null';
            // else $this->profile_picture=$picture;
        }
        // if($this->update())
            return true;
        
        return false;
    }
    protected function makeAndGetDefaultPath()
    {
        if(config('app.file-driver') != 'cloud')
        {
            $default_path=static::get_default_image_full_path();
            if(!is_dir($default_path)) {
                mkdir($default_path,0775,true);
            }
            return $default_path;
        }
    }
    public function getRandomDefaultPicture()
    {
        $path=$this->makeAndGetDefaultPath();
        $files=[];
        $default_files= Storage::disk()->files( static::$default_storage_path );
        // this is path of default picture path
        if($default_files!==null && count($default_files)>0) {
            $randomPicture=$default_files[array_rand($default_files,1)];
            return $randomPicture;
        }else return null;
        
        // $randomPicture;
        //$this->default_picture=Storage::disk()->get( static::$default_storage_path.'/');
    }
    public function storagePath()
    {
        // dd();
        return storage_path().'/users';
    }
    public function validate(Request $r)
    {
        $validation=Validator::make($r->all(), $this->rules);
        if( $validation->fails() )
            return false;
        return true;
    }
    public function preCreate(Request $r)
    {
        //  This is user who try to add another user
        $user= JWTAuth::parseToken()->authenticate();
        if( !$this->checkUserExists($r) )
            return false;
        if ( !$user->checkCreatePermissions($r) )
            return false;
        
        // ToDo

        return true;
    }
    public function checkUserExists(Request $r)
    {
        $exists= static::where('email',$r['email']);
        if($r->has('username'))
            $exists->orWhere('username',$r['username']);
        if( $exists->first()!==null  )
            return false;
        return true;
    }
    public function preUpdate(Request $r,$id)
    {
        \Log::alert("Zahtjev za update user ",(array)$r->all());
        $loggedInUser= JWTAuth::parseToken()->authenticate();
        // If mails is different then we must send confirm null
        if($loggedInUser->email!==$r['email'])
            return true;

        if( !$r->has('fcm_token'))
            return true;

        $loggedInUser->checkAnotherUserFCMTokens($r['fcm_token']);
        return true;
    }

    public function postCreation(Request $r)
    {
        // dd($this);
        if($r->has('profile_picture')) {
            try{
                $image=$r->file('profile_picture');
                if(Storage::disk()->putFileAs($this->storagePath(),$image,$this->id)) {
                    $this->profile_picture='/storage/users/'.$this->id;
                    return true;
                }
                    
            }catch(Intervention\Image\Facades\Image\NotreadableException $r)
            {
                return false;
            }
            $this->profile_picture='null';
        }else{
            $this->profile_picture='null';
            // dd($this->getRandomDefaultPicture());
            
            // return true;
            // Storage::disk()->copy($this->getRandomDefaultPicture(), '/users/'.$this->id);
            // $this->profile_picture='/media/users/'.$this->id;
            // $this->profile_picture='/media/'.$this->getRandomDefaultPicture();
        }
        
        

        $this->password= \Hash::make('user1234');

        /////////////////////////////////
        //      Check organization     //
        /////////////////////////////////
        if( $r->has('organization_name') )
        {
            $org_id=Organization::insertGetId(['name'=>$r['organization_name']]);
            $this->organization_id=$org_id;
        }
        if($r->has('organization_id'))
        {
            $this->organization_id=$r['organization_id'];
        }
        ////////////////////////////////
        //      Check team            //
        ////////////////////////////////
        if( $r->has('team_name') )
        {
            $team_id= Team::insertGetId(['name'=>$r['team_name'],'organization_id'=>$this->organization_id]);
            $this->team_id= $team_id;
        }
        if( $r->has('team_id') ) {
            $this->team_id= $r['team_id'];
        }

        if(!$this->save())
            return false;

        $creator= JWTAuth::parseToken()->authenticate();
        if($this->role!=='superadmin' ) {
            $this->parent_id=$creator->id;
            // ToDo make logic for this
            // $this->team_id=$creator->team_id;
            // $this->organization_id=$creator->organization_id;
        } else $this->parent_id=null;

        (new TrainingOrder)->newUserOrder($this);
        (new VisibleTraining)->getParentsVisibility($this);

        if($this->isChatEnabled())
        {
            $chatkit= new Chat;
            try{
                $chatkit->createChatkitUser($this);
            }catch(Exception $e) {
                \Log::info('Nije dodao korisnika u chat');
            }
        }
        if(! $this->save())
            return false;
        
        $this->setFirstTimeCreditials();

        return true;
    }


    public function setFirstTimeCreditials()
    {
        if($this->email){
            $hashedCreditials=new stdClass();
            $hashedCreditials->data=env('PAJIN_LINK').\Hash::make($this->email);
            $hashedCreditials->message='Register';
            $this->mailToRegister($hashedCreditials);
            $this->identifier=\Hash::make($this->email);
            $this->password=\Hash::make(env('DEFAULT_PASS'));
        }
        if(! $this->save())
            return false;
        return true;    
    }

    public function checkCreatePermissions(Request $r)
    {
        // trainer cannot add another admin
        $exists= static::where('email','=',$r['email'])->first();
        if ($exists!==null)
            return false;
        // if( $this->type=='trainer' )
        //     if( $r['role']==='trainer' || $r['role']==='admin' )
        //         return false;
        if( $this->type=='user')
            return false;
        return true;
    }
    public function loadSingle($id=false,$columns='*')
    {
        if($id) {
            $user=User::find($id);
            $organization=Organization::where('id',$user->organization_id)->first();
            if(isset($organization))
                $user->organization_name= $organization->name;
            $team=Team::where('id',$user->team_id)->first();
            if(isset($team))
                $user->team_name=$team->name; 
            return $user;
        }else {
            $user=static::find(static::max('id'));
            $organization=Organization::where('id',$user->organization_id)->first();
            if(isset($organization))
                $user->organization_name= $organization->name;
            $team=Team::where('id',$user->team_id)->first();
            if(isset($team))
                $user->team_name=$team->name; 
            return $user;
        }
        
        // return $user;
    }

    public function loadAll($getQuery=false)
    {
        $loggedIn= JWTAuth::parseToken()->authenticate();
        $allData=static::where('users.id','<>',$loggedIn->id)->select(static::$responseData);
        $allData->leftJoin('organizations',function ($query) {
            $query->on('organizations.id','=','users.organization_id');
        });
        $allData->leftJoin('teams', function ($query) {
            $query->on('teams.id','=','users.team_id');
        });
        if($loggedIn->role=='admin') 
            $allData->where('users.organization_id','=',$loggedIn->organization_id)->where('users.role','not like','superadmin');
        
        $inOrg=User::where('organization_id',$loggedIn->organization_id)->get(['id']);
        if($loggedIn->role=='trainer') {
            $allData=$allData->where('users.organization_id','=',$loggedIn->organization_id)->orWhere(['users.id'=>'in'],['users.role'=>'admin']);
        }
        
        if (!$getQuery) {
            $data= $allData->where(function($query){
                $query->where('users.first_name','not like','%Regular%');
                $query->orWhereNull('users.first_name');
            })->orderBy('users.id','desc')->get(static::$responseData);
            foreach ($data as $user)
            {
                if($user->profile_picture!==null)
                {
                    $pic=Storage::disk()->exists($user->profile_picture);
                    if($pic==false)
                        $user->profile_picture=static::$default_picture;
                }else $user->profile_picture=static::$default_picture;
            }
            return $data;
        }
        // else return $allData;
    }
    
    public function postUpdate(Request $r)
    {
        
        if( $r->has('profile_picture') )
        {
            if($this->storePicture($r) && !strpos($r['profile_picture'],'media'))
            {
                $this->profile_picture='/storage/users/'.$this->id;
                $this->save();
            }else {
                $this->profile_picture=$r['profile_picture'];
            }
        }else {
            // $this->profile_picture=getRandomDefaultPicture();
            // $this->save();
        }

        if($r->has('organization_id') && $r->has('organization_name'))
        {
            $org= Organization::where('id',$r['organization_id'])->first();
            $org->name= $r['organization_name'];
            if( !$org->save() )
                return false;
        }
        if($r->has('team_id') && $r->has('team_name'))
        {
            $team= Team::where('id',$r['team_id'])->first();
            $team->name= $r['team_name'];
            if( !$team->save() )
                return false;
        }
        if($r->has('organization_name') && !$r->has('organization_id'))
        {
            $org_id= Organization::insertGetId(['name'=>$r['organization_name']]);
            $this->organization_id= $org_id;
            // dd($org_id);
        }
        if($r->has('team_name') && !$r->has('team_id'))
        {
            $team_id= Team::insertGetId(['name'=> $r['team_name']]);
            $team= Team::find($team_id);
            
            $this->team_id= $team_id;
        }
        
        if($this->isChatEnabled())
        {
            //  Patch pusher account
            $chatkit= new Chat;
            $chatkit->patchPusherAccount($this);
        }
        $this->save();
        return true;
    }
    public function checkConstraints()
    {
        return true;
    }

    public function changePassword(Request $r)
    {
        $this->identifier='NULL';
        $this->save();
        $this->password= Hash::make($r['password']);
        // if($this->email){
        //     $hashedCreditials=new stdClass();
        //     $hashedCreditials->data=env('PAJIN_LINK').\Hash::make($this->email);
        //     $hashedCreditials->message='reset password';
        //     $this->mailToRegister($hashedCreditials);
        //     $this->identifier=\Hash::make($this->email);
        // }
        if(! $this->save())
            return false;
        return true;    
    }



    public function mailToRegister($hashedData)
    {
        
        \Mail::send('registerMail',['creditials'=>$hashedData], function ($message) {
            $message->from(config('mail.username'), config('mail.name'));
            $message->sender(config('mail.from.address'), config('mail.name'));
            $message->to($this->email, $this->full_name);
            $message->subject('Register');
            $message->priority(3);
        });
        return true;
    }
    public function bulkMail(User $user)
    {
        \Mail::send('registerBulk',['user'=>$user], function ($message)use($user) {
            // dd($user);
            $message->from(config('mail.username'), config('mail.name'));
            $message->sender(config('mail.from.address'), config('mail.name'));
            $message->to($user->email, $user->full_name);
            $message->subject('Register');
            $message->priority(3);
        });
    }
    public function GetFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }
    public function specificUpdate(Request $r)
    {
        if($r->has('order'))
        {
            try{
                $user= JWTAuth::parseToken()->authenticate();
            }catch(JWTException $e) {
                return false;
            }
            $arr=implode(',',$r['order']);
            // dd($arr);
            if($user) {
                $orders= TrainingOrder::where('user_id',$user->id)->get();
                // dd($order);
                foreach($orders as $order) {
                    $order->delete();
                }
                
                    TrainingOrder::insert(['user_id'=>$user->id,'table'=>ucfirst($r['table']),'order'=>$arr]);
            } else {
                return false;
            }
            return 'success';
        }
        return false;
    }
    public function user_parents()
    {
        // $users=DB::select(DB::raw("SELECT distinct(u.id) FROM users u
        //                              JOIN users up on up.id=u.parent_id
        //                              JOIN users ugp on ugp.id=up.parent_id
        //                              GROUP BY u.id"));
            $user= DB::select(DB::raw("SELECT up.* 
                                        FROM users u,users up
                                        WHERE u.id=up.parent_id and u.id=$this->id"));
        return $user;
    }
    public function user_childrens()
    {
        $user= DB::select(DB::raw("SELECT u.*
                                     FROM users u,users up
                                     WHERE u.parent_id=$this->id and u.id<>up.id"));
    }
    public function inTeam()
    {
        $query= static::select();

        if( $this->role=='admin')
            $query->where('role','=','admin')->orWhere('role','=','superadmin')->orWhere('team_id',$this->team_id);

        if( $this->role=='user')
            $query->where('role','=','superadmin')->where('team_id',$this->team_id);

        return $query->get();
    }
    public function getInTeamIds()
    {
        return $this->inTeam()->pluck('id');
    }
    public function inOrganization($getQuery=false)
    {
        $query= static::select();

        if( $this->role=='admin')
            $query->where('role','=','admin')->orWhere('role','=','superadmin')->orWhere('team_id',$this->team_id);

        if( $this->role=='user')
            $query->where('role','=','superadmin')->where('team_id',$this->team_id);
        return $query->get();
    }
    public function getInOrganizationIds()
    {
        return $this->inOrganization()->pluck('id');
    }
    public function deleteMany(Request $r)
    {
        if (!$r->has('ids'))
            return false;
        foreach($r['ids'] as $id)
        {
            $user= static::find($id);
            if( !$user->delete() )
                return false;

            if($this->isChatEnabled())
            {
                $chat= new Chat();
                $chat->deleteUser($user);
            }
        }
        if( !$this->deleteEmptyOrganizations() )
            return false;
        if( !$this->deleteEmptyTeams() )
            return false;

        return true;
    }
    public function deleteEmptyOrganizations()
    {
        $query= "SELECT distinct(organizations.id) FROM organizations LEFT JOIN users on organizations.id=users.organization_id WHERE users.id is null";
        $data= DB::select(DB::raw($query));
        foreach($data as $row)
        {
            $record= Organization::where('id','=',$row->id)->first();
            try {
                $record->delete();
                if($this->isChatEnabled())
                {
                    $chatRoom= new Chat;
                    $chatRoom->deleteOrganizationRoom($record);
                }
            }catch(Exception $e) {

            }
        }
        return true;
    }
    public function deleteEmptyTeams()
    {
        $query= "SELECT teams.id FROM teams LEFT JOIN users on teams.id=users.team_id WHERE users.id IS NULL";
        $data= DB::select(DB::raw($query));
        foreach($data as $row)
        {
            $record= Team::where('id','=',$row->id)->first();
            try {
                $record->delete();
                $chatRoom= new Chat;
                $chatRoom->deleteTeamRoom($record);
            }catch(Exception $e) {

            }
        }
        return true;
    }
    public function getLastInserted()
    {
        $user= static::find(static::max('id'));
        
        $organization=Organization::where('id',$user->organization_id)->first();
        if(isset($organization))
            $user->organization_name= $organization->name;
        $team=Team::where('id',$user->team_id)->first();
        if(isset($team))
            $user->team_name=$team->name; 
        return $user;
    }
    public function loadSuperAdmins()
    {
        $user= $this->getLoggedIn();
        // dd($user);
        if($user->role=='superadmin') {
            $users= DB::select(DB::raw("SELECT * FROM users WHERE role like 'superadmin' AND users.id<>$user->id"));
            // dd("tu si");
            return $users;
        }
        else return [];
    }
    // public function loadUnsorted()
    // {
    //     $users=DB::select(DB::raw("SELECT * FROM users WHERE organization_id IS NULL AND team_id IS NULL"));
    //     return $users;
    // }
    public function loadByOrgId($id)
    {
        $loggedIn= $this->getLoggedIn();
        $query="SELECT ".implode(',',static::$responseData)." FROM users JOIN organizations ON organizations.id=users.organization_id JOIN teams ON teams.id=users.team_id WHERE users.organization_id=$id AND users.id<>$loggedIn->id AND users.first_name not like 'Regular'";
            // dd($query);
        $data= DB::select(DB::raw($query));
        return $data;
    }
    public function loadNestedOrganizations()
    {
        $data=[];
        $organizations= DB::select(DB::raw("SELECT id,name FROM organizations"));
        foreach($organizations as $organization) {
            if($organization!==null && $organization && isset($organization->id) && $organization->id !==NULL && $organization->id !=='NULL') {
                // var_dump($organization->id);
                $organization->admin= $this->loadAdminForOrganizations($organization->id);
                $organization->unsorted=$this->loadUnsorted($organization,'org');
                $organization->teams= $this->loadNestedTeams($organization->id);
            }   
        }
        // dd();
        return $organizations;
    }
    public function loadUnsorted($org=false,$type)
    {
        $admin=(isset($org->admin) && $org->admin!==null && isset($org->admin->id))?$org->admin->id:'null';
        $loggedIn= $this->getLoggedIn();
        switch($type)
        {
            case 'org':
            {
                $unsorted= DB::select(DB::raw("SELECT * FROM users WHERE organization_id= $org->id AND team_id IS NULL and id<>$admin AND id<>$loggedIn->id"));
            };break;
            case 'global':
            {
                $unsorted= DB::select(DB::raw("SELECT * FROM users WHERE organization_id is NULL AND team_id IS NULL AND users.role not like 'superadmin' AND users.id<>$loggedIn->id"));
            }
        }
        return $unsorted;
    }
    public function loadAdminForOrganizations($org_id)
    {
        $loggedIn= $this->getLoggedIn();
        $admin= DB::select(DB::raw("SELECT * FROM users WHERE role like 'admin' AND organization_id=$org_id AND users.id<>$loggedIn->id LIMIT 1"));
        if(isset($admin[0]))
            return $admin[0];
        return $admin;
    }
    public function loadNestedTeams($org_id)
    {
        $teams= DB::select(DB::raw("SELECT id,name FROM teams WHERE organization_id=$org_id"));
        if($teams!==null && count($teams)>0 && isset($teams) && isset($teams[0]))
        {
            $team=$teams[0];
            $team_id=$team->id;
            
            $team->users=$this->loadUsersByTeam($team_id);
            $trainer= DB::select(DB::raw("SELECT * FROM users WHERE team_id=$team_id AND role like 'trainer' LIMIT 1"));

            if(isset($trainer[0]))
            {
                $team->trainer=$trainer[0];
                if($team->trainer->profile_picture=='null')
                    $team->trainer->profile_picture=static::$default_picture;
            }
                
            // else $team->trainer=$trainer;
            return $teams;
        }
    }
    public function loadUsersByTeam($team_id)
    {
        $loggedIn=$this->getLoggedIn();
        $users= DB::select(DB::raw("SELECT * FROM users WHERE team_id= $team_id AND users.role not like 'trainer' AND users.id<>$loggedIn->id "));
        return $users;
    }
    public function loadAllWithOrganizations(Request $r)
    {
        // $users = static::join('organizations',function($join){
        //     $join->on('organizations.id','=','users.organization_id');
        // })
        // ->join('teams',function($join){
        //     $join->on('teams.id','=','users.team_id');
        // })->get();
        $users=[];
        $superadmins=$this->loadSuperAdmins();
        $users["superadmins"]=$superadmins;
        $unsorted=$this->loadUnsorted('','global');
        $users['unsorted']=$unsorted;
        $users['organizations']=$this->loadNestedOrganizations();
        // dd($users);
        return $users;
    }
    public function getByTeamId($id)
    {
        $loggedIn= $this->getLoggedIn()->id;
        $query= DB::select(DB::raw("SELECT * FROM users WHERE team_id=$id AND users.id<>$loggedIn;"));
        return $query;
    }
    public function returnCreated()
    {
        $user= static::find(static::max('id'));
        
        $organization=Organization::where('id',$user->organization_id)->first()->name;
        if(isset($organization))
            $user->organization_name= $organization->name;
        $team=Team::where('id',$user->team_id)->first();
        if(isset($team))
            $user->team_name=$team->name; 
        return $user;
    }
    /**
     * @r contains
     * @param Illuminate\Http\Request $r
     */
    public function multiCreate(Request $r)
    {
        // dd($r->all());
        $user_ids=[];
        
        foreach($r->all() as $user)
        {
            $team_id=null;
            $user_exists= User::where('email',$user['email'])->first();
            if($user_exists==null)
            {
                if(isset($user['team_id']))
                    $team_id=$user['team_id'];
                
                if( isset($user['team_name']))
                    $team_id= Team::insertGetId(['name'=>$user['team_name'],'organization_id'=>$user['organization_id']]);
                
                $req= new Request([
                    'email'=> $user['email'],
                    'role'=> $user['role'],
                    'organization_id'=>$user['organization_id'],
                    'team_id'=> $team_id,
                    'first_name'=>'New',
                    'last_name'=>'User',
                    'password'=> \Hash::make(env('DEFAULT_PASS'))
                ]);
                $id= static::insertGetId($req->all());
    
                if(!$id)
                    return 'failed';
                $user= static::find($id);

                $chatkit= new Chat();
                $chatkit->createChatkitUser($this);

                $this->bulkMail($user);
                $user_ids[]=$id;
            }
        }
        $response=[];
        foreach($user_ids as $id)
        {
            $response[]=(new User)->loadSingle($id);
        }
        return $response;
    }
    public function checkAnotherUserFCMTokens(string $fcm)
    {
        $users= static::where('fcm_token','=',$fcm)->get();
        \Log::info("Korisnici sa tim tokenom",(array)$users);
        foreach($users as $user)
        {
            $user->fcm_token=null;
            $user->save();
        }
    }
    
    
    
    public function getChatRooms(User $user,string $type)
    {
        switch($type)
        {
            case 'team':
            {
                switch($user->role)
                {
                    case 'superadmin':
                    {
                        return $this->chatkit->getRoom([]);
                    break;
                    }
                    case 'admin':
                    {
                        $teams= Team::where('organization_id','=',$user->organization_id)->get();
                        
                    break;
                    }
                    case 'trainer':
                    case 'user':
                    {

                    break;
                    }
                }
                $rooms= $this->chatkit->getRoom(['id'=>'Team-']);
            break;
            }
            case 'organization':
            {

            }
        }
    }
    /**
     * Return chatkit user
     *
     * @return array|mixed|null
     */
    public function Chatkit()
    {
        if(!$this->isChatEnabled())
            return null;
        $chatkit= new Chat();
        return $chatkit->loadChatkitUser($this->id);
    }

    public function isChatEnabled()
    {
        return config('app.chatkit-enabled') == true;
    }

    /*
    | RESET PASSWORD WITH TEMPORARY MAIL KEY
     */
    public function resetPasswordRequest()
    {
        $code = static::generateRandomCode();
        $this->identifier = \Hash::make($code);
        if(!$this->save())
            return response()->json([
                'message' => 'Server error'
            ], 500);

        $user = $this;
        // dd(config('mail'));
        \Mail::send('changePassword',['user'=>$this, 'code' => $code], function ($message)use($user) {
            // dd($user);
            $message->from(config('mail.username'), config('mail.name'));
            $message->sender(config('mail.from.address'), config('mail.name'));
            $message->to($user->email, $user->full_name);
            $message->subject('Change password');
            $message->priority(3);
        });
        return response()->json([
            'message' => 'Code sen\'t on email.',
            'data' => $user
        ], 200);
    }

    public function resetPasswordCode(Request $r)
    {
        $code_valid = Hash::check($r->code, $this->identifier);
        if($code_valid)
        {
            $this->identifier = 'NULL';
            return $this->save();
        }

        // Code is invalid
        return false;
    }

    public function generateRandomCode()
    {
        $characters = '0123456789';
        $randstring = '';
        for ($i = 0; $i < 6; $i++) 
            $randstring .= $characters[rand(0, strlen($characters)-1)];
        return $randstring;
    }
    /*
    |--------------------------------------------------
    | Used for validation user's identifier/code
    | when he 
    |--------------------------------------------------
     */
    public function validateUserIdentifier(Request $r)
    {

    }
    
}

/*

    u.id,
    u.first_name,   
    u.last_name,
    u.email,
    u.password,
    u.gender,
    u.parent_id,
    u.username,
    u.team_name,
    u.organization_name,
    u.identifier 


*/


/*

 // If logged user is trainer then he only see his users
        // if($loggedIn->role=='trainer') {
        //     $allData=$allData->where('parent_id','=',$loggedIn->id);
        // }
        // // $allData=$allData->orderBy('created_at','DESC');
        // if($loggedIn->role=='admin') {
        //     // return DB::raw("SELECT * FROM users u join users p on p.parent_id=u.id join users q on q.parent_id=p.id where u.id=$loggedIn->id");
        //     $parent=$allData->where('parent_id','=',$loggedIn->id);
        //     // $allData=$parent->where('parent_id','in',$parent->id);
        //     // $allData->childrens();

        //     // return($allData);
        //     // $loggedIn->where('users.parent_id','=',$loggedIn->id);
        //     // $allData->join('users as p',function($q)
        //     // {
        //     //     $q->on('p.id','users.parent_id');
        //     // });
        //     // $loggedIn->parents();//->join(function($q){
        //         // $q->on('users.parent_id','=','users.id');
        //         // return $q;
        //     // });
        //         // $q->join('users.parent_id','=',$q->id);
        //     // });//->join('users',function($q) {
        //         //     $q->where('users.parent_id','=',$q->get()->id);
        //         // });
        //     // dd($loggedIn);
        // }
        // if($loggedIn->role=='superadmin')
        //     return static::where('parent_id','<>',$loggedIn->id)->orderBy('created_at','desc');
            

        
                            
        // dd($allData);


        //Put random images to all users from default folder
        // foreach(static::all() as $user)
        // {
        //     // dd($user);
        //     if($user->id !==1 && $user->id!==2)
        //     {
        //         try{
        //             Storage::disk()->copy($user->getRandomDefaultPicture(), '/users/'.$user->id);
        //         }catch(FileExistsException $e){

        //         }
                
        //         $user->profile_picture='/media/users/'.$user->id;
        //         // dd($user);
        //         $user->update(); 
        //     }
        // }

*/