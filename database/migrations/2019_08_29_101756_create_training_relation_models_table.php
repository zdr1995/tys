<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingRelationModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_relation_models', function (Blueprint $table) {
            $table->increments('id');
            $table->string('training_picture',50)->nullable();
            $table->string('session_picture',50)->nullable();
            $table->string('subsession_picture',50)->nullable();
            $table->string('practice_picture',70)->nullable();
            $table->string('practice_video',70)->nullable();
            $table->enum('active',['active','inactive']);

            // Relation tables
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('training_id')->unsigned()->nullable();
            $table->integer('session_id')->unsigned()->nullable();
            $table->integer('subsession_id')->unsigned()->nullable();
            $table->integer('practice_id')->unsigned()->nullable();


            $table->timestamps();
        });
        Schema::table('training_relation_models',function(Blueprint $table) {
           $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('set null');
           $table->foreign('training_id')->references('id')->on('trainings')->onDelete('set null');
           $table->foreign('session_id')->references('id')->on('sessions')->onDelete('set null');
           $table->foreign('subsession_id')->references('id')->on('subsessions')->onDelete('set null');
           $table->foreign('practice_id')->references('id')->on('practices')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_relation_models');
    }
}
