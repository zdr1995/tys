<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingSessionSubsessionPracticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_session_subsession_practices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('training_id')->unsigned();
            $table->integer('session_id')->unsigned();
            $table->integer('subsession_id')->unsigned();
            $table->integer('practice_id')->unsigned();

            $table->string('name');

            $table->string('practice_video')->nullable();
            $table->string('practice_picture')->nullable();

            $table->integer('creator_id')->unsigned();
            $table->integer('publisher_id')->unsigned();
            $table->tinyInteger('active');
            $table->tinyInteger('public');
            $table->timestamps();
        });
        Schema::table('training_session_subsession_practices',function(Blueprint $table) {
            $table->foreign('training_id')->references('id')->on('trainings')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('session_id')->references('id')->on('sessions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('subsession_id')->references('id')->on('subsessions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('practice_id')->references('id')->on('practices')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('publisher_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_session_subsession_practices');
    }
}