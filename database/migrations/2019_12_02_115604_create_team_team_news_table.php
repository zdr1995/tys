<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamTeamNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_team_news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_news_id')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('team_team_news', function(Blueprint $table) {
            $table->foreign('team_news_id')->references('id')->on('team_news')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('team_id')->references('id')->on('teams')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_team_news');
    }
}
