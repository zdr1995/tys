<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSessionSubsessionPracticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_subsession_practices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('session_id')->unsigned();
            $table->integer('subsession_id')->unsigned();
            $table->integer('practices_id')->unsigned();

            $table->string('name');
            $table->string('default_picture');


            $table->string('practice_video');
            $table->string('practice_picture');

            $table->integer('creator_id')->unsigned();
            $table->integer('publisher_id')->unsigned();
            $table->tinyInteger('active');
            $table->tinyInteger('public');
            $table->timestamps();
        });
        Schema::table('session_subsession_practices',function(Blueprint $table) {
            $table->foreign('session_id')->references('id')->on('sessions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('subsession_id')->references('id')->on('subsessions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('practices_id')->references('id')->on('practices')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('publisher_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_subsession_practices');
    }
}
