<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubsessionPracticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subsession_practices', function (Blueprint $table) {
            $table->integer('subsession_id')->unsigned();
            $table->integer('practice_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('practice_video');
            $table->string('practice_picture');
            $table->timestamps();
        });
        Schema::table('subsession_practices',function (Blueprint $table) {
            $table->foreign('subsession_id')->references('id')->on('subsessions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('practice_id')->references('id')->on('practices')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subsession_practices');
    }
}
