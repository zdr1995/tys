<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingSessionSubsessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_session_subsessions', function (Blueprint $table) {
            $table->integer('training_id')->unsigned();
            $table->integer('session_id')->unsigned();
            $table->integer('subsession_id')->unsigned();

            $table->string('name');
            $table->string('picture')->nullable();


            $table->integer('creator_id')->unsigned();
            $table->integer('publisher_id')->unsigned()->nullable();
            $table->tinyInteger('active');
            $table->tinyInteger('public');
            $table->timestamps();
        });
        Schema::table('training_session_subsessions',function(Blueprint $table){
            $table->foreign('training_id')->references('id')->on('trainings')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('session_id')->references('id')->on('sessions')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('subsession_id')->references('id')->on('subsessions')->onUpdate('cascade')->onDelete('cascade');

            $table->foreign('publisher_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('creator_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_session_subsessions');
    }
}
