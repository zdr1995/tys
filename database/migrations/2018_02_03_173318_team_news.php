<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeamNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        Schema::create('team_news', function(Blueprint $table) {
            $table->increments('id');
            $table->text('message');
            $table->integer('sender')->unsigned();
            $table->timestamps();
        });
        Schema::table('team_news', function(Blueprint $table) {
            $table->foreign('sender')->references('id')->on('users')
                                        ->onUpdate('cascade')
                                        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
