<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('url')->nullable();
            $table->enum('type',[1,2,3]);
            $table->integer('owner_id')->unsigned();
            $table->tinyInteger('public')->nullable();
            $table->integer('published_by')->unsigned()->nullable();
            $table->string('path')->nullable();
            $table->string('video_thumbnail')->nullable();

            $table->timestamps();
        });
        Schema::table('media',function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('published_by')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media');
    }
}
