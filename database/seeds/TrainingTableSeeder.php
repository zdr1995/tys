<?php

use App\User;
use App\Classes\Storage;
use Illuminate\Database\Seeder;

class TrainingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->checkFiles();
        $this->insertData();
        // factory(\App\Training::class,100)->create();
        $trainings=\App\Training::all();
        $all_ids=[];
        foreach($trainings->pluck('id') as $id)
        {
            $all_ids[]=$id;
        }
        foreach($trainings as $training) {
            $faker=\Faker\Factory::create();
            $publisher=[
                null,
                null,
                \App\User::inRandomOrder()->first()->id
            ];
            
            $data=[
                'active'=>array_rand([0,1],1),
                'public'=>array_rand([0,1],1),
                'creator_id'=>$publisher[array_rand($publisher,1)],
                'training_id'=>\App\Training::inRandomOrder()->first()->id
            ];
            ($data['public'])?$data['publisher_id']=$publisher[array_rand($publisher,1)]:null;
            \App\UserTraining::insert($data);
        }
        shuffle($all_ids);
        foreach(\App\UserTraining::all() as $training) {
            $data=[
                'user_id'=>$training->creator_id,
                'table' => 'Training',
                'order'=> implode(',',$all_ids)
            ];
            \App\TrainingOrder::insert($data);
               
        }
    }
    public function insertData()
    {
        $faker=\Faker\Factory::create();
        $training= new \App\Training;
        for($x=0;$x<5;$x++)
        {
            $data=[
                'name' => $faker->name,
                'picture' => $training->getDefaultPicture(),
                'creator_id' => User::inRandomOrder()->first()->id,
                'icon' => $training->getDefaultIcon()
            ];
            DB::table('trainings')->insert($data);
        }
    }
    public function checkFiles()
    {
        $filesFromDb=Storage::disk()->files('training_icon/');
        $dbCount=\App\Training::count('id');
        $filesCount=count($filesFromDb);
        // dd($filesFromDb);
        if($dbCount-$filesCount>0)
            $this->deleteFiles($filesFromDb);
        $filesFromDb=Storage::disk()->files('training_picture/');
        $filesCount=count($filesFromDb);
        if($dbCount-$filesCount>0)
            $this->deleteFiles($filesFromDb);
    }
    public function deleteFiles($files)
    {
        foreach($files as $file)
        {
            try{
                // dd(Storage::disk()->file())
                Storage::disk()->delete($file);
            }catch (FileNotFoundException $r) {

            }
            
        }
    }
}
