<?php

use App\User;
use App\Organization;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Classes\Storage;

class UserTableSeeder extends Seeder
{
    private $mainOrganizations = [
        'ITC'
    ];

    private $mainTeams = [
        [
            'name'          => 'Tys Team',
            'organization'  => 'ITC'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertOrganizations();
        $this->insertTeams();

        $new_users = [];
        if(\App\User::count()==0)
        {
            $this->deletePictures();
        }
        // if(\App\User::count()==0)
        // {
            foreach($this->items() as &$item)
            {
                // Check organizations
                if(array_key_exists('organization', $item))
                {
                    $org = Organization::where('name', $item['organization'])->first();
                    if($org)
                        $item['organization_id'] = $org->id;
                    else $item['organization_id'] = Organization::insertGetId([ 'name' => $item['organization'] ]);

                    unset($item['organization']);
                }

                // Check teams
                if(array_key_exists('team', $item))
                {
                    $team = DB::table('teams')->where('name', 'like', $item['team'])->first();
                    if(!$team) {
                        $team = DB::table('teams')->insertGetId([ 
                            'name'              => $item['team'],
                            'organization_id'   => $item['organization_id'] 
                        ]);
                    }else $team = $team->id;

                    $item['team_id'] = $team;

                    unset($item['team']);
                }

                // check emails
                $exists = DB::table('users')->where('email', '=', $item['email'])->first();
                if($exists)
                    $continue;

                $new_users[] = $item;
            }
            DB::table('users')->insert($new_users);
        // }
        factory(\App\User::class,4)->make();
        foreach(User::all() as $user)
        {
            try{
                if( Storage::disk()->exists( $user->getRandomDefaultPicture().'/users/'.$user->id) )
                    Storage::disk()->copy($user->getRandomDefaultPicture(), '/users/'.$user->id);
            }catch(FileExistsException $e){

            }
            
            $user->profile_picture='/media/users/'.$user->id;
            // dd($user);
            $user->update(); 
        }
        
    }

    protected function items()
    {
        $user=new \App\User();
        return [
                [
                'first_name'=>'Zdravko',
                'last_name' => 'Sokcevic',
                'email'     => 'zdravko.sokcevic@itcentar.rs',
                'identifier'=> null,
                'role'      => 'superadmin',
                'gender'    => 'm',
                'password'  => Hash::make('zdravko123'),
                "profile_picture"=>'/media/'.$user->getRandomDefaultPicture(),
                'parent_id' =>null,
                'organization'=>'ITC',
                'team' =>'Tys Team'
            ],[
                'first_name'=>'Pavle',
                'last_name' => 'Vukovic',
                'email'     => 'pavle.vukovic@itcentar.rs',
                'identifier'=> null,
                'role'      => 'superadmin',
                'gender'    => 'm',
                "profile_picture"=>'/media/'.$user->getRandomDefaultPicture(),
                'password'  => Hash::make('paja123'),
                'parent_id' =>null,
                'organization'=>'ITC',
                'team' => 'IT Centar team'
            
            ],[
                // [
                'first_name'=>'Super',
                'last_name' => 'Admin 2',
                'email'     => 'superadmin2@mail.com',
                'identifier'=> null,
                'role'      => 'superadmin',
                'gender'    => 'm',
                "profile_picture"=>'/media/'.$user->getRandomDefaultPicture(),
                'password'  => Hash::make('user1234'),
                'parent_id' =>null,
                'organization_id'=>\App\Organization::inRandomOrder()->first()->id,
                'team_id' => \App\Team::inRandomOrder()->first()->id
            ],[
                'first_name'=>'Super',
                'last_name' => 'Admin',
                'email'     => 'superadmin@mail.com',
                'identifier'=> null,
                'role'      => 'superadmin',
                'gender'    => 'm',
                "profile_picture"=>'/media/'.$user->getRandomDefaultPicture(),
                'password'  => Hash::make('user1234'),
                'parent_id' =>null,
                'organization_id'=>\App\Organization::inRandomOrder()->first()->id,
                'team_id' => \App\Team::inRandomOrder()->first()->id
            
            ],[
                'first_name'=>'Regular',
                'last_name' => 'Admin',
                'email'     => 'admin@mail.com',
                'identifier'=> null,
                'role'      => 'admin',
                'gender'    => 'm',
                "profile_picture"=>'/media/'.$user->getRandomDefaultPicture(),
                'password'  => Hash::make('user1234'),
                'parent_id' =>3,
                'organization_id'=> \App\Organization::inRandomOrder()->first()->id,
                'team_id' => \App\Team::inRandomOrder()->first()->id
            
            ],[
                'first_name'=>'Trainer',
                'last_name' => 'Coach',
                'email'     => 'trainer@mail.com',
                'identifier'=> null,
                'role'      => 'trainer',
                'gender'    => 'm',
                "profile_picture"=>'/media/'.$user->getRandomDefaultPicture(),
                'password'  => Hash::make('user1234'),
                'parent_id' =>4,
                'organization_id'=>\App\Organization::inRandomOrder()->first()->id,
                'team_id' => \App\Team::inRandomOrder()->first()->id
            
            ],[
                'first_name'=>'Parent',
                'last_name' => 'Player',
                'email'     => 'parent@mail.com',
                'identifier'=> null,
                'role'      => 'user',
                'gender'    => 'm',
                "profile_picture"=>'/media/'.$user->getRandomDefaultPicture(),
                'password'  => Hash::make('user1234'),
                'parent_id' =>5,
                'organization_id'=>\App\Organization::inRandomOrder()->first()->id,
                'team_id' => \App\Team::inRandomOrder()->first()->id
            
            ]
        ];
    }
    public function deletePictures()
    {
        $files=Storage::disk()->files('/users/');
        foreach($files as $file)
        {
            if(Storage::disk()->exists($file))
                Storage::disk()->delete($file);
        }
    }
    public function insertTeams()
    {
        $new_teams = [];
        foreach($this->mainTeams as $team)
        {
            $organization = DB::table('organizations')->where('name', $team['organization'])->first();
            if(!$organization)
                $organization = DB::table('organizations')->insertGetId( ['name' => $team['organization']] );

            $team['organization_id'] = $organization->id;
            unset($team['organization']);
        }
        for($x=0;$x<10;$x++)
        {
            $faker= \Faker\Factory::create();
            $new_teams[] = [
                'name'=>$faker->company,
                'organization_id'=> \App\Organization::inRandomOrder()->first()->id
            ];
        }

        DB::table('teams')->insert($new_teams);
    }

    public function insertOrganizations()
    {
        $organizations = [];
        // Check for main organization
        foreach($this->mainOrganizations as $organization) {
            // check if exists
            $instance = Organization::where('name', '=', $organization)->first();
            if(!$instance) 
                $organizations[] = [ 'name' => $organization ];
        }
        for($x=0;$x<6;$x++)
        {
            $faker= \Faker\Factory::create();
            $organizations[] = [ 'name' => $faker->company ];
        }
        DB::table('organizations')->insert($organizations);
    }

}
