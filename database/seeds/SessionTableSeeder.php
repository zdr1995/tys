<?php

use Illuminate\Database\Seeder;

class SessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory(\App\Session::class,10)->create();
        for($x=0;$x<7;$x++)
        {
            $sessionNameArray=[
                'Session1 x60',
                'Session2 x120',
                'Session1 x30',
                'Session4 x45'
            ];
            $data= [
                'name' => $sessionNameArray[array_rand($sessionNameArray,1)]
            ];
            \App\Session::create($data);
        }
        foreach(\App\Session::all() as $session) {
            $user=\App\User::inRandomOrder()->first();
            $data=[
              'user_id'=> $user->id,
              'session_id'=> $session->id,
              'training_id'=> \App\Training::inRandomOrder()->first()->id,
              'active'=> array_rand([0,1],1),
              'public'=> array_rand([0,1],1)
            ];
            \App\TrainingSession::create($data);
        }
    }
}
