<?php

use Illuminate\Database\Seeder;

class HelpTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin=\App\User::where('role','superadmin')->first();
        $data=[
            'creator_id'=>$superadmin->id,
            'html'=> '<p>This is default help page</p>'
        ];
        \App\Help::insert($data);
    }
}
