<?php

use Illuminate\Database\Seeder;

class PracticeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    //    factory(\App\Practice::class,15)->create();
        $faker=\Faker\Factory::create();
       $practice_array = [
        'Around Head',
        'Hand Slap',
        'Slow',
        'Walking',
        'Fast',
        '1 Leg',
        'Movement'
        ];
        $data= [
            'name' => $practice_array[array_rand($practice_array,1)],
            'picture' => 'null',
            'description'=>$faker->sentence,
            'video'=> 'null'
        ];
        $inserted_id=\App\Practice::insertGetId($data);
        $instance=\App\Practice::find($inserted_id);
        $instance->picture=$instance->getDefaultPicture();
        $instance->video=$instance->getDefaultVideo();
        $instance->save();
    }
}
