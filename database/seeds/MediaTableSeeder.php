<?php

use Illuminate\Database\Seeder;

class MediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $id=DB::select(DB::raw("SELECT max(id) as id FROM media"))[0]->id;
        // if($id==null)
        //     $id=0;
        // factory(\App\Media::class,400)->create();
        $picture=1;
        for($x=0;$x<10;$x++)
        {
            $faker=\Faker\Factory::create();
            $id=DB::select(DB::raw("SELECT max(id) as id FROM media"))[0]->id;
            if($id==null)
                $id=0;
            $url='/media/media_gallery/';
            $data=[
                'url'=>$url.$id++,
                'type'=>$picture,
                'name'=>$faker->name,
                'owner_id'=>\App\User::inRandomOrder()->first()->id,
                'public'=>1,
                'published_by'=>null,
                'path'=>$faker->imageUrl()
            ];
            DB::table('media')->insert($data);

        }

        $video=2;

        $url='/media/media_gallery/';
        for ($x=0;$x<10;$x++) {
            $faker2= \Faker\Factory::create();
            $id=DB::select(DB::raw("SELECT max(id) as id FROM media"))[0]->id;
            $data=[
                'url'=>$url.$id++,
                'type'=>$video,
                'name'=>$faker2->name,
                'owner_id'=>\App\User::inRandomOrder()->first()->id,
                'public'=>1,
                'published_by'=>null,
                'path'=>(new \App\Media)->getRandomVideo()
            ];
            DB::table('media')->insert($data);
        }
    }
}
