<?php

use Illuminate\Database\Seeder;

class TrainingRelationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\TrainingRelationModel::class,600)->create();
    }
}
