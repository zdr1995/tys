<?php

use App\Classes\Storage;
use Illuminate\Database\Seeder;

class SubsessionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //factory(\App\Subsession::class,20)->create();
        $subsessionNames=[
            'Flexibility',
            'Running',
            'Cardio',
            'Dribbling',
            'Pressing',
            'Shooting',
            'Rebounding',
            'Offense',
            'Team',
            'Durability'
        ];
        for($x=0;$x<10;$x++)
        {
            $faker=\Faker\Factory::create();
            $random_pdf=$this->randomPdfPath();
            $data= [
                'name' => $subsessionNames[array_rand($subsessionNames,1)],
                'picture' => 'null',
                'description'=> $faker->sentence,
                'drill_manual'=>'null',
                'court_diagram'=> ''
            ];
            $id=\App\Subsession::insertGetId($data);
            $instance=\App\Subsession::find($id);
            //for drill_manual
            $this->copyFile($instance->id);
            
            $instance->drill_manual='/media/drill_manual/'.$instance->id.'.pdf';
            $instance->picture=$instance->getDefaultPicture();
            
            $instance->save();
        }
    }
    public function randomPdfPath()
    {
        return \App\Media::get_pdf_path();
    }
    public function copyFile($id)
    {
        $all_files=Storage::disk()->files( $this->randomPdfPath() );
        // dd(rand(0,count($all_files)-1));
        if($all_files == null) 
            return;
        $random_file=$all_files[rand(0,count($all_files)-1)];
        if(!Storage::disk()->exists($random_file))
            Storage::disk()->copy($random_file,\App\Subsession::$drill_path.$id.'.pdf');
    }
}
