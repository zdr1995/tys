<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(TrainingTableSeeder::class);
        $this->call(SessionTableSeeder::class);
        $this->call(SubsessionTableSeeder::class);
        $this->call(PracticeTableSeeder::class);
        $this->call(MediaTableSeeder::class);
        $this->call(HelpTableSeeder::class);
        $this->call(AboutTableSeeder::class);
        // $this->call(TrainingRelationTableSeeder::class);
    }
}
