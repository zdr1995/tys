<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $user=new \App\User;
    $roles=[
        'admin',
        'user',
        'trainer',
        'superadmin'
    ];
    $gender=['m','f'];
    $newUser=[];

    $faker_serbian=\Faker\Factory::create('sr_Latn_RS');

    $newUser['first_name']= $faker->firstName;
    $newUser['last_name'] = $faker->lastName;
    $newUser['role']      = $roles[array_rand($roles)];
    $newUser['gender']    = $gender[array_rand($gender)];

    if($newUser['role']=='user') {
        $newUser['parent_id']=\App\User::inRandomOrder()->orderByRaw("RAND()")->first()->id;
        $newUser['team_id']= \App\Team::inRandomOrder()->first()->id;
        $newUser['organization_id']=\App\Organization::inRandomOrder()->first()->id;
    }
        
    if($newUser['role']=='trainer') {
        $newUser['parent_id']=\App\User::where('role','=','admin')->orWhere('role','=','superadmin')->orderByRaw("RAND()")->first()->id;
        $newUser['organization_id']= \App\Organization::inRandomOrder()->first()->id;
        $newUser['team_id']= \App\Team::inRandomOrder()->first()->id;
    }
        
    if($newUser['role']=='admin') {
        $newUser['parent_id']=\App\User::where('role','=','superadmin')->orderByRaw("RAND()")->first()->id;
        $newUser['organization_id']= \App\Organization::inRandomOrder()->first()->id;
        $newUser['team_id']= \App\Team::inRandomOrder()->first()->id;
    }

    if($newUser['role']=='superadmin') {
        $newUser['parent_id']= null;
        $newUser['organization_id']= \App\Organization::inRandomOrder()->first()->id;
        $newUser['team_id']= \App\Team::inRandomOrder()->first()->id;
    }
        

    $newUser['identifier']=null;
    $newUser['email']     = $faker->unique()->safeEmail;
    $newUser['profile_picture']='/media/'.$user->getRandomDefaultPicture();
    $newUser['password']  = '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm';
    $newUser['remember_token'] = str_random(10);
    //$newUser['team']= $faker->name;

    return $newUser;
});
