<?php

use App\Practice;
use Faker\Generator as Faker;

$factory->define(\App\Practice::class, function (Faker $faker) {
    $practice_array = [
        'Around Head',
        'Hand Slap',
        'Slow',
        'Walking',
        'Fast',
        '1 Leg',
        'Movement'
    ];
    return [
        'name' => $practice_array[array_rand($practice_array,1)],
        'picture' => 'null',
        'video'=> 'null'
    ];
});
