<?php

use Faker\Generator as Faker;

$factory->define(\App\Training::class, function (Faker $faker) {
    $training=new \App\Training;
    $id=\App\Training::max('id');
    
    ($id==null)?$id=1:$id=++$id;
    // var_dump($id);
    // dd($id);
    // dd($training->getDefaultPicture($id));
    return [
        'name' => $faker->name,
        'picture' => $training->getDefaultPicture($id),
        'icon' => $training->getDefaultIcon($id)
    ];
});
