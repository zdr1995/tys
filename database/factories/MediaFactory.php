<?php

use Faker\Generator as Faker;

$factory->define(\App\Media::class, function (Faker $faker) {
    $picture=1;
    $video=2;
    $pdf=3;
    
    $id=DB::select(DB::raw("SELECT max(id) as id FROM media"))[0]->id;
    var_dump($id);
    if($id==null)
        $id=0;
    $url=env('MY_ADDRESS').'/media/media_gallery/';
    return [
        'url'=>$url.$id++,
        'type'=>$picture,
        'name'=>$faker->name,
        'owner_id'=>\App\User::inRandomOrder()->first()->id,
        'public'=>1,
        'published_by'=>null,
        'path'=>$faker->imageUrl(460,200)
    ];
});
