<?php

use Faker\Generator as Faker;

$factory->define(\App\TrainingRelationModel::class, function (Faker $faker) {
    return [
        'user_id'   => \App\User::where('role','=','admin')->inRandomOrder()->first()->id,
        'training_id' => \App\Training::inRandomOrder()->first()->id,
        'session_id'  => \App\Session::inRandomOrder()->first()->id,
        'subsession_id' => \App\Subsession::inRandomOrder()->first()->id,
        'practice_id' => \App\Practice::inRandomOrder()->first()->id,
        'training_picture' => $faker->ImageUrl(400,400),
        'session_picture' => $faker->imageUrl(400,400),
        'subsession_picture' => $faker->imageUrl(400,200),
        'practice_picture'=> $faker->imageUrl(400,200),
        'practice_video' => 'null'
    ];
});
