<?php

use Faker\Generator as Faker;

$factory->define(\App\Session::class, function (Faker $faker) {
    $sessionNameArray=[
        'Session1 x60',
        'Session2 x120',
        'Session1 x30',
        'Session4 x45'
    ];
    return [
        'name' => $sessionNameArray[array_rand($sessionNameArray,1)],
        'default_picture' => (new \App\Session)->getDefaultPicture()
    ];
});
