<?php

use Faker\Generator as Faker;


$factory->define(\App\Subsession::class, function (Faker $faker) {
    $subsessionNames=[
        'Flexibility',
        'Running',
        'Cardio',
        'Dribbling',
        'Pressing',
        'Shooting',
        'Rebounding',
        'Offense',
        'Team',
        'Durability'
    ];
    return [
        'name' => $subsessionNames[array_rand($subsessionNames,1)],
        'picture' => 'null'
    ];
});

