<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Test pusher app</h1>
    
    <script src="https://js.pusher.com/5.0/pusher.min.js"></script>
    <script src="{{ asset('js/test.js') }}"></script>
</body>
</html>