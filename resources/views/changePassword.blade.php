<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <table align="center">
        <tr>
            <td>
                <table 
                    style="
                        width:600px; 
                        border-collapse: collapse; 
                        margin: 0; padding: 0; 
                        border: none;
                        background-image: url('{{env('API_URL')}}/bg.jpg');
                        height: 400px;
                        background-repeat: repeat-x;
                        background-size: 600px;"
                >
                        <tr style="">
                            <td>
                                <img 
                                src="{{env('API_URL')}}/logo.png" 
                                style="
                                  display: block;
                                  margin-left: auto;
                                  margin-right: auto;
                                  width: 40%;"
                                />
                            </td>
                        </tr>
                </table>
            <table 
                style="
                    width:600px; 
                    border-collapse: collapse; 
                    margin: 0; padding: 0; 
                    border: none;
                    height: 400px;
                    background-color: #1D0807;
                    color:white;" 
            >
                    <tr align="center">
                        <td align="center" style="text-align: left;">
                            <table align="center" width="500" style="width:500px;color:white;">
                                <tr>
                                    <td width="100%">
                                        Your verification code is <font style="color:#f2711c">{{$code}}</font>.
                                        Use it to reset your password. <br /> <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" width="50%">
                                        TrueCoach Team
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
            </table>
            </td>
        </tr>
    </table>

</body>
</html>