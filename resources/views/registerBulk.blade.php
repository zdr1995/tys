<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <p>Hello!</p>
    <p>Your mail, {{$user->email}}, has been registered to True Coach App.</p>
    <p>Default password is: <code>{{env('DEFAULT_PASS')}}</code>. Please change it as soon as possible.</p>
    <p>
        <a href="{{env('APP_LINK')}}">CLICK HERE</a> to use the app.
    </p>
    {{-- <p>{{$user->email}},Welcome to Tys app,your default password is:{{env('DEFAULT_PASS')}}, Please change it as soon as possible.</p> --}}
</body>
</html>