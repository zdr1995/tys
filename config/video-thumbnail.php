<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Binaries
    |--------------------------------------------------------------------------
    |
    | Paths to ffmpeg nad ffprobe binaries
    |
    */
    
    'binaries' => [
        'ffmpeg.binaries' => env('FFMPEG_BINARIES'),
        'ffmpeg.threads'  => 12,
        'ffprobe.binaries' => env('FFPROBE_BINARIES'),
        'timeout' => 3600,
    ]
];