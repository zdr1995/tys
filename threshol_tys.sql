-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 22, 2019 at 05:32 PM
-- Server version: 8.0.18-0ubuntu0.19.10.1
-- PHP Version: 7.3.11-0ubuntu0.19.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `threshol_tys`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `html`, `creator_id`, `created_at`, `updated_at`) VALUES
(1, '<p>This is default help page</p>', 1, NULL, NULL),
(2, '<p>asds</p>', 347, '2019-11-15 08:48:35', '2019-11-15 08:48:35'),
(3, '<p>iojij</p>', 349, '2019-11-15 11:19:39', '2019-11-15 11:19:39'),
(4, '<p>This is about page.</p>', 2, '2019-11-15 12:52:19', '2019-11-15 12:52:19');

-- --------------------------------------------------------

--
-- Table structure for table `helps`
--

CREATE TABLE `helps` (
  `id` int(10) UNSIGNED NOT NULL,
  `html` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `helps`
--

INSERT INTO `helps` (`id`, `html`, `creator_id`, `created_at`, `updated_at`) VALUES
(1, '<p>This is default help page</p>', 1, NULL, NULL),
(2, '<p>This is help page.</p>', 2, '2019-11-07 08:57:12', '2019-11-07 08:57:12');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('1','2','3') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` int(10) UNSIGNED NOT NULL,
  `public` tinyint(4) DEFAULT NULL,
  `published_by` int(10) UNSIGNED DEFAULT NULL,
  `path` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video_thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `name`, `url`, `type`, `owner_id`, `public`, `published_by`, `path`, `created_at`, `updated_at`, `video_thumbnail`) VALUES
(160, 'SampleVideo_176x144_1mb.3gp', '/storage/media_gallery/videos/SampleVideo_176x144_1mb.3gp', '2', 3, 0, NULL, '/storage/media_gallery/videos/SampleVideo_176x144_1mb.3gp', '2019-10-20 15:32:51', '2019-10-20 15:32:51', NULL),
(161, 'SampleVideo_720x480_1mb.mp4', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', '2', 3, 0, NULL, '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', '2019-10-20 15:33:29', '2019-10-20 15:33:29', NULL),
(162, 'external-content.duckduckgo', '/storage/media_gallery/pictures/external-content.duckduckgo.com', '1', 2, 0, NULL, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '2019-11-01 07:38:37', '2019-11-01 09:59:19', NULL),
(163, 'zdravkoadadasd', '/storage/media_gallery/pictures/as', '1', 2, 0, NULL, '/storage/media_gallery/pictures/as', '2019-11-01 09:53:39', '2019-11-01 10:14:05', NULL),
(165, '1', '/storage/media_gallery/pictures/1', '1', 2, 0, NULL, '/storage/media_gallery/pictures/1', '2019-11-04 18:06:55', '2019-11-04 18:06:55', NULL),
(166, 'Dzeki', '/storage/media_gallery/pictures/Dzeki', '1', 2, 0, NULL, '/storage/media_gallery/pictures/Dzeki', '2019-11-04 18:07:02', '2019-11-04 18:07:03', NULL),
(167, 'Njuska', '/storage/media_gallery/pictures/Njuska', '1', 2, 0, NULL, '/storage/media_gallery/pictures/Njuska', '2019-11-04 18:07:13', '2019-11-04 18:07:13', NULL),
(168, 'Ker', '/storage/media_gallery/pictures/Ker', '1', 2, 0, NULL, '/storage/media_gallery/pictures/Ker', '2019-11-04 18:07:21', '2019-11-04 18:07:21', NULL),
(169, 'Pump Fake', '/storage/media_gallery/videos/Pump Fake', '2', 2, 0, NULL, '/storage/media_gallery/videos/Pump Fake', '2019-11-04 18:09:50', '2019-11-04 18:09:50', NULL),
(170, 'Pump Fake', '/storage/media_gallery/videos/Pump Fake', '2', 2, 0, NULL, '/storage/media_gallery/videos/Pump Fake', '2019-11-05 14:46:39', '2019-11-05 14:46:39', NULL),
(171, 'Figure 8 - Standing', '/storage/media_gallery/videos/Figure 8 - Standing', '2', 2, 0, NULL, '/storage/media_gallery/videos/Figure 8 - Standing', '2019-11-05 14:47:24', '2019-11-05 14:47:24', NULL),
(172, 'Pump Fake', '/storage/media_gallery/videos/Pump Fake', '2', 2, 0, NULL, '/storage/media_gallery/videos/Pump Fake', '2019-11-05 14:47:47', '2019-11-05 14:47:47', NULL),
(173, 'Figure 8 - Walking', '/storage/media_gallery/videos/Figure 8 - Walking', '2', 2, 0, NULL, '/storage/media_gallery/videos/Figure 8 - Walking', '2019-11-05 14:48:28', '2019-11-05 14:48:28', NULL),
(174, 'Triple Threat - Sweep Through, Reverse Layup', '/storage/media_gallery/videos/Triple Threat - Sweep Through, Reverse Layup', '2', 2, 0, NULL, '/storage/media_gallery/videos/Triple Threat - Sweep Through, Reverse Layup', '2019-11-05 14:49:21', '2019-11-05 14:49:21', NULL),
(175, 'Figure 8 - Walking', '/storage/media_gallery/videos/Figure 8 - Walking', '2', 2, 0, NULL, '/storage/media_gallery/videos/Figure 8 - Walking', '2019-11-05 14:51:42', '2019-11-05 14:51:42', NULL),
(176, '2 Ball Toss & Catch', '/storage/media_gallery/videos/2 Ball Toss & Catch', '2', 2, 0, NULL, '/storage/media_gallery/videos/2 Ball Toss & Catch', '2019-11-05 14:53:39', '2019-11-05 14:53:39', NULL),
(179, '1 Ball Toss', NULL, '2', 2, NULL, NULL, NULL, '2019-11-07 15:46:17', '2019-11-07 15:46:17', NULL),
(180, '2-3 shell Def 22', NULL, '2', 2, NULL, NULL, NULL, '2019-11-07 15:47:15', '2019-11-07 15:47:15', NULL),
(181, '1-2-2 press', '/storage/media_gallery/videos/1-2-2 press', '2', 2, 0, NULL, '/storage/media_gallery/videos/1-2-2 press', '2019-11-07 15:47:55', '2019-11-07 15:47:56', NULL),
(182, 'Ball screen switch', '/storage/media_gallery/videos/Ball screen switch', '2', 2, 0, NULL, '/storage/media_gallery/videos/Ball screen switch', '2019-11-07 15:48:37', '2019-11-07 15:48:37', NULL),
(183, 'Bounce Pass', '/storage/media_gallery/videos/Bounce Pass', '2', 2, 0, NULL, '/storage/media_gallery/videos/Bounce Pass', '2019-11-07 15:49:31', '2019-11-07 15:49:32', NULL),
(184, '1 Ball Toss - Catch Behind', NULL, '2', 2, NULL, NULL, NULL, '2019-11-07 16:35:18', '2019-11-07 16:35:18', NULL),
(185, '1 Ball Toss - Catch Behind', NULL, '2', 2, NULL, NULL, NULL, '2019-11-07 16:36:50', '2019-11-07 16:36:50', NULL),
(186, '1 Ball Toss - Catch Behind', NULL, '2', 2, NULL, NULL, NULL, '2019-11-07 16:37:26', '2019-11-07 16:37:26', NULL),
(187, '1 Ball Toss - Catch Behind', NULL, '2', 2, NULL, NULL, NULL, '2019-11-07 16:37:59', '2019-11-07 16:37:59', NULL),
(188, '1 Ball Toss - Catch Behind', '/storage/media_gallery/videos/1 Ball Toss - Catch Behind', '2', 2, 0, NULL, '/storage/media_gallery/videos/1 Ball Toss - Catch Behind', '2019-11-07 16:38:31', '2019-11-07 16:38:32', '/storage/media_gallery/videos/thumb/188.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `media_orders`
--

CREATE TABLE `media_orders` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `order` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2012_10_07_101850_create_teams_table', 1),
(2, '2012_10_07_101900_create_organizations_table', 1),
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_08_29_084725_create_trainings_table', 1),
(6, '2019_08_29_084736_create_sessions_table', 1),
(7, '2019_08_29_084744_create_practices_table', 1),
(8, '2019_08_29_101555_create_subsessions_table', 1),
(9, '2019_08_29_101756_create_training_relation_models_table', 1),
(10, '2019_09_19_100957_create_training_sessions_table', 1),
(11, '2019_09_19_101021_create_session_subsessions_table', 1),
(12, '2019_09_19_101030_create_subsession_practices_table', 1),
(13, '2019_09_20_074331_create_media_table', 1),
(14, '2019_09_27_113153_create_training_orders_table', 1),
(15, '2019_10_01_104553_create_session_subsession_practices_table', 1),
(16, '2019_10_01_104613_create_training_session_subsessions_table', 1),
(17, '2019_10_01_104624_create_training_session_subsession_practices_table', 1),
(18, '2019_10_04_111948_create_helps_table', 1),
(19, '2019_10_04_112004_create_abouts_table', 1),
(20, '2019_10_07_145517_create_training_session_orders_table', 1),
(21, '2019_10_07_145532_create_training_session_subsession_orders_table', 1),
(22, '2019_10_07_145541_create_training_session_subsession_practice_orders_table', 1),
(23, '2019_10_07_145558_create_session_subsession_practice_orders_table', 1),
(24, '2019_10_10_121935_create_user_user_trainings_table', 1),
(25, '2019_09_27_081802_create_user_trainings_table', 2),
(26, '2019_10_15_134054_training_session_order', 3),
(27, '2019_10_15_134857_create_training_session_subsession_order_table', 3),
(28, '2019_10_15_134909_create_training_session_subsession_practice_order_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Beatty, Harvey and Smitham', NULL, NULL),
(2, 'Konopelski-Simonis', NULL, NULL),
(19, 'Djole Org', NULL, NULL),
(20, 'Org private', NULL, NULL),
(22, 'TIME', NULL, NULL),
(26, 'Org newwww', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `org_publish`
--

CREATE TABLE `org_publish` (
  `id` int(11) UNSIGNED NOT NULL,
  `organization_id` varchar(255) DEFAULT NULL,
  `training_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `org_publish`
--

INSERT INTO `org_publish` (`id`, `organization_id`, `training_id`) VALUES
(1, '1', 2),
(2, '19', 2),
(3, '22', 118),
(19, '22', 113),
(20, '20', 113),
(21, '19', 113),
(22, '2', 113),
(23, '1', 113);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pera`
--

CREATE TABLE `pera` (
  `id` int(11) NOT NULL,
  `organization_id` varchar(255) NOT NULL,
  `team_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `practices`
--

CREATE TABLE `practices` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `practices`
--

INSERT INTO `practices` (`id`, `name`, `creator_id`, `picture`, `video`, `description`, `created_at`, `updated_at`, `video_name`) VALUES
(15, 'Practice 1', 2, NULL, NULL, NULL, '2019-10-29 13:09:03', '2019-10-29 13:09:03', NULL),
(16, 'Practice 2', 2, NULL, NULL, NULL, '2019-10-29 13:09:42', '2019-10-29 13:09:42', NULL),
(17, 'Pera', 2, NULL, '/tmp/php6fJ9Gs', NULL, '2019-10-30 12:39:41', '2019-10-30 12:39:41', NULL),
(18, 'Pera 10', 2, NULL, '/tmp/phpRtQiMu', NULL, '2019-10-30 12:40:09', '2019-10-30 12:40:09', NULL),
(19, 'Practyice 66', 2, NULL, '/tmp/phpudV0rt', NULL, '2019-10-30 12:40:53', '2019-10-30 12:40:53', NULL),
(20, 'Kuce', 2, '/storage/practice_picture/20', NULL, 'pas', '2019-11-01 07:28:48', '2019-11-01 07:28:48', NULL),
(21, 'Practice 5', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', NULL, '2019-11-01 07:49:16', '2019-11-01 07:49:17', NULL),
(22, 'Doggo', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/storage/media_gallery/videos/SampleVideo_176x144_1mb.3gp', 'Pasce', '2019-11-01 07:51:20', '2019-11-01 07:51:20', NULL),
(23, 'Practice 5', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', NULL, '2019-11-01 07:53:16', '2019-11-01 07:53:16', NULL),
(24, 'Pera 1', 2, NULL, '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', NULL, '2019-11-01 07:53:29', '2019-11-01 07:53:29', NULL),
(25, 'dgdsv', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', NULL, '2019-11-01 07:54:26', '2019-11-01 07:54:26', NULL),
(26, 'asdas', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/tmp/phpEIg27n', 'dasdad', '2019-11-01 07:56:44', '2019-11-01 07:56:44', NULL),
(27, 'asdas', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/storage/practice_video/27.mp4', 'dasdad', '2019-11-01 07:57:35', '2019-11-01 07:57:36', NULL),
(28, 'KUCOV', 2, '/storage/practice_picture/28', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', 'asd', '2019-11-01 07:59:08', '2019-11-01 09:53:24', '2 Ball Pound Dribble - Ankles.MP4'),
(29, 'Smiljan', 2, '/storage/media_gallery/pictures/as', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', NULL, '2019-11-01 10:23:02', '2019-11-01 10:23:02', NULL),
(30, 'Smiljana', 2, '/storage/media_gallery/pictures/as', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', NULL, '2019-11-01 10:23:35', '2019-11-01 10:23:35', NULL),
(31, 'Smiljanici', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', NULL, '2019-11-01 10:23:48', '2019-11-01 10:23:48', NULL),
(33, 'pera', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/tmp/php2Kswvw', NULL, '2019-11-01 10:34:09', '2019-11-01 10:34:09', NULL),
(34, 'pera', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/tmp/php0wHlds', NULL, '2019-11-01 10:34:37', '2019-11-01 10:34:37', NULL),
(35, 'dfaf', 2, '/storage/media_gallery/pictures/external-content.duckduckgo.com', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', NULL, '2019-11-01 10:34:50', '2019-11-01 10:34:50', NULL),
(36, 'Practice', 2, '/storage/media_gallery/pictures/Ker', '/tmp/php6KQL7J', NULL, '2019-11-04 18:08:00', '2019-11-04 18:08:41', NULL),
(37, 'Nekki novi practice', 2, NULL, '/storage/media_gallery/videos/2 Ball Toss & Catch', 'Desc Here', '2019-11-07 14:49:44', '2019-11-07 14:49:44', NULL),
(38, 'Practice 6', 2, NULL, '/storage/media_gallery/videos/Figure 8 - Walking', NULL, '2019-11-07 14:51:37', '2019-11-07 14:51:37', NULL),
(39, 'afsdg', 2, NULL, '/storage/media_gallery/videos/Figure 8 - Walking', NULL, '2019-11-07 14:52:37', '2019-11-07 14:52:37', NULL),
(40, 'ddvfsdnov', 2, NULL, '/storage/practice_video/40.mp4', NULL, '2019-11-07 14:53:32', '2019-11-07 14:53:32', '3-2 Shell Def 1-.mp4'),
(41, 'afsdgvsf', 2, NULL, '/storage/practice_video/41.mp4', NULL, '2019-11-07 14:55:12', '2019-11-07 14:55:12', 'Behind the Back x5, Jumper.MP4'),
(50, 'Practice nije', 2, '/storage/media_gallery/pictures/1', '/storage/practice_video/50.mp4', NULL, '2019-11-07 15:05:11', '2019-11-21 14:39:52', 'Reaction - Through the Legs.MP4'),
(51, 'afsdfsdfd', 2, NULL, '/storage/practice_video/51.mp4', NULL, '2019-11-07 15:06:01', '2019-11-07 15:06:01', 'BLOB Line set.mp4'),
(52, 'Practice 22', 2, NULL, '/storage/practice_video/52.mp4', NULL, '2019-11-07 15:08:18', '2019-11-07 15:08:19', 'Reaction - Crossover.MP4'),
(53, 'SDASDFASD', 2, NULL, '/storage/practice_video/53.mp4', NULL, '2019-11-07 15:08:34', '2019-11-07 15:08:35', 'BLOB box set upscreens.mp4'),
(54, 'afsdgs', 2, NULL, NULL, NULL, '2019-11-07 15:09:24', '2019-11-07 15:09:24', NULL),
(55, 'afsdgs', 2, NULL, NULL, NULL, '2019-11-07 15:11:13', '2019-11-07 15:11:13', NULL),
(56, 'afsdgs', 2, NULL, NULL, NULL, '2019-11-07 15:11:28', '2019-11-07 15:11:28', NULL),
(57, 'afsdgs', 2, NULL, NULL, NULL, '2019-11-07 15:11:49', '2019-11-07 15:11:49', NULL),
(58, 'afsdgs', 2, NULL, NULL, NULL, '2019-11-07 15:12:02', '2019-11-07 15:12:02', NULL),
(59, 'afsdgs', 2, NULL, NULL, NULL, '2019-11-07 15:12:10', '2019-11-07 15:12:10', NULL),
(60, 'afsdgs', 2, NULL, NULL, NULL, '2019-11-07 15:12:24', '2019-11-07 15:12:24', NULL),
(61, 'afsdgs', 2, NULL, '/storage/media_gallery/videos/2 Ball Toss & Catch', NULL, '2019-11-07 15:12:45', '2019-11-07 15:12:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `name`, `creator_id`, `created_at`, `updated_at`) VALUES
(8, 'TEST', 2, '2019-11-04 06:35:47', '2019-11-04 06:35:47'),
(9, 'Jelena', 2, '2019-11-07 09:50:02', '2019-11-07 09:50:02'),
(10, 'Rozga', 2, '2019-11-07 09:50:06', '2019-11-07 09:50:06'),
(11, 'Zena', 2, '2019-11-07 09:50:12', '2019-11-07 09:50:12'),
(12, 'Zmaj', 2, '2019-11-07 09:50:16', '2019-11-07 09:50:16'),
(13, 'Kucov', 2, '2019-11-07 09:50:21', '2019-11-07 09:50:21'),
(14, 'Macka', 2, '2019-11-07 09:50:26', '2019-11-07 09:50:26'),
(15, 'Cosa', 2, '2019-11-07 09:50:31', '2019-11-07 09:50:31'),
(16, 'Alexa', 2, '2019-11-07 09:50:37', '2019-11-07 09:50:37'),
(17, 'Nemanja', 2, '2019-11-07 09:50:44', '2019-11-07 09:50:44');

-- --------------------------------------------------------

--
-- Table structure for table `session_subsessions`
--

CREATE TABLE `session_subsessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `subsession_id` int(10) UNSIGNED NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `publisher_id` int(10) UNSIGNED DEFAULT NULL,
  `published` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `session_subsession_order`
--

CREATE TABLE `session_subsession_order` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `session_id` int(11) NOT NULL,
  `order` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `session_subsession_practices`
--

CREATE TABLE `session_subsession_practices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `subsession_id` int(10) UNSIGNED NOT NULL,
  `practices_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `practice_video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `practice_picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `publisher_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(4) NOT NULL,
  `public` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subsessions`
--

CREATE TABLE `subsessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `drill_manual` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'null',
  `drill_manual_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `court_diagram` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT 'null',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subsessions`
--

INSERT INTO `subsessions` (`id`, `name`, `creator_id`, `picture`, `description`, `drill_manual`, `drill_manual_name`, `court_diagram`, `created_at`, `updated_at`) VALUES
(40, 'sdfdsfdsf', 2, NULL, NULL, '/storage/media_gallery/pdf/pdfSample', 'pdfSample', 'null', '2019-11-01 07:30:04', '2019-11-05 15:14:37'),
(41, 'SBS', 2, '/storage/media_gallery/pictures/Ker', NULL, '/storage/media_gallery/pdf/pdfSample', 'pdfSample', 'null', '2019-11-04 18:11:15', '2019-11-05 15:13:51'),
(42, 'Sbsb 2sdfsd', 2, '/storage/media_gallery/pictures/Ker', NULL, '/storage/media_gallery/pdf/c4611_sample_explain', 'c4611_sample_explain', 'null', '2019-11-05 11:13:00', '2019-11-05 15:13:52');

-- --------------------------------------------------------

--
-- Table structure for table `subsession_practices`
--

CREATE TABLE `subsession_practices` (
  `subsession_id` int(10) UNSIGNED NOT NULL,
  `practice_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `practice_video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `practice_picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `organization_id`, `created_at`, `updated_at`) VALUES
(10, 'Pollich, Bosco and Stroman', 1, NULL, NULL),
(200, 'Some Team', 1, NULL, NULL),
(206, 'nik', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `team_publish`
--

CREATE TABLE `team_publish` (
  `id` int(11) NOT NULL,
  `team_id` int(10) UNSIGNED DEFAULT NULL,
  `training_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `team_publish`
--

INSERT INTO `team_publish` (`id`, `team_id`, `training_id`) VALUES
(3, 206, 116);

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `public` tinyint(4) DEFAULT NULL,
  `publisher_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `name`, `picture`, `icon`, `active`, `public`, `publisher_id`, `creator_id`, `created_at`, `updated_at`) VALUES
(94, 'PAVLE', NULL, NULL, 1, 0, 2, 2, '2019-10-31 11:34:22', '2019-11-15 10:06:18'),
(97, 'TEST', '/storage/media_gallery/pictures/as', NULL, 1, 2, 2, 2, '2019-11-04 06:34:30', '2019-11-18 16:31:15'),
(100, 'IOSFIOSF', '/storage/training_picture/100', NULL, 0, 0, 2, 2, '2019-11-04 09:27:15', '2019-11-15 11:02:47'),
(102, 'Trening test', '/storage/training_picture/102', NULL, 0, 0, 2, 2, '2019-11-04 12:19:03', '2019-11-15 11:02:49'),
(109, 'Trening 555', '/storage/media_gallery/pictures/external-content.duckduckgo.com', NULL, 0, 0, 2, 46, '2019-11-04 12:36:17', '2019-11-15 11:02:44'),
(111, 'NJUSKA', '/storage/media_gallery/pictures/Njuska', NULL, 0, 0, 2, 2, '2019-11-05 11:03:11', '2019-11-15 09:41:19'),
(113, 'NJUSKA', '/storage/media_gallery/pictures/Dzeki', NULL, 0, 2, 2, 2, '2019-11-05 11:05:25', '2019-11-15 11:03:05'),
(114, 'Novi t', '/storage/media_gallery/pictures/Njuska', NULL, 0, 0, NULL, 2, '2019-11-05 11:51:40', '2019-11-05 11:51:40'),
(116, 'NJUSO', '/storage/media_gallery/pictures/Njuska', NULL, 1, 2, 2, 2, '2019-11-07 15:39:33', '2019-11-15 11:03:07'),
(117, '789', '/storage/media_gallery/pictures/Ker', NULL, 1, 2, 2, 344, '2019-11-14 16:33:18', '2019-11-15 11:23:56'),
(118, 'NOVI KER', '/storage/media_gallery/pictures/Njuska', NULL, 1, 2, 2, 2, '2019-11-15 08:07:50', '2019-11-19 08:21:42'),
(119, 'STH', NULL, NULL, 1, 0, NULL, 2, '2019-11-15 11:17:57', '2019-11-15 11:17:57');

-- --------------------------------------------------------

--
-- Table structure for table `training_orders`
--

CREATE TABLE `training_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `order` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `table` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_orders`
--

INSERT INTO `training_orders` (`id`, `user_id`, `order`, `table`, `created_at`, `updated_at`) VALUES
(5, 3, '108,94,97,100,89,99,101,92,90,102,113,114,115,116,117,118,119', 'Training', '2019-10-31 11:33:44', '2019-11-15 11:17:57'),
(6, 2, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', '2019-10-31 11:34:03', '2019-11-15 11:27:10'),
(7, 4, '97,90,94,100,89,101,99,113,114,115,116,117,118,119', 'Training', '2019-11-04 09:09:49', '2019-11-15 11:17:57'),
(8, 25, '94,97,100,89,101,113,114,115,116,117,118,119', 'Training', '2019-11-04 11:13:26', '2019-11-15 11:17:57'),
(9, 44, '101,94,97,100,89,113,114,115,116,117,118,119', 'Training', '2019-11-04 11:27:56', '2019-11-15 11:17:57'),
(10, 45, '94,97,100,89,101,113,114,115,116,117,118,119', 'Training', '2019-11-04 12:03:35', '2019-11-15 11:17:57'),
(11, 46, '101,94,92,109,97,100,89,99,108,90,113,114,115,116,117,118,119', 'Training', '2019-11-04 12:09:23', '2019-11-15 11:17:57'),
(12, 153, '94,97,100,102,115,116,117,118,119', 'Training', '2019-11-06 12:50:26', '2019-11-15 11:17:57'),
(35, 330, '97,94,100,102,117,118,119', 'Training', '2019-11-12 13:07:12', '2019-11-15 11:17:57'),
(36, 343, '94,97,100,102,117,118,119', 'Training', NULL, '2019-11-15 11:17:57'),
(37, 344, '116,117,118,94,97,100,102,109,119', 'Training', NULL, '2019-11-15 11:17:57'),
(38, 345, '94,97,100,102,109,111,113,114,116,117,118,119', 'Training', NULL, '2019-11-15 11:17:57'),
(39, 346, '94,97,100,102,109,111,113,114,116,117,118,119', 'Training', NULL, '2019-11-15 11:17:57'),
(40, 347, '116,118,113,119', 'Training', '2019-11-15 08:42:04', '2019-11-15 11:17:57'),
(41, 348, '94,97,100,102,109,111,113,114,116,117,118,119', 'Training', NULL, '2019-11-15 11:17:57'),
(42, 349, '97,100,102,109,116,118,119', 'Training', NULL, '2019-11-15 11:17:57'),
(43, 16, '116,118,97,94,100,109,102,119', 'Training', '2019-11-15 09:36:29', '2019-11-15 11:17:57'),
(44, 350, '113,116,118,119', 'Training', NULL, '2019-11-15 11:17:57'),
(45, 351, '113,116,118,119', 'Training', NULL, '2019-11-15 11:17:57'),
(46, 354, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, '2019-11-15 11:17:57'),
(47, 355, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, '2019-11-15 11:17:57'),
(48, 356, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, '2019-11-15 11:17:57'),
(49, 357, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, '2019-11-15 11:17:57'),
(50, 358, '118,113,116', 'Training', NULL, '2019-11-15 11:20:40'),
(51, 359, '118,113,116,117', 'Training', NULL, '2019-11-15 11:24:43'),
(52, 360, '118,113,116,117', 'Training', NULL, '2019-11-15 11:24:45'),
(53, 361, '118,113,117,116', 'Training', NULL, '2019-11-15 11:25:54'),
(54, 362, '118,117,113,116', 'Training', NULL, '2019-11-15 15:09:06'),
(55, 363, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(56, 364, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(57, 365, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(58, 366, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(59, 367, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(60, 368, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(61, 369, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(62, 370, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(63, 371, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(64, 372, '118,113,116,117', 'Training', NULL, '2019-11-15 12:28:39'),
(65, 373, '118,113,116,117,97,94,109,100,102,111,114,119', 'Training', NULL, NULL),
(66, 379, '94,97,100,102,111,113,114,116,118,119,109,117', 'Training', NULL, '2019-11-22 14:51:24'),
(67, 380, '118,113,116,117,97', 'Training', NULL, '2019-11-22 15:22:23');

-- --------------------------------------------------------

--
-- Table structure for table `training_relation_models`
--

CREATE TABLE `training_relation_models` (
  `id` int(10) UNSIGNED NOT NULL,
  `training_picture` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `session_picture` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subsession_picture` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `practice_picture` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `practice_video` varchar(70) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` enum('active','inactive') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `training_id` int(10) UNSIGNED DEFAULT NULL,
  `session_id` int(10) UNSIGNED DEFAULT NULL,
  `subsession_id` int(10) UNSIGNED DEFAULT NULL,
  `practice_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `training_sessions`
--

CREATE TABLE `training_sessions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(4) NOT NULL,
  `public` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_sessions`
--

INSERT INTO `training_sessions` (`id`, `user_id`, `training_id`, `session_id`, `active`, `public`, `created_at`, `updated_at`) VALUES
(98, 2, 109, 8, 0, 0, NULL, NULL),
(100, 2, 113, 8, 0, 0, NULL, NULL),
(101, 2, 114, 8, 0, 0, NULL, NULL),
(104, 2, 111, 8, 0, 0, NULL, NULL),
(136, 2, 94, 15, 0, 0, NULL, NULL),
(137, 2, 94, 11, 0, 0, NULL, NULL),
(138, 2, 94, 14, 0, 0, NULL, NULL),
(139, 2, 102, 17, 0, 0, NULL, NULL),
(140, 2, 102, 13, 0, 0, NULL, NULL),
(153, 2, 97, 14, 0, 0, NULL, NULL),
(154, 2, 97, 16, 0, 0, NULL, NULL),
(155, 2, 97, 17, 0, 0, NULL, NULL),
(156, 2, 116, 17, 0, 0, NULL, NULL),
(160, 2, 118, 17, 0, 0, NULL, NULL),
(161, 2, 118, 16, 0, 0, NULL, NULL),
(162, 2, 118, 15, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `training_session_orders`
--

CREATE TABLE `training_session_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `order` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_session_orders`
--

INSERT INTO `training_session_orders` (`id`, `user_id`, `training_id`, `order`) VALUES
(10, 2, 97, '14,16,17'),
(12, 2, 109, '8'),
(14, 2, 111, '8'),
(15, 2, 94, '15,11,14'),
(16, 2, 102, '17,13'),
(17, 2, 116, '17'),
(18, 2, 118, '17,16,15');

-- --------------------------------------------------------

--
-- Table structure for table `training_session_subsessions`
--

CREATE TABLE `training_session_subsessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `subsession_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `publisher_id` int(10) UNSIGNED DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `public` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_session_subsessions`
--

INSERT INTO `training_session_subsessions` (`id`, `training_id`, `session_id`, `subsession_id`, `name`, `picture`, `creator_id`, `publisher_id`, `active`, `public`, `created_at`, `updated_at`) VALUES
(13, 109, 8, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL),
(14, 97, 8, 42, 'Sbsb 2', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL),
(15, 97, 8, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL),
(16, 97, 8, 42, 'Sbsb 2', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(17, 97, 8, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(31, 113, 8, 42, 'Sbsb 2', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(32, 113, 8, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(33, 113, 8, 42, 'Sbsb 2', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(34, 113, 8, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(35, 114, 8, 42, 'Sbsb 2', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(36, 114, 8, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(37, 114, 8, 42, 'Sbsb 2', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(38, 114, 8, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(50, 94, 17, 42, 'Sbsb 2sdfsd', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL),
(51, 94, 17, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL),
(54, 102, 17, 40, 'sdfdsfdsf', NULL, 2, NULL, 1, 1, NULL, NULL),
(55, 102, 17, 42, 'Sbsb 2sdfsd', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL),
(56, 116, 17, 42, 'Sbsb 2sdfsd', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL),
(57, 118, 17, 42, 'Sbsb 2sdfsd', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL),
(58, 118, 17, 41, 'SBS', '/storage/media_gallery/pictures/Ker', 2, NULL, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `training_session_subsession_order`
--

CREATE TABLE `training_session_subsession_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `order` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_session_subsession_order`
--

INSERT INTO `training_session_subsession_order` (`id`, `user_id`, `training_id`, `session_id`, `order`, `created_at`, `updated_at`) VALUES
(6, 2, 109, 8, '41', '2019-11-04 18:11:30', '2019-11-04 18:11:30'),
(7, 2, 97, 8, '42,41', '2019-11-05 11:13:13', '2019-11-05 11:13:13'),
(9, 2, 94, 17, '42,41', '2019-11-07 09:57:24', '2019-11-07 10:12:28'),
(10, 2, 102, 17, '40,42', '2019-11-07 10:18:45', '2019-11-07 10:18:47'),
(11, 2, 116, 17, '42', '2019-11-08 08:11:48', '2019-11-08 08:11:48'),
(12, 2, 118, 17, '42,41', '2019-11-20 08:20:20', '2019-11-20 08:20:20');

-- --------------------------------------------------------

--
-- Table structure for table `training_session_subsession_practices`
--

CREATE TABLE `training_session_subsession_practices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `subsession_id` int(10) UNSIGNED NOT NULL,
  `practice_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `practice_video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `practice_picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` int(10) UNSIGNED NOT NULL,
  `publisher_id` int(10) UNSIGNED DEFAULT NULL,
  `active` tinyint(4) NOT NULL,
  `public` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_session_subsession_practices`
--

INSERT INTO `training_session_subsession_practices` (`id`, `training_id`, `session_id`, `subsession_id`, `practice_id`, `name`, `practice_video`, `practice_picture`, `creator_id`, `publisher_id`, `active`, `public`, `created_at`, `updated_at`) VALUES
(52, 109, 8, 41, 35, 'dfaf', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(53, 97, 8, 42, 36, 'Practice', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(54, 97, 8, 42, 35, 'dfaf', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(55, 97, 8, 42, 34, 'pera', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(56, 97, 8, 42, 27, 'asdas', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(57, 97, 8, 42, 25, 'dgdsv', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(58, 113, 8, 42, 36, 'Practice', '/tmp/php6KQL7J', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(59, 113, 8, 42, 35, 'dfaf', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(60, 113, 8, 42, 34, 'pera', '/tmp/php0wHlds', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(61, 113, 8, 42, 27, 'asdas', '/storage/practice_video/27.mp4', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(62, 113, 8, 42, 25, 'dgdsv', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(65, 97, 8, 41, 36, 'Practice', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(66, 97, 8, 41, 35, 'dfaf', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(67, 97, 8, 41, 31, 'Smiljanici', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(68, 114, 8, 42, 36, 'Practice', '/tmp/php6KQL7J', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(69, 114, 8, 42, 35, 'dfaf', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(70, 114, 8, 42, 34, 'pera', '/tmp/php0wHlds', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(71, 114, 8, 42, 27, 'asdas', '/storage/practice_video/27.mp4', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(72, 114, 8, 42, 25, 'dgdsv', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(73, 114, 8, 41, 36, 'Practice', '/tmp/php6KQL7J', '/storage/media_gallery/pictures/Ker', 2, NULL, 0, 0, NULL, NULL),
(74, 114, 8, 41, 35, 'dfaf', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(75, 114, 8, 41, 31, 'Smiljanici', '/storage/media_gallery/videos/SampleVideo_720x480_1mb.mp4', '/storage/media_gallery/pictures/external-content.duckduckgo.com', 2, NULL, 0, 0, NULL, NULL),
(80, 94, 17, 40, 35, 'dfaf', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(81, 116, 17, 42, 61, 'afsdgs', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(85, 118, 17, 42, 50, 'Practice govno', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(86, 118, 17, 42, 53, 'SDASDFASD', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(87, 118, 17, 42, 52, 'Practice 22', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(88, 118, 17, 42, 51, 'afsdfsdfd', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(89, 118, 17, 42, 61, 'afsdgs', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(90, 118, 17, 42, 58, 'afsdgs', NULL, NULL, 2, NULL, 0, 0, NULL, NULL),
(91, 118, 17, 42, 60, 'afsdgs', NULL, NULL, 2, NULL, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `training_session_subsession_practice_order`
--

CREATE TABLE `training_session_subsession_practice_order` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `session_id` int(10) UNSIGNED NOT NULL,
  `subsession_id` int(10) UNSIGNED NOT NULL,
  `order` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `training_session_subsession_practice_order`
--

INSERT INTO `training_session_subsession_practice_order` (`id`, `user_id`, `training_id`, `session_id`, `subsession_id`, `order`, `created_at`, `updated_at`) VALUES
(5, 2, 109, 8, 41, '35', '2019-11-04 18:11:38', '2019-11-04 18:11:38'),
(6, 2, 97, 8, 42, '36,35,34,27,25', '2019-11-05 11:13:25', '2019-11-05 11:13:25'),
(7, 2, 97, 8, 41, '36,35,31', '2019-11-05 11:51:18', '2019-11-05 11:51:21'),
(9, 2, 94, 17, 40, '35', '2019-11-07 10:08:38', '2019-11-07 10:08:45'),
(10, 2, 116, 17, 42, '61', '2019-11-08 08:11:56', '2019-11-08 08:11:56'),
(11, 2, 118, 17, 42, '50,53,52,51,61,58,60', '2019-11-20 08:20:28', '2019-11-21 14:35:06');

-- --------------------------------------------------------

--
-- Table structure for table `unsorted_visible`
--

CREATE TABLE `unsorted_visible` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `training_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unsorted_visible`
--

INSERT INTO `unsorted_visible` (`id`, `user_id`, `training_id`) VALUES
(1, 16, 94),
(2, 14, 94),
(3, 13, 94),
(4, 360, 118);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `identifier` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` enum('admin','trainer','user','superadmin') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('m','f') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_id` int(10) UNSIGNED DEFAULT NULL,
  `team_id` int(10) UNSIGNED DEFAULT NULL,
  `phone_number` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fcm_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `identifier`, `role`, `gender`, `organization_id`, `team_id`, `phone_number`, `profile_picture`, `password`, `parent_id`, `remember_token`, `created_at`, `updated_at`, `fcm_token`) VALUES
(2, 'Super', 'Admin', 'superadmin', 'superadmin@mail.com', 'NULL', 'superadmin', 'm', NULL, NULL, NULL, '/storage/users/2', '$2y$10$YenaYG5zZfCykiZocaG6b.p4FK0DAs5Wbo91ebz.kkdmQVaPsQtI6', NULL, NULL, '2019-10-11 13:27:00', '2019-11-04 16:34:40', 'e-ciU5LQVk4:APA91bHu-6djiDYpQPjhJ66tWsrB8NrllM_yDyRiLvpLhPkbF3lSTS3-tNpBPz1uU5geFkwNcTwJbdCUa_-9wuGK8W9PRPEHy8VOrCOZ-J5EhwYdx8ljEtOBuZTiez5g_vv3npmYn7su'),
(5, 'Parent', 'Player', NULL, 'parent@mail.com', NULL, 'user', 'm', 1, 10, NULL, '/media/users/5', '$2y$10$tNnkSg4Z6OpRq1JN/v003.i3N.DIxaziTANVq8Ytkjn96qVsIZ0aS', 5, NULL, '2019-10-11 13:27:00', '2019-10-11 13:27:01', NULL),
(8, 'Alexandria', 'Schinner', NULL, 'walker.herta@example.net', NULL, 'superadmin', 'f', NULL, NULL, NULL, '/media/users/8', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', NULL, 'KQph7stBxU', '2019-10-11 13:27:01', '2019-10-11 13:27:01', NULL),
(12, 'Milos1', 'Paunovic', 'miskec', 'milos.paunovic@itcentar.rs', '$2y$10$5GklCnnWvrWJnmTAZ7mpauUR7.ix2S6jmGgF0mUemi.prgXg52Nd6', 'superadmin', 'm', NULL, NULL, NULL, 'null', NULL, NULL, NULL, '2019-10-30 13:31:57', '2019-10-30 13:39:01', NULL),
(13, 'Vlahdislav', 'Cosa', 'vlah', 'vladislav.cosic@itcentar.rs', '$2y$10$JB4ZULFPl/GZIkZ69iJJHOr3eek8NS0tBcp4FeYal3Dql3rP6POga', 'admin', 'm', NULL, NULL, NULL, 'null', 'user1234', 2, NULL, '2019-10-30 15:25:50', '2019-10-30 16:15:19', NULL),
(14, 'Zdravko', 'Schinner', 'zdrvk', 'zd@zd.cin', '$2y$10$yHs9MykeL0NQNCzbY5BNEOiZ3UgL/3VgRAYDHSnDd967lBFeSZn/e', 'admin', 'm', 1, NULL, NULL, 'null', NULL, 2, NULL, '2019-10-30 15:49:35', '2019-10-30 15:49:37', NULL),
(16, 'pera', 'peric', 'usear', 'pera@aol.com', '$2y$10$KNr0YwtDhMP.fbXM4K9/HucfiOKRU6LhUerWcWayYjRr9Yajx8jP6', 'trainer', 'm', 1, NULL, NULL, 'null', '$2y$10$YenaYG5zZfCykiZocaG6b.p4FK0DAs5Wbo91ebz.kkdmQVaPsQtI6', NULL, NULL, '2019-10-30 15:54:58', '2019-10-30 15:55:00', NULL),
(17, 'User1', 'User_prezime', 'user1', 'user@mail.com', '$2y$10$69jJcLeLeGrrYaTUI7/jTeiCfP2FS2QlJtMGByD6LPSXsK3cnUDqu', 'user', 'm', 1, NULL, NULL, 'null', NULL, NULL, NULL, '2019-10-30 15:58:05', '2019-10-30 15:58:07', NULL),
(21, 'perada123', 'afds', 'pera', 'pera@mail.com1', '$2y$10$O/LuT/n1HScjZWE8Us5CZulS9Dn87mKobmNwe2vXTOtVdH2/0w4m.', 'admin', 'm', 1, NULL, NULL, 'null', NULL, NULL, NULL, '2019-10-30 16:11:20', '2019-11-04 06:37:33', NULL),
(46, 'pavle', 'vukovic', 'paja123', 'paja@mail.com', '$2y$10$GeqkBoRwKDCVsAB.tmMpLe9YqipNxnPwLJaOBYezeqwXEXRFYccNi', 'trainer', 'm', 1, NULL, NULL, 'null', '$2y$10$zFiioydi/IvLJtUQgSkHrO/wGB6ZfVjUQwqAy1eZA7SzyhtuJimRC', 2, NULL, '2019-11-04 12:09:06', '2019-11-04 12:09:06', NULL),
(47, NULL, NULL, NULL, 'asndiasnd@iasjdasd', NULL, 'user', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, NULL, NULL, NULL, 'asdasjdo', NULL, 'user', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, NULL, NULL, NULL, 'asdasdasd', NULL, 'user', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, NULL, NULL, NULL, 'asdasd', NULL, 'user', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, NULL, NULL, NULL, 'aas', NULL, 'user', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, NULL, NULL, NULL, 'd', NULL, 'user', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, NULL, NULL, NULL, 'asdasd@mail.com', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, NULL, NULL, NULL, 'user5', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, NULL, NULL, NULL, 'Djole@org.com', NULL, 'trainer', 'm', 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, NULL, NULL, NULL, 'Pera@org.com', NULL, 'trainer', 'm', 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, NULL, NULL, NULL, 'janko@mail.com', NULL, 'trainer', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, NULL, NULL, NULL, 'stanko@mail.com', NULL, 'trainer', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, NULL, NULL, NULL, 'petar@mail.com', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, NULL, NULL, NULL, 'milan@mail.com', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, NULL, NULL, NULL, 'zarko@mails.com', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, NULL, NULL, NULL, 'zarko1@mails.com', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, NULL, NULL, NULL, 'adgfdkgb@dsag[f', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, NULL, NULL, NULL, 'sfdsgfd@s,dgmf', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, NULL, NULL, NULL, 'pera@mail.sdasf', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, NULL, NULL, NULL, 'janko2@mail.com', NULL, 'trainer', 'm', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, NULL, NULL, NULL, '1233444', NULL, 'trainer', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, NULL, NULL, NULL, 'personal@mail.com', NULL, 'trainer', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, NULL, NULL, NULL, 'asdasdad', NULL, 'trainer', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, NULL, NULL, NULL, 'asasddddd!', NULL, 'trainer', 'm', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(330, 'Admin', 'New', NULL, 'new_admin@mail.com', '$2y$10$WVElTSgJ9cYGuvwCUjSDzOnejPr06ZrEv0SgvXTQXhPYU4OFhu2QG', 'admin', 'm', 2, NULL, NULL, 'null', '$2y$10$rw03bSKqXtIx527DFfXOG.k.yhNXhq1I/48tUCcnI12jqwAwllFEu', 2, NULL, '2019-11-08 10:16:14', '2019-11-08 10:16:14', NULL),
(344, 'First', 'Last', 'nekiuser', 'neki@mail.rs', '$2y$10$okJtX3t1qO3e.YLXz.fNGeoA8tBLbSypiNUfD4WLZZ/cd7janyhfq', 'trainer', 'm', 1, 200, NULL, 'null', '$2y$10$tuIrpC54b0D9eW/x8kZwYusT8EJIdCTnAbc.PHRxCMGSOHUyLFatm', 2, NULL, '2019-11-14 13:53:10', '2019-11-14 15:49:29', NULL),
(360, 'KORISNIK', 'NAJNOVIJI', 'prc', 'prc@mail.com', '$2y$10$IS8utNcsEdgTtK3faL.NP.ctLtjqt1zal5QWMtlEJmngK3VxhaMPG', 'admin', 'm', 22, NULL, NULL, 'null', '$2y$10$S5jqgCHtFciHoK9mv35DH.qJ92dZcH4DS5kF5eTn9NoGrfikz/pfe', 2, NULL, '2019-11-15 11:24:33', '2019-11-15 11:24:33', NULL),
(362, 'nik', 'nik', 'nik', 'nik@mail.com', '$2y$10$y8wXbBh5O2B0In/WtUuTIuwkYfC8fNuGahdMCBtI6YSaYWPnB.cDC', 'trainer', 'm', 20, 206, NULL, '/storage/users/362', '$2y$10$KRNIiwxjGOFrQzxP3IFkq.jU1viwJYfhsg78CkbYlxwICXjnlZiqm', 2, NULL, '2019-11-15 11:27:41', '2019-11-18 11:08:25', NULL),
(379, 'trainer', 'trainerovic', 'trainer', 'trainer@mail.com', '$2y$10$WCtG8iG2w5VnLZotqOA.We0eLtp65gFkK40ox3MgqM/DKfvrCzoVC', 'superadmin', 'm', NULL, NULL, NULL, 'null', '$2y$10$xWK1mDWiylKQ.OyoHQWzvutbv6Q4PZOFqjyACARzzS1ZJafcHI2i6', NULL, NULL, '2019-11-22 14:51:07', '2019-11-22 14:51:08', NULL),
(380, 'trener', 'trenercic', 'user123', 'trainercic@mail.com', '$2y$10$SF49OnYHdAWXsC.ZE9h1YerniMGsADxgdHae.TfDu7Wt0DegeHJV.', 'admin', 'm', 26, NULL, NULL, 'null', '$2y$10$O.w8yx7mb62IpMJhOUZEfubK7CP2T/p/dn25snbzWTsYUztxC2m.W', 2, NULL, '2019-11-22 15:21:21', '2019-11-22 15:21:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_trainings`
--

CREATE TABLE `user_trainings` (
  `id` int(10) UNSIGNED NOT NULL,
  `active` tinyint(4) NOT NULL,
  `public` tinyint(4) NOT NULL,
  `publisher_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `training_id` int(10) UNSIGNED DEFAULT NULL,
  `picture` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_user_trainings`
--

CREATE TABLE `user_user_trainings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `visible_trainings`
--

CREATE TABLE `visible_trainings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `visible` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visible_trainings`
--

INSERT INTO `visible_trainings` (`id`, `user_id`, `visible`) VALUES
(5, 344, '[{\"training_id\":97,\"active\":1},{\"training_id\":117,\"active\":1},{\"training_id\":102,\"active\":1},{\"training_id\":100,\"active\":0},{\"training_id\":94,\"active\":1},{\"training_id\":116,\"active\":1}]'),
(6, 2, '[{\"training_id\":118,\"active\":1},{\"training_id\":113,\"active\":0},{\"training_id\":116,\"active\":1},{\"training_id\":117,\"active\":0},{\"training_id\":97,\"active\":1},{\"training_id\":94,\"active\":0},{\"training_id\":109,\"active\":0},{\"training_id\":100,\"active\":0},{\"training_id\":102,\"active\":0},{\"training_id\":111,\"active\":0},{\"training_id\":114,\"active\":0},{\"training_id\":119,\"active\":1}]'),
(177, 347, '[{\"training_id\":116,\"active\":1},{\"training_id\":97,\"active\":1},{\"training_id\":118,\"active\":0},{\"training_id\":100,\"active\":0},{\"training_id\":102,\"active\":0},{\"training_id\":109,\"active\":0}]'),
(178, 16, '[{\"training_id\":116,\"active\":0},{\"training_id\":118,\"active\":1},{\"training_id\":97,\"active\":1},{\"training_id\":94,\"active\":1}]'),
(179, 358, NULL),
(180, 359, NULL),
(181, 360, NULL),
(182, 361, NULL),
(183, 362, '[{\"training_id\":118,\"active\":1},{\"training_id\":117,\"active\":1},{\"training_id\":113,\"active\":1},{\"training_id\":116,\"active\":1}]'),
(184, 372, NULL),
(185, 373, NULL),
(186, 380, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abouts_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `helps`
--
ALTER TABLE `helps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `helps_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_owner_id_foreign` (`owner_id`),
  ADD KEY `media_published_by_foreign` (`published_by`);

--
-- Indexes for table `media_orders`
--
ALTER TABLE `media_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org_publish`
--
ALTER TABLE `org_publish`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `practices`
--
ALTER TABLE `practices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `practices_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `session_subsessions`
--
ALTER TABLE `session_subsessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_subsessions_session_id_foreign` (`session_id`),
  ADD KEY `session_subsessions_subsession_id_foreign` (`subsession_id`),
  ADD KEY `session_subsessions_creator_id_foreign` (`creator_id`),
  ADD KEY `session_subsessions_publisher_id_foreign` (`publisher_id`);

--
-- Indexes for table `session_subsession_order`
--
ALTER TABLE `session_subsession_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_session_subsession_order_foreign` (`user_id`);

--
-- Indexes for table `session_subsession_practices`
--
ALTER TABLE `session_subsession_practices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `session_subsession_practices_session_id_foreign` (`session_id`),
  ADD KEY `session_subsession_practices_subsession_id_foreign` (`subsession_id`),
  ADD KEY `session_subsession_practices_practices_id_foreign` (`practices_id`),
  ADD KEY `session_subsession_practices_publisher_id_foreign` (`publisher_id`),
  ADD KEY `session_subsession_practices_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `subsessions`
--
ALTER TABLE `subsessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subsessions_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `subsession_practices`
--
ALTER TABLE `subsession_practices`
  ADD KEY `subsession_practices_subsession_id_foreign` (`subsession_id`),
  ADD KEY `subsession_practices_practice_id_foreign` (`practice_id`),
  ADD KEY `subsession_practices_user_id_foreign` (`user_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teams_organization_constraint` (`organization_id`);

--
-- Indexes for table `team_publish`
--
ALTER TABLE `team_publish`
  ADD PRIMARY KEY (`id`),
  ADD KEY `team_id` (`team_id`),
  ADD KEY `training_id` (`training_id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trainings_publisher_id_foreign` (`publisher_id`),
  ADD KEY `trainings_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `training_orders`
--
ALTER TABLE `training_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `training_relation_models`
--
ALTER TABLE `training_relation_models`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_relation_models_user_id_foreign` (`user_id`),
  ADD KEY `training_relation_models_training_id_foreign` (`training_id`),
  ADD KEY `training_relation_models_session_id_foreign` (`session_id`),
  ADD KEY `training_relation_models_subsession_id_foreign` (`subsession_id`),
  ADD KEY `training_relation_models_practice_id_foreign` (`practice_id`);

--
-- Indexes for table `training_sessions`
--
ALTER TABLE `training_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_sessions_user_id_foreign` (`user_id`),
  ADD KEY `training_sessions_training_id_foreign` (`training_id`),
  ADD KEY `training_sessions_session_id_foreign` (`session_id`);

--
-- Indexes for table `training_session_orders`
--
ALTER TABLE `training_session_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_session_orders_user_id_foreign` (`user_id`),
  ADD KEY `training_session_orders_training_id_foreign` (`training_id`);

--
-- Indexes for table `training_session_subsessions`
--
ALTER TABLE `training_session_subsessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_session_subsessions_training_id_foreign` (`training_id`),
  ADD KEY `training_session_subsessions_session_id_foreign` (`session_id`),
  ADD KEY `training_session_subsessions_subsession_id_foreign` (`subsession_id`),
  ADD KEY `training_session_subsessions_publisher_id_foreign` (`publisher_id`),
  ADD KEY `training_session_subsessions_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `training_session_subsession_order`
--
ALTER TABLE `training_session_subsession_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_session_subsession_order_user_id_foreign` (`user_id`),
  ADD KEY `training_session_subsession_order_training_id_foreign` (`training_id`),
  ADD KEY `training_session_subsession_order_session_id_foreign` (`session_id`);

--
-- Indexes for table `training_session_subsession_practices`
--
ALTER TABLE `training_session_subsession_practices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_session_subsession_practices_training_id_foreign` (`training_id`),
  ADD KEY `training_session_subsession_practices_session_id_foreign` (`session_id`),
  ADD KEY `training_session_subsession_practices_subsession_id_foreign` (`subsession_id`),
  ADD KEY `training_session_subsession_practices_practice_id_foreign` (`practice_id`),
  ADD KEY `training_session_subsession_practices_publisher_id_foreign` (`publisher_id`),
  ADD KEY `training_session_subsession_practices_creator_id_foreign` (`creator_id`);

--
-- Indexes for table `training_session_subsession_practice_order`
--
ALTER TABLE `training_session_subsession_practice_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_session_subsession_practice_order_session_id_foreign` (`session_id`),
  ADD KEY `training_session_subsession_practice_order_training_id_foreign` (`training_id`),
  ADD KEY `training_session_subsession_practice_order_subsession_id_foreign` (`subsession_id`),
  ADD KEY `training_session_subsession_practice_order_user_id_foreign` (`user_id`);

--
-- Indexes for table `unsorted_visible`
--
ALTER TABLE `unsorted_visible`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `training_id` (`training_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_parent_id_foreign` (`parent_id`),
  ADD KEY `users_team_id_foreign` (`team_id`),
  ADD KEY `users_organization_id_foreign` (`organization_id`);

--
-- Indexes for table `user_trainings`
--
ALTER TABLE `user_trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_trainings_creator_id_foreign` (`creator_id`),
  ADD KEY `user_trainings_publisher_id_foreign` (`publisher_id`),
  ADD KEY `user_trainings_training_id_foreign` (`training_id`);

--
-- Indexes for table `user_user_trainings`
--
ALTER TABLE `user_user_trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_user_trainings_user_id_foreign` (`user_id`),
  ADD KEY `user_user_trainings_training_id_foreign` (`training_id`);

--
-- Indexes for table `visible_trainings`
--
ALTER TABLE `visible_trainings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `helps`
--
ALTER TABLE `helps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=189;

--
-- AUTO_INCREMENT for table `media_orders`
--
ALTER TABLE `media_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `org_publish`
--
ALTER TABLE `org_publish`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `practices`
--
ALTER TABLE `practices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `session_subsessions`
--
ALTER TABLE `session_subsessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `session_subsession_order`
--
ALTER TABLE `session_subsession_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `session_subsession_practices`
--
ALTER TABLE `session_subsession_practices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subsessions`
--
ALTER TABLE `subsessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `team_publish`
--
ALTER TABLE `team_publish`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=120;

--
-- AUTO_INCREMENT for table `training_orders`
--
ALTER TABLE `training_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;

--
-- AUTO_INCREMENT for table `training_relation_models`
--
ALTER TABLE `training_relation_models`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `training_sessions`
--
ALTER TABLE `training_sessions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `training_session_orders`
--
ALTER TABLE `training_session_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `training_session_subsessions`
--
ALTER TABLE `training_session_subsessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `training_session_subsession_order`
--
ALTER TABLE `training_session_subsession_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `training_session_subsession_practices`
--
ALTER TABLE `training_session_subsession_practices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `training_session_subsession_practice_order`
--
ALTER TABLE `training_session_subsession_practice_order`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `unsorted_visible`
--
ALTER TABLE `unsorted_visible`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;

--
-- AUTO_INCREMENT for table `user_trainings`
--
ALTER TABLE `user_trainings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_user_trainings`
--
ALTER TABLE `user_user_trainings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `visible_trainings`
--
ALTER TABLE `visible_trainings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=187;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_published_by_foreign` FOREIGN KEY (`published_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `practices`
--
ALTER TABLE `practices`
  ADD CONSTRAINT `practices_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `session_subsessions`
--
ALTER TABLE `session_subsessions`
  ADD CONSTRAINT `session_subsessions_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `session_subsessions_publisher_id_foreign` FOREIGN KEY (`publisher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `session_subsessions_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `session_subsessions_subsession_id_foreign` FOREIGN KEY (`subsession_id`) REFERENCES `subsessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `session_subsession_order`
--
ALTER TABLE `session_subsession_order`
  ADD CONSTRAINT `user_session_subsession_order_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `session_subsession_practices`
--
ALTER TABLE `session_subsession_practices`
  ADD CONSTRAINT `session_subsession_practices_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `session_subsession_practices_practices_id_foreign` FOREIGN KEY (`practices_id`) REFERENCES `practices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `session_subsession_practices_publisher_id_foreign` FOREIGN KEY (`publisher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `session_subsession_practices_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `session_subsession_practices_subsession_id_foreign` FOREIGN KEY (`subsession_id`) REFERENCES `subsessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subsessions`
--
ALTER TABLE `subsessions`
  ADD CONSTRAINT `subsessions_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subsession_practices`
--
ALTER TABLE `subsession_practices`
  ADD CONSTRAINT `subsession_practices_practice_id_foreign` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`),
  ADD CONSTRAINT `subsession_practices_subsession_id_foreign` FOREIGN KEY (`subsession_id`) REFERENCES `subsessions` (`id`),
  ADD CONSTRAINT `subsession_practices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `teams_organization_constraint` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `team_publish`
--
ALTER TABLE `team_publish`
  ADD CONSTRAINT `team_publish_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`),
  ADD CONSTRAINT `team_publish_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `trainings_publisher_id_foreign` FOREIGN KEY (`publisher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_relation_models`
--
ALTER TABLE `training_relation_models`
  ADD CONSTRAINT `training_relation_models_practice_id_foreign` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `training_relation_models_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `training_relation_models_subsession_id_foreign` FOREIGN KEY (`subsession_id`) REFERENCES `subsessions` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `training_relation_models_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `training_relation_models_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `training_sessions`
--
ALTER TABLE `training_sessions`
  ADD CONSTRAINT `training_sessions_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_sessions_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_sessions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_session_orders`
--
ALTER TABLE `training_session_orders`
  ADD CONSTRAINT `training_session_orders_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_session_subsessions`
--
ALTER TABLE `training_session_subsessions`
  ADD CONSTRAINT `training_session_subsessions_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsessions_publisher_id_foreign` FOREIGN KEY (`publisher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsessions_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsessions_subsession_id_foreign` FOREIGN KEY (`subsession_id`) REFERENCES `subsessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsessions_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_session_subsession_order`
--
ALTER TABLE `training_session_subsession_order`
  ADD CONSTRAINT `training_session_subsession_order_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsession_order_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsession_order_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_session_subsession_practices`
--
ALTER TABLE `training_session_subsession_practices`
  ADD CONSTRAINT `training_session_subsession_practices_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsession_practices_practice_id_foreign` FOREIGN KEY (`practice_id`) REFERENCES `practices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsession_practices_publisher_id_foreign` FOREIGN KEY (`publisher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsession_practices_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsession_practices_subsession_id_foreign` FOREIGN KEY (`subsession_id`) REFERENCES `subsessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsession_practices_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_session_subsession_practice_order`
--
ALTER TABLE `training_session_subsession_practice_order`
  ADD CONSTRAINT `training_session_subsession_practice_order_session_id_foreign` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `training_session_subsession_practice_order_subsession_id_foreign` FOREIGN KEY (`subsession_id`) REFERENCES `subsessions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `training_session_subsession_practice_order_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `training_session_subsession_practice_order_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `unsorted_visible`
--
ALTER TABLE `unsorted_visible`
  ADD CONSTRAINT `unsorted_visible_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unsorted_visible_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `users_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_trainings`
--
ALTER TABLE `user_trainings`
  ADD CONSTRAINT `user_trainings_creator_id_foreign` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_trainings_publisher_id_foreign` FOREIGN KEY (`publisher_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_trainings_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_user_trainings`
--
ALTER TABLE `user_user_trainings`
  ADD CONSTRAINT `user_user_trainings_training_id_foreign` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_user_trainings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
