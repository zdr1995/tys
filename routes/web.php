<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: *');
// header('Access-Control-Allow-Headers: *');

/*
 * -------------------------------------------------------------------------
 *  STORAGE ROUTES  ** REDIRECT TO AWS PATCH **
 * ------------------------------------------------------------------------- 
 */
// Route::group(['prefix'=>'storage'],function() {
//     Route::get('/{path1}/{path2?}/{path3?}/{path4?}/{path5?}/{path6?}','MediaController@handleStoragePaths');
// });

use App\Http\Middleware\Auth;
// use Illuminate\Routing\Route;

Route::get('/',function(){
    return json_encode(['message'=>'HomePage']);
});
Route::group(['middleware'=>['web',Auth::class,'cors']], function ()use($router) {

    // $router->get('/user/{id}','UserController@trainings');

    //  --CRUD--
    $router->post('/create/{resource}','BaseController@create');

    $router->get('/load/{resource}/single/{id}','BaseController@single');
    $router->get('/load/{resource}/all/{paginate?}/{mobile?}','BaseController@all');
    $router->post('/load/{resource}/all/{paginate?}','BaseController@all');
    $router->get('/organization/nested','BaseController@nestedOrganizations');

    $router->post('/multi_actions/{resource}','BaseController@multiActions');

    $router->get('/load/about','BaseController@loadAbout');
    $router->get('/load/help','BaseController@loadHelp');
    $router->post('/mobile/news','BaseController@loadNewsMobile');

    $router->post('/patch/{resource}','BaseController@specificPatch');
    $router->post('/patch/{resource}/{id}','BaseController@patch');

    $router->post('/delete/multi/{resource}','BaseController@destroyMany');
    $router->delete('/delete/{resource}/{id}','BaseController@destroy');
    $router->post('/delete/{resource}','BaseController@destroyData');

    $router->post('/order/{resource}','OrderController@insertNew');
    $router->post('/search/{resource}','BaseController@searchQuery');
    $router->post('/active/training','TrainingController@setActive');

    $router->get('/user/trainings','UserController@visibleTrainings');
    $router->get('/user/{id}/trainings','UserController@userTrainings');
    $router->get('/user/{id}/sessions','UserController@allSessions');

    $router->get('/training/{id}','TrainingController@find');
    $router->get('/training/{id}/users','TrainingController@users');
    $router->get('/training/{id}/session','TrainingController@sessions');
    $router->post('/training/check/publish','PublishController@checkVisibilityForTraining');

    $router->get('/chat/load/rooms','ChatController@loadRooms');
    
    //  Publish training for specific groups
    $router->post('/custom/publish','PublishController@changePublishedStatus');

    // Filter practices for app priview
    $router->post('/filter/practice','PracticeController@filterPractices');

    // $router->get('/training/{id}/sessions','TrainingController@sessions');

    $router->get('/session/{id}/subsession','SessionController@subsessions');

    $router->post('/notify/users','NotifyController@notifyUsers');

    $router->get('/subsession/{id}/practice','SubsessionController@practices');

    $router->post('/chpsswd','UserController@changePassword');

    //  --Media Library route--
    // $router->post('/media/insert','MediaController@insert');
    $router->post('/media/media_gallery/all/{type}/{pagination?}','MediaController@loadByType');

    $router->post('/filter/user','UserController@filter');
    $router->get('/get/pusher/info','UserController@getChatkitData');
//  Media download route
    // $router->get('/media/{name}','BaseController@downloadFile');

    Route::get('/orders/subsession','OrderController@readOrders');
});

Route::post('/user_by_hash','UserController@getHashedUser');
Route::get('/register/user','AuthController@register');
Route::post('/hash','BaseController@hashMail');
Route::post('/user/reset-password-request', 'UserController@resetPasswdRequest');
Route::post('/user/validate-reset-password', 'UserController@validateResetPasswdCode');
Route::post('/user/change-reset-password', 'UserController@changeResetPassword');

// Route::get('user/{id}','UserController@trainings');
Route::get('/load/{resource}/{id}','BaseController@single');
Route::get('/user/{id}/trainer','UserController@getTrainer');

//  --Load default users picture
Route::get('/media/{object}/{id}','MediaController@downloadFile');
Route::get('/media/{object}/{default?}/{name}','MediaController@loadDefaults');

// Route::get('/register/user','AuthController@register');

// Route::get('/migrations/fresh','BaseController@freshMigrations');
Route::get('/user/{id}/teams','BaseController@loadTeams');

Route::get('/user/test/{resource}/all/{paginate?}','BaseController@test');
Route::get('/user/childrens',function(){
    $user= \JWTAuth::parseToken()->authenticate();
    return response()->json($user->childrens());
});

//  --Media Load--
// Route::get('load/')
Route::get('media_video','MediaController@test_video');
Route::get('proba',function(){
    $data=\App\TrainingSessionSubsessionOrder::where('session_id',24)->where('training_id',83)->get();
    return response()->json($data);
//    return response()->json(['message'=>'Proba']);
});
//  Local helper routes
Route::get('profilePicture',function(){
    $user=new \App\User();
    $pic=$user->getRandomDefaultPicture();
    return $pic;
});

Route::get('/user/parent/{id}','BaseController@parents');
Route::get('/user/children/{id}','BaseController@childrens');
Route::get('/organizations/all','UserController@loadWithOrganizations');


Route::get('/migrate/fresh','BaseController@runMigrations');
Route::get('/db/seed','BaseController@runSeeder');
Route::get('storage','BaseController@storageLink');
//Route::get('/undefined','BaseController@returnEmpty');
// Route::get('/alter','BaseController@addColumn');
Route::get('/alter','TestController@alterModifications');
Route::get('/push/send','TestController@sendPush');
Route::get('pusher/test','TestController@sendPusher');
Route::get('pusher/view',function(){
    return view('test');
});
Route::post('/test/team/insert','TestController@insertTeamNews');
Route::post('insert/chat','TestController@testChatMongo');
Route::get('load/chat','TestController@getChat');
Route::get('get/chatbyid/{id}','TestController@loadChatByUserId');
Route::post('/insert/room','TestController@storeRoom');
Route::post('/test','TestController@guzzletest');
Route::get('/chatkit/rooms','TestController@loadAllRooms');
Route::get('mailTest', 'TestController@mailTest');
Route::get('testMailView', 'TestController@loadHtmlView');