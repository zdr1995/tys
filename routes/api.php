<?php

use App\Http\Middleware\Auth;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// header('Access-Control-Allow-Origin: *');
// header('Access-Control-Allow-Methods: *');
// header('Access-Control-Allow-Headers: *');


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/pera')->middleware(Auth::class);
Route::group(['middleware'=>'api','prefix'=>'auth'],function()use($router){
    $router->post('/login','AuthController@login');
});

Route::group(['middleware'=>['api',Auth::class],'prefix'=>'auth'],function()use ($router){

//    Here is place for all routes
    

//     $router->get('/user/trainings','UserController@visibleTrainings');
//     $router->get('/user/{id}/trainings','UserController@userTrainings');
//     $router->get('/user/{id}/sessions','UserController@allSessions');

//     $router->get('/training/{id}','TrainingController@find');
//     $router->get('/training/{id}/users','TrainingController@users');

//     $router->get('/session/{id}/subsessions','SessionController@subsessions');


//     $router->get('/subsession/{id}/practices','SubsessionController@practices');


// //  Media download route
//     $router->get('/media/{name}','BaseController@downloadFile');

//     $router->get('/training/{id}/sessions','TrainingController@sessions');



});
